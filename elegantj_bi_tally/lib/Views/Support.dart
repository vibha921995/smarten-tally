import 'dart:async';

import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:elegantj_bi_tally/Helper/PreferenceHelper.dart';
import 'package:elegantj_bi_tally/Models/LogResponse.dart';
import 'package:elegantj_bi_tally/Models/LoginResponse.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/DashBoardView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

//class Logs extends StatefulWidget {
//  final DashBoardViewState dashBoardViewState;
//
//  @override
//  _LogsState createState() => _LogsState();
//}
class Support extends StatefulWidget {
  final DashBoardViewState dashBoardViewState;

  // SupportView(this.dashBoardViewState,{Key key});
  Support({Key key, this.dashBoardViewState}) : super(key: key);

  @override
  _SupportState createState() => _SupportState();
}

class _SupportState extends State<Support> {
  // Completer<WebViewController> _controller = Completer<WebViewController>();
  InAppWebViewController _webViewController;

  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;
  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;
  String clientId = "";
  List supportDropdownList = [
    'Select Type',
    'Registration',
    'Payment',
    'Technical',
    'Desktop App',
    'Reseller Inquiry',
    'Other'
  ];
  String _currentNode;

  @override
  void initState() {
    super.initState();
    //getSharedPreferenceObject();
    _currentNode = supportDropdownList.first;
    print("on initState===");
  }

  Future<void> getSharedPreferenceObject() async {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
      UserModel userModel = preferenceHelper.getUserData();
      clientId = userModel.clientId;

      // getLogHistory();
    });
  }

  @override
  Widget build(BuildContext context) {
    print("on build===");
    return Scaffold(
      //resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      key: scaffoldGlobalKey,
      body: Stack(
        children: [
          Column(
            children: [
              Padding(
                padding: EdgeInsets.all(Constant.size9),
                child: Stack(
                  children: [
                    // Align(
                    //   alignment: Alignment.centerLeft,
                    //   child: InkWell(
                    //     child: Icon(FontAwesomeIcons.longArrowAltLeft),
                    //     onTap: () {
                    //       widget.dashBoardViewState.onManutap("dashboard", 0);
                    //     },
                    //   ),
                    // ),
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        "SUPPORT",
                        style: TextStyle(
                            fontFamily: 'fontawesome',
                            fontSize: FontSize.s17,
                            fontWeight: FontWeight.w600,
                            color: AppColors.BLACKCOLOR),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: Constant.size0_5,
                color: AppColors.appLightGray,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.all(Constant.size6),
                decoration: BoxDecoration(
                  border: Border.all(
                      color: AppColors.appBlue, width: Constant.size1),
                ),
                child: Padding(
                  padding: EdgeInsets.all(Constant.size4),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton(
                      iconEnabledColor: AppColors.CARD_BG_COLOR,
                      style: TextStyle(
                          color: AppColors.appBlue, fontSize: FontSize.s16),
                      iconSize: Constant.size32,
                      //map each value from the lIst to our dropdownMenuItem widget
                      items: supportDropdownList
                          .map(
                            (value) => DropdownMenuItem(
                              child: Padding(
                                padding: EdgeInsets.only(left: Constant.size8),
                                child: Text(
                                  value,
                                  style: TextStyle(color: AppColors.appBlue),
                                ),
                              ),
                              value: value,
                            ),
                          )
                          .toList(),
                      onChanged: (value) {
                        setState(() {
                          _currentNode = value;
//                        filterSearchResults(_currentValue);
                        });
                      },
                      //this wont make dropdown expanded and fill the horizontal space
                      isExpanded: false,
                      value: _currentNode,
                      isDense: true,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                    margin: EdgeInsets.all(Constant.size6),
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: AppColors.appBlue, width: Constant.size1),
                    ),
                    child: Padding(
                      padding: EdgeInsets.only(left: Constant.size8),
                      child: TextField(
                        cursorColor: Colors.black,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            hintText: 'Notes',
                            contentPadding: EdgeInsets.all(0)),
                        //   controller: _etcontroller,
                        textInputAction: TextInputAction.done,
                        style: TextStyle(
                          backgroundColor: Colors.transparent,
                          fontSize: FontSize.s16,
                          color: AppColors.CARD_BG_COLOR,
                        ),
                      ),
                    )),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: Container(
                       margin: EdgeInsets.all(Constant.size6),
                      decoration: BoxDecoration(
                        color: AppColors.APPBLUE,
                      ),
                      child: Expanded(
                        child: Padding(
                          padding:  EdgeInsets.all(Constant.size6),
                          child: Text(
                            "SUBMIT",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: FontSize.s16,
                                fontFamily: 'fontawesome',
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
          ProgressIndicatorLoader(AppColors.primary, isLoading)
        ],
      ),
    );
  }
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: const Text('Logs'),
//      ),
//      body: WebView(
//        initialUrl: 'http://35.154.99.45/tally_saas/mobile-logs.php',
//        onWebViewCreated: (WebViewController webViewController) {
//          _controller.complete(webViewController);
//        },
//      ),
//    );
//  }
}

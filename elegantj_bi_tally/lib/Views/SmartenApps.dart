import 'dart:async';

import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:elegantj_bi_tally/Helper/PreferenceHelper.dart';
import 'package:elegantj_bi_tally/Models/LogResponse.dart';
import 'package:elegantj_bi_tally/Models/LoginResponse.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/DashBoardView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

//class Logs extends StatefulWidget {
//  final DashBoardViewState dashBoardViewState;
//
//  @override
//  _LogsState createState() => _LogsState();
//}
class SmartenApps extends StatefulWidget {

  final DashBoardViewState dashBoardViewState;

  // SmartenAppsView(this.dashBoardViewState,{Key key});
  SmartenApps({
    Key key,
    this.dashBoardViewState
  }):super(key:key);
  @override
  _SmartenAppsState createState() => _SmartenAppsState();
}
class _SmartenAppsState extends State<SmartenApps> {
  // Completer<WebViewController> _controller = Completer<WebViewController>();
  InAppWebViewController _webViewController;

  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;
  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;
  List<LogModel> LogsList = [];
  String clientId = "";
  @override
  void initState() {
    super.initState();
    getSharedPreferenceObject();
    print("on initState===");
  }
  Future<void> getSharedPreferenceObject() async {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
      UserModel userModel = preferenceHelper.getUserData();
      clientId = userModel.clientId;
     // getLogHistory();
    });
  }

  @override
  Widget build(BuildContext context) {
    print("on build===");
    return Scaffold(
      //resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      key: scaffoldGlobalKey,
      body: Stack(
        children: [
          Column(
            children: [
              Padding(
                padding: EdgeInsets.all(Constant.size9),
                child: Stack(
                  children: [
                    // Align(
                    //   alignment: Alignment.centerLeft,
                    //   child: InkWell(
                    //     child: Icon(FontAwesomeIcons.longArrowAltLeft),
                    //     onTap: () {
                    //       widget.dashBoardViewState.onManutap("dashboard", 0);
                    //     },
                    //   ),
                    // ),
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        "SmartenApps for Tally",
                        style: TextStyle(
                            fontFamily: 'fontawesome',
                            fontSize: FontSize.s17,
                            fontWeight: FontWeight.w600,
                            color: AppColors.BLACKCOLOR),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: Constant.size0_5,
                color: AppColors.appLightGray,
              ),
              Container(
                margin: EdgeInsets.all(Constant.size4) ,
                child:  Padding(
                  padding:  EdgeInsets.all(Constant.size16),
                  //here
                  child:  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      style: TextStyle(color: Colors.black),
                      children: <TextSpan>[
                        TextSpan(
                            text: 'Business users love the Tally® solutions, and power of ad hoc data mining and visualization can help businesses to take them to the next level. Business reporting needs are ever-changing and never-ending, and the need for ad hoc data mining and visualization is crucial. Executives need to monitor summary results and key performance indicators (KPIs), while sales managers may need information on weekly sales, targets, and receivables. With SmartenApps and Tally, businesses can better leverage their accounting solution and gather and analyze business intelligence data from multiple sources.',
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'open-sans',
                                letterSpacing: 0.9,
                                fontSize: FontSize.s17)),
                        // TextSpan(
                        //     text:
                        //     String.fromCharCodes(new Runes('Click here')),
                        //     style: TextStyle(
                        //         color: AppColors.APPBLUE,
                        //         fontFamily: 'open-sans',
                        //         letterSpacing: 0.9,
                        //         fontSize: FontSize.s18),
                        //     recognizer: TapGestureRecognizer()
                        //       ..onTap = () {
                        //         _launchInWebViewWithJavaScript("http://35.154.99.45/tally_saas/pdf/EULA.pdf");
                        //         //print('Terms of Service"');
                        //       }),
                        TextSpan(
                            text: 'Click here to know more about how SmartenApps for Tally® can help you take your organisation to the next level.',
                            style: TextStyle(
                                color: Colors.black,
                                letterSpacing: 0.9,
                                fontSize: FontSize.s17)),
                      ],
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: InkWell(
                      onTap: (){
                        _launchInWebViewWithJavaScript("https://www.smartenapps.com/");
                      },
                      child: Container(
                        margin: EdgeInsets.all(Constant.size10),
                        decoration: BoxDecoration(
                          color: AppColors.skyblue,
                        ),
                        child: Expanded(
                          child: Padding(
                            padding:  EdgeInsets.all(Constant.size6),
                            child: Text(
                              "VISIT OUR WEBSITE",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: FontSize.s15,
                                  fontFamily: 'fontawesome',
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
          ProgressIndicatorLoader(AppColors.primary, isLoading)
        ],
      ),
    );
  }

  Future<void> _launchInWebViewWithJavaScript(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        // forceSafariVC: true,
        // forceWebView: true,
        enableJavaScript: true,
      );
    } else {
      throw 'Could not launch $url';
    }
  }


//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: const Text('Logs'),
//      ),
//      body: WebView(
//        initialUrl: 'http://35.154.99.45/tally_saas/mobile-logs.php',
//        onWebViewCreated: (WebViewController webViewController) {
//          _controller.complete(webViewController);
//        },
//      ),
//    );
//  }
}
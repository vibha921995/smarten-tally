import 'dart:convert';
import 'dart:io';

import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ConstantString.dart';
import 'package:elegantj_bi_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:elegantj_bi_tally/Helper/PreferenceHelper.dart';
import 'package:elegantj_bi_tally/Models/ErrorResponse.dart';
import 'package:elegantj_bi_tally/Models/LoginResponse.dart';
import 'package:elegantj_bi_tally/Utils/ApiUtils.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/DashBoardView.dart';
import 'package:elegantj_bi_tally/Views/ForgotPasswordView.dart';
import 'package:elegantj_bi_tally/Views/Payment.dart';
import 'package:elegantj_bi_tally/Views/SignUpView.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:in_app_review/in_app_review.dart';
import 'package:intl/intl.dart';
// import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:elegantj_bi_tally/Views/IAP.dart';

class SignInView extends StatefulWidget {
  SignInView({Key key}) : super(key: key);

  @override
  _SignInViewState createState() => _SignInViewState();
}

class _SignInViewState extends State<SignInView> {
  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();

  // PreferenceHelper preferenceHelper;
  // SharedPreferences prefs;
  bool isLoading = false;
  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;
  LoginResponse studentResponse;

  // LoginResponse studentResponse;
  TextEditingController _etUsername = new TextEditingController();
  TextEditingController _etPassword = new TextEditingController();
  FocusNode fnusername = new FocusNode();
  FocusNode fnpassword = new FocusNode();
  bool isError = false;
  String errorText = "";
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String _homeScreenText = "Waiting for token...";
  String _messageText = "Waiting for message...";
  String fcmToken = "";



  @override
  void initState() {
    super.initState();
    getSharedPreferenceObject();

    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      fcmToken = token;
      print("fcmToken===  $fcmToken");
    });

    // _firebaseMessaging.configure(
    //   onBackgroundMessage: myBackgroundMessageHandler,
    //   onMessage: (Map<String, dynamic> message) async {
    //     setState(() {
    //       _messageText = "Push Messaging message: $message";
    //     });
    //     print("onMessage: $message");
    //   },
    //   onLaunch: (Map<String, dynamic> message) async {
    //     setState(() {
    //       _messageText = "Push Messaging message: $message";
    //     });
    //     print("onLaunch: $message");
    //   },
    //   onResume: (Map<String, dynamic> message) async {
    //     setState(() {
    //       _messageText = "Push Messaging message: $message";
    //     });
    //     print("onResume: $message");
    //   },
    // );
    // _firebaseMessaging.requestNotificationPermissions(
    //     const IosNotificationSettings(sound: true, badge: true, alert: true));
    // _firebaseMessaging.onIosSettingsRegistered
    //     .listen((IosNotificationSettings settings) {
    //   print("Settings registered: $settings");
    // });
    // _firebaseMessaging.getToken().then((String token) {
    //   assert(token != null);
    //   setState(() {
    //     _homeScreenText = "Push Messaging token: $token";
    //   });
    //   print(_homeScreenText);
    // });
  }



  static Future<dynamic> myBackgroundMessageHandler(
      Map<String, dynamic> message) async {
    print("onMessage: $message");

    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }

    // Or do other work.
  }

  Future<void> getSharedPreferenceObject() async {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
    });
  }

  _checkInternetConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print("Internet result  = " + result.toString());
        setState(() {
          isLoading = true;
        });
        doLogin();
      }
    } on SocketException catch (e) {
      e.toString();
      setState(() {
        isLoading = false;
      });
//      if (!isDialogShowing) {
//        _showDialog();
//      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Constant.setScreenAwareConstant(context);
    return SafeArea(
      child: Scaffold(
        // resizeToAvoidBottomPadding: true,
        resizeToAvoidBottomInset: true,
        key: scaffoldGlobalKey,
        body: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height,
              color: AppColors.progressPathShadowColor,
              child: ListView(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
//                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("assets/images/inner-logo.png",
                      fit: BoxFit.scaleDown),
                  Container(
                    height: Constant.size0_5,
                    color: AppColors.appLightGray,
                  ),
                  Padding(
                    padding: EdgeInsets.all(Constant.size8),
                    child: Text("SIGN IN",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: FontSize.s17,
                            color: AppColors.BLACKCOLOR,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'open-sens')),
                  ),
                  Container(
                    height: Constant.size0_5,
                    color: AppColors.appLightGray,
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size20,
                        left: Constant.size15,
                        right: Constant.size15),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        new LengthLimitingTextInputFormatter(10),
                        WhitelistingTextInputFormatter(RegExp(r'[0-9]')),
                      ],
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: FontSize.s16,
                          fontFamily: 'open-sans',
                          fontWeight: FontWeight.w600),
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(Constant.size17),
                          hintText: "Phone",
                          hintStyle: TextStyle(
                              color: AppColors.hintTextColor,
                              fontSize: FontSize.s16,
                              fontFamily: 'open-sans',
                              fontWeight: FontWeight.w500),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                              borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: AppColors.APPBLUE),
                              borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.circular(Constant.size3))),
                      controller: _etUsername,
                      onFieldSubmitted: (String value) {
                        FocusScope.of(context).requestFocus(fnpassword);
                      },
                      focusNode: fnusername,
                      textInputAction: TextInputAction.next,
                      textAlign: TextAlign.start,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: Constant.size20,
                        left: Constant.size15,
                        right: Constant.size15),
                    child: TextFormField(
                      obscureText: true,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: FontSize.s16,
                          fontFamily: 'open-sans',
                          fontWeight: FontWeight.w600),
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(Constant.size17),
                          hintText: "Password",
                          hintStyle: TextStyle(
                              color: AppColors.hintTextColor,
                              fontSize: FontSize.s16,
                              fontFamily: 'open-sans',
                              fontWeight: FontWeight.w500),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                              borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: AppColors.APPBLUE),
                              borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.circular(Constant.size3))),
                      controller: _etPassword,
                      focusNode: fnpassword,
                      textInputAction: TextInputAction.done,
                      textAlign: TextAlign.start,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      if (_etUsername.text == "") {
                        setState(() {
                          scaffoldGlobalKey.currentState.showSnackBar(
                              ErrorSnakbar('Enter registered Mobile Number'));
                          fnusername.requestFocus();
                        });
                      } else if (_etUsername.text.length < 10) {
                        scaffoldGlobalKey.currentState.showSnackBar(
                            ErrorSnakbar('Enter valid Mobile Number'));
                        fnusername.requestFocus();
                      } else if (_etPassword.text == "") {
                        setState(() {
                          scaffoldGlobalKey.currentState
                              .showSnackBar(ErrorSnakbar('Enter Password'));
                          fnpassword.requestFocus();
                        });
                      } else {
                        doLogin();
                        //  _checkInternetConnection();
                      }
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          left: Constant.size15,
                          right: Constant.size15,
                          bottom: Constant.size12,
                          top: Constant.size18),
                      padding: EdgeInsets.all(Constant.size12),
                      decoration: BoxDecoration(
                        color: AppColors.APPBLUE,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Container(
                            child: Expanded(
                              child: Text(
                                "SIGN IN",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: FontSize.s18,
                                    fontFamily: 'fontawesome',
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  // Padding(
                  //   padding: EdgeInsets.only(
                  //       left: Constant.size18,
                  //       right: Constant.size18,
                  //       bottom: Constant.size12,
                  //       top: Constant.size10),
                  //   child: IntrinsicHeight(
                  //     child: Row(
                  //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //       mainAxisSize: MainAxisSize.max,
                  //       children: <Widget>[
                  //         InkWell(
                  //           onTap: () {
                  //             Navigator.push(
                  //                 context,
                  //                 MaterialPageRoute(
                  //                     builder: (BuildContext context) =>
                  //                         SignUpView()));
                  //           },
                  //           child: Text(
                  //             "Not Registered?",
                  //             textAlign: TextAlign.end,
                  //             style: TextStyle(
                  //                 color: AppColors.APPBLUE,
                  //                 fontSize: FontSize.s16,
                  //                 fontFamily: 'fontawesome'),
                  //           ),
                  //         ),
                  //         Container(
                  //             width: Constant.size0_5,
                  //             color: AppColors.appLightGray),
                  //         InkWell(
                  //           onTap: () {
                  //             Navigator.push(
                  //                 context,
                  //                 MaterialPageRoute(
                  //                     builder: (BuildContext context) =>
                  //                         ForgotPasswordView()));
                  //           },
                  //           child: Text(
                  //             "Forgot Password?",
                  //             textAlign: TextAlign.end,
                  //             style: TextStyle(
                  //                 color: AppColors.APPBLUE,
                  //                 fontSize: FontSize.s16,
                  //                 fontFamily: 'fontawesome'),
                  //           ),
                  //         ),
                  //       ],
                  //     ),
                  //   ),
                  // ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: Constant.size18,
                        right: Constant.size18,
                        bottom: Constant.size12,
                        top: Constant.size10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        SignUpView()));
                          },
                          child: Text(
                            "Not Registered?",
                            //   textAlign: TextAlign.end,
                            style: TextStyle(
                                color: AppColors.APPBLUE,
                                fontSize: FontSize.s16,
                                fontFamily: 'fontawesome'),
                          ),
                        ),
                        Container(height: Constant.size10),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        ForgotPasswordView()));
                          },
                          child: Text(
                            "Forgot Password?",
                            //   textAlign: TextAlign.end,
                            style: TextStyle(
                                color: AppColors.APPBLUE,
                                fontSize: FontSize.s16,
                                fontFamily: 'fontawesome'),
                          ),
                        ),
                        Container(height: Constant.size10),
                        InkWell(
                          onTap: () {
                            _launchInWebViewWithJavaScript(
                                ConstantString.Faqs_login_url);
                          },
                          child: Text(
                            "FAQs",
                            textAlign: TextAlign.end,
                            style: TextStyle(
                                //    decoration: TextDecoration.underline,
                                color: AppColors.APPBLUE,
                                fontSize: FontSize.s16,
                                fontFamily: 'fontawesome'),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            ProgressIndicatorLoader(AppColors.primary, isLoading)
          ],
        ),
      ),
    );
  }

  Future<void> _launchInWebViewWithJavaScript(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        // forceSafariVC: true,
        // forceWebView: true,
        enableJavaScript: true,
      );
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget ErrorSnakbar(title) {
    return new SnackBar(
      backgroundColor: Colors.redAccent,
      content: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Icon(
            FontAwesomeIcons.exclamationTriangle,
            size: Constant.size15,
            color: Colors.white,
          ),
          SizedBox(
            width: Constant.size8,
          ),
          Expanded(
            child: Text(
              title,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: FontSize.s15,
                  fontFamily: 'fontawesome'),
              overflow: TextOverflow.ellipsis,
              maxLines: 10,
            ),
          ),
        ],
      ),
    );
  }

  Future doLogin() async {
    setState(() {
      isLoading = true;
    });
    try {
      // PackageInfo packageInfo = await PackageInfo.fromPlatform();
      var body = {
        "email_phone": _etUsername.text.trim(),
        "password": _etPassword.text.trim(),
        "login_with": "M",
        "appversion": "3.0",
        "firebase": fcmToken,
        "app_version": "3.0",
        "app_store_version":"3.0"
    // "ostype": Platform.isAndroid?"Android":"iOS"
      };
      var response =
          await http.post(Uri.parse(ApiUtils.LOGIN_URL), body: body);
      print("Login response : ----" + response.body);
      if (response.statusCode == 200 || response.statusCode == 201) {
        ErrorResponse errorResponse = new ErrorResponse.fromJson(json.decode(
            response.body
                .replaceFirst("associatedcompany", '"associatedcompany"')));
        setState(() {
          isLoading = false;
        });
        if (errorResponse.success == "1") {
          isPaymentFromIAP = false;
          studentResponse = new LoginResponse.fromJson(json.decode(response.body
              .replaceFirst("associatedcompany", '"associatedcompany"')));
          scaffoldGlobalKey.currentState.showSnackBar(SnackBar(
              content: Text(studentResponse.message),
              backgroundColor: Colors.green));
          preferenceHelper.saveUserData(studentResponse.response);
          preferenceHelper.saveIsUserLoggedIn(true);

          preferenceHelper.saveSubscriptionEndDate(studentResponse.response.subscriptionEndDate.toString());
          preferenceHelper.savePackageID(studentResponse.response.package_id);
          FirebaseAnalytics().logLogin(loginMethod: "");
          var now = new DateTime.now();
          var formatter = new DateFormat('yyyy-MM-dd hh:mm:ss');
          String formattedDate = formatter.format(now);
          print(formattedDate);
        //  DateTime dateTimeCreatedAt = DateTime.parse(
              //studentResponse.response.subscriptionEndDate.toString());
          DateTime dateTimeCreatedAt = DateTime.parse(preferenceHelper.getSubscriptionEndDate());
          DateTime dateTimeNow = DateTime.now();
          final differenceInDays =
              dateTimeNow.difference(dateTimeCreatedAt).inDays;
          print(differenceInDays);
          //if (differenceInDays > 0) {
//            Navigator.pushReplacement(
//                context,
//                MaterialPageRoute(
//                    builder: (BuildContext context) => IAP()));
//          }
//          else {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => DashBoardView()));
//          }
        } else {
          scaffoldGlobalKey.currentState
              .showSnackBar(ErrorSnakbar(errorResponse.message));
        }
      } else if (response.statusCode == 400) {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState
            .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      } else {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState
            .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      }
    } on SocketException catch (e) {
      print(e.message);
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState
          .showSnackBar(ErrorSnakbar(ConstantString.network_error));
    } on Exception catch (e) {
      print(e.toString());
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState
          .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
    }
  }
}

extension DateOnlyCompare on DateTime {
  bool isSameDate(DateTime other) {
    return this.year == other.year &&
        this.month == other.month &&
        this.day == other.day;
  }
}

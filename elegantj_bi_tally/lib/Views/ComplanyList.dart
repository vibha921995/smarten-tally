import 'dart:convert';
import 'dart:io';

import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ConstantString.dart';
import 'package:elegantj_bi_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:elegantj_bi_tally/Helper/PreferenceHelper.dart';
import 'package:elegantj_bi_tally/Models/CompanyListResponse.dart';
import 'package:elegantj_bi_tally/Models/CompanyModel.dart';
import 'package:elegantj_bi_tally/Models/ErrorResponse.dart';
import 'package:elegantj_bi_tally/Models/LogResponse.dart';
import 'package:elegantj_bi_tally/Models/LoginResponse.dart';
import 'package:elegantj_bi_tally/Utils/ApiUtils.dart';
import 'package:elegantj_bi_tally/Utils/PrefUtils.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/DashBoardView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class CompanyList extends StatefulWidget {
  final DashBoardViewState dashBoardViewState;

  // CompanyList(this.dashBoardViewState,{Key key});
  CompanyList({Key key, this.dashBoardViewState}) : super(key: key);

  @override
  _CompanyListState createState() => _CompanyListState();
}

class _CompanyListState extends State<CompanyList> {
  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();

  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;
  bool isLoading = false;
  List<CompanyModel> companyList = [];

  // String clientId = "";
  String userId = "";

  @override
  void initState() {
    super.initState();
    getSharedPreferenceObject();
  }

  Future<void> getSharedPreferenceObject() async {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
      UserModel userModel = preferenceHelper.getUserData();
      // clientId = userModel.clientId;
      userId = userModel.userId;
      // setState(() {
      //   // for (int i = 0; i < 5; i++) {
      //     companyList.add(new CompanyModel(
      //         "Demo Company PVT. LTD",
      //         "assets/images/home-icon.png",
      //         false));
      //   // }
      // });
      getCompanyList();
    });
  }

  Future getCompanyList() async {
    setState(() {
      isLoading = true;
    });
    try {
      var body = {"user_id": userId, "getcompanymappingdisplay": "1", "cuser_id": userId, "appversion": "3.0"};
      var response = await http.post(Uri.parse(ApiUtils.COMPANYLIST_URL), body: body);
      print("company response : ----" + response.body);
      if (response.statusCode == 200 || response.statusCode == 201) {
        CompanyListResponse logResponse = new CompanyListResponse.fromJson(json.decode(response.body));
        setState(() {
          isLoading = false;
        });
        if (logResponse.success == "1") {
          setState(() {
            for (int i = 0; i < logResponse.response.companyIds.length; i++) {
              List<String> arrFinal = logResponse.response.companyIds[i].split('\$*\$');
              CompanyModel companyModel = new CompanyModel(arrFinal[0].trim(), arrFinal[1].trim(), arrFinal[2].trim() == "1" ? true : false);
              companyList.add(companyModel);
            }
          });
        } else {
          scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(logResponse.message));
        }
      } else if (response.statusCode == 400) {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      } else {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      }
    } on SocketException catch (e) {
      print(e.message);
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.network_error));
    } on Exception catch (e) {
      print(e.toString());
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
    }
  }

  Future updateCompanyList() async {
    setState(() {
      isLoading = true;
    });
    try {
      List<String> selectCompany = [];
      for(int i = 0;i<companyList.length;i++){
        if(companyList[i].isSelected){
          selectCompany.add(companyList[i].id);
        }
      }
      var body = {"user_id": userId, "updatedisplaymapping": "1", "cuser_id": userId, "appversion": "3.0","company":selectCompany.join(',')};
      var response = await http.post(Uri.parse(ApiUtils.COMPANYLIST_URL), body: body);
      print("company response : ----" + response.body);
      if (response.statusCode == 200 || response.statusCode == 201) {
        ErrorResponse logResponse = new ErrorResponse.fromJson(json.decode(response.body));
        setState(() {
          isLoading = false;
        });
        if (logResponse.success == "1") {
          scaffoldGlobalKey.currentState.showSnackBar(SuccessSnakbar(logResponse.message));
        } else {
          scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(logResponse.message));
        }
      } else if (response.statusCode == 400) {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      } else {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      }
    } on SocketException catch (e) {
      print(e.message);
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.network_error));
    } on Exception catch (e) {
      print(e.toString());
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
    }
  }


  Future addCompany(String companyId) async {
    setState(() {
      isLoading = true;
    });
    try {
      var body = {"user_id": userId, "updatecompanymapping": "true", "cuser_id": userId, "company_id": companyId};
      var response = await http.post(Uri.parse(ApiUtils.COMPANYLIST_URL), body: body);
      print("company req param : ----" + body.toString());
      print("company update : ----" + response.body);
      if (response.statusCode == 200 || response.statusCode == 201) {
        CompanyListResponse logResponse = new CompanyListResponse.fromJson(json.decode(response.body));
        setState(() {
          isLoading = false;
        });
        if (logResponse.success == "1") {
          scaffoldGlobalKey.currentState.showSnackBar(SnackBar(content: Text(logResponse.message), backgroundColor: Colors.green));
        } else {
          scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(logResponse.message));
        }
      } else if (response.statusCode == 400) {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      } else {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      }
    } on SocketException catch (e) {
      print(e.message);
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.network_error));
    } on Exception catch (e) {
      print(e.toString());
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
    }
  }

  @override
  Widget build(BuildContext context) {
    print("on build===");
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      key: scaffoldGlobalKey,
      body: Stack(
        children: [
          Column(
            children: [
              Padding(
                padding: EdgeInsets.all(Constant.size9),
                child: Stack(
                  children: [
                    // Align(
                    //   alignment: Alignment.centerLeft,
                    //   child: InkWell(
                    //     child: Icon(FontAwesomeIcons.longArrowAltLeft),
                    //     onTap: () {
                    //      widget.dashBoardViewState.onManutap("dashboard", 0);
                    //     },
                    //   ),
                    // ),
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        "SELECT COMPANY",
                        style: TextStyle(fontFamily: 'fontawesome', fontSize: FontSize.s17, fontWeight: FontWeight.w600, color: AppColors.BLACKCOLOR),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: Constant.size0_5,
                color: AppColors.appLightGray,
              ),
              Expanded(
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  addSemanticIndexes: true,
                  itemCount: companyList.length,
                  itemBuilder: (context, index) {
                    return logItemView(/*promoList[index], */ index);
                  },
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: (){
                        widget.dashBoardViewState.navigateToDashboard();
                      },
                      child: Container(
                        margin: EdgeInsets.only(left: Constant.size15, right: Constant.size15, bottom: Constant.size12, top: Constant.size18),
                        padding: EdgeInsets.all(Constant.size8),
                        decoration: BoxDecoration(
                          color: AppColors.APPBLUE,
                        ),
                        child: Text(
                          "Cancel".toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: FontSize.s14, fontFamily: 'fontawesome', fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        List<String> selectCompany = [];
                        for(int i = 0;i<companyList.length;i++){
                          if(companyList[i].isSelected){
                            selectCompany.add(companyList[i].id);
                          }
                        }
                        if(selectCompany.length!=0)
                           updateCompanyList();
                        else
                          scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar("At least select one company"));

                      },
                      child: Container(
                        margin: EdgeInsets.only(left: Constant.size15, right: Constant.size15, bottom: Constant.size12, top: Constant.size18),
                        padding: EdgeInsets.all(Constant.size8),
                        decoration: BoxDecoration(
                          color: AppColors.APPBLUE,
                        ),
                        child: Text(
                          "Apply".toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: FontSize.s14, fontFamily: 'fontawesome', fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
          ProgressIndicatorLoader(AppColors.primary, isLoading)
        ],
      ),
    );
  }

  Widget logItemView(int index) {
    return Container(
      margin: EdgeInsets.only(left: Constant.size2, right: Constant.size2),
      child: Card(
        color: AppColors.progressPathColor,
        elevation: Constant.size4,
        //  shadowColor: Colors.black,
        borderOnForeground: true,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: [
            // Text(companyList[index].name,
            //     style: TextStyle(
            //         color: Colors.black,
            //         fontFamily: 'fontawesome',
            //         fontWeight: FontWeight.w700,
            //         fontSize: FontSize.s16)),
            // Checkbox(
            //   value: companyList[index].isSelected,
            //   checkColor: AppColors.darkBlueTop,
            //   activeColor: Colors.grey,
            //   onChanged: (newValue) {
            //       setState(() {
            //         companyList[index].isSelected = newValue;
            //       });
            //   },
            // ),
            Expanded(
              child: Theme(
                data: ThemeData(unselectedWidgetColor: AppColors.APPBLUE),
                child: CheckboxListTile(
                  title: Text(companyList[index].name,
                      style: TextStyle(color: AppColors.APPBLUE, fontFamily: 'fontawesome', fontWeight: FontWeight.w600, fontSize: FontSize.s14)),
                  value: companyList[index].isSelected,
                  checkColor: AppColors.WHITECOLOR,
                  activeColor: AppColors.APPBLUE,
                  controlAffinity: ListTileControlAffinity.leading,
                  onChanged: (newValue) {
                    setState(() {
                      // if (newValue == false) {
                      //   int count = 0;
                      //   for (int i = 0; i < companyList.length; i++) {
                      //     if (companyList[i].isSelected) {
                      //       count++;
                      //     }
                      //   }
                      //   if (count == 1) {
                      //     companyList[index].isSelected = true;
                      //   } else {
                      //     companyList[index].isSelected = newValue;
                      //     String selString = preferenceHelper.getCompanyData();
                      //     List<String> tempFilename = [];
                      //     if (selString != null) {
                      //       tempFilename = selString.split(",");
                      //     }
                      //     if (newValue == true) {
                      //       //      print("New value===true---"+newValue.toString());
                      //       for (int i = 0; i < tempFilename.length; i++) {
                      //         if (tempFilename[i] == companyList[index].id) {
                      //           tempFilename.removeAt(i);
                      //         }
                      //       }
                      //     } else {
                      //       //        print("New value===false---"+newValue.toString());
                      //       tempFilename.add(companyList[index].id);
                      //     }
                      //     preferenceHelper.saveCompanyData(tempFilename.join(","));
                      //   }
                      // } else {
                      //   companyList[index].isSelected = newValue;
                      //   String selString = preferenceHelper.getCompanyData();
                      //   List<String> tempFilename = [];
                      //   if (selString != null) {
                      //     tempFilename = selString.split(",");
                      //   }
                      //   if (newValue == true) {
                      //     //      print("New value===true---"+newValue.toString());
                      //     for (int i = 0; i < tempFilename.length; i++) {
                      //       if (tempFilename[i] == companyList[index].id) {
                      //         tempFilename.removeAt(i);
                      //       }
                      //     }
                      //   } else {
                      //     //        print("New value===false---"+newValue.toString());
                      //     tempFilename.add(companyList[index].id);
                      //   }
                      //   preferenceHelper.saveCompanyData(tempFilename.join(","));
                      // }
                      companyList[index].isSelected = newValue;
                    });
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget ErrorSnakbar(title) {
    return new SnackBar(
      backgroundColor: Colors.redAccent,
      content: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Icon(
            FontAwesomeIcons.exclamationTriangle,
            size: Constant.size15,
            color: Colors.white,
          ),
          SizedBox(
            width: Constant.size8,
          ),
          Expanded(
            child: Text(
              title,
              style: TextStyle(color: Colors.white, fontSize: FontSize.s15, fontFamily: 'fontawesome'),
              overflow: TextOverflow.ellipsis,
              maxLines: 10,
            ),
          ),
        ],
      ),
    );
  }

  Widget SuccessSnakbar(title) {
    return new SnackBar(
      backgroundColor: Colors.green,
      content: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Icon(
            FontAwesomeIcons.exclamationTriangle,
            size: Constant.size15,
            color: Colors.white,
          ),
          SizedBox(
            width: Constant.size8,
          ),
          Expanded(
            child: Text(
              title,
              style: TextStyle(color: Colors.white, fontSize: FontSize.s15, fontFamily: 'fontawesome'),
              overflow: TextOverflow.ellipsis,
              maxLines: 10,
            ),
          ),
        ],
      ),
    );
  }
}


import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ConstantString.dart';
import 'package:elegantj_bi_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/DashBoardView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HowWorks extends StatefulWidget {
  final DashBoardViewState dashBoardViewState;
  final String webUrl;

  // LogsView(this.dashBoardViewState,{Key key});
  HowWorks({Key key, this.dashBoardViewState,this.webUrl}) : super(key: key);

  @override
  _HowWorksState createState() => _HowWorksState();
}

class _HowWorksState extends State<HowWorks> {
  // Completer<WebViewController> _controller = Completer<WebViewController>();
  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    print("How it works url = "+widget.webUrl);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      key: scaffoldGlobalKey,
      body: Stack(
        children: [
          Column(
            children: [
              // Padding(
              //   padding: EdgeInsets.all(Constant.size9),
              //   child: Stack(
              //     children: [
              //       Align(
              //         alignment: Alignment.centerLeft,
              //         child: InkWell(
              //           child: Icon(FontAwesomeIcons.longArrowAltLeft),
              //           onTap: () {
              //             widget.dashBoardViewState.onManutap("dashboard", 0);
              //           },
              //         ),
              //       ),
              //       Align(
              //         alignment: Alignment.center,
              //         child: Text(
              //           "FAQs",
              //           style: TextStyle(
              //               fontFamily: 'fontawesome',
              //               fontSize: FontSize.s17,
              //               color: AppColors.BLACKCOLOR),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              // Container(
              //   height: Constant.size0_5,
              //   color: AppColors.appLightGray,
              // ),
              Expanded(
                child: InAppWebView(
                  initialUrlRequest: URLRequest(url: Uri.parse(widget.webUrl)),
                  // initialUrl: /*ConstantString.how_works_url+'?user_id='+DashBoardViewState.user_Id*/widget.webUrl,
                  initialOptions: InAppWebViewGroupOptions(
                    crossPlatform: InAppWebViewOptions(
                        clearCache: false,
                        supportZoom: true,
                        // debuggingEnabled: true,
                        useOnDownloadStart: true,
                        javaScriptEnabled: true,
                        verticalScrollBarEnabled: true,
                        useShouldInterceptAjaxRequest: true,
                        useShouldOverrideUrlLoading: true),
                      android: AndroidInAppWebViewOptions(
                        useWideViewPort: true,
                        allowContentAccess: true
                      ),
                      ios: IOSInAppWebViewOptions(
                        allowsInlineMediaPlayback: true,
                      )

                  ),
                  onWebViewCreated: (InAppWebViewController controller) {
                    DashBoardViewState.webViewController = controller;
                  },
                  shouldOverrideUrlLoading: (controller, request) async {
                    // return ShouldOverrideUrlLoadingAction.ALLOW;
                    return NavigationActionPolicy.ALLOW;
                  },
                  onProgressChanged: (InAppWebViewController controller, int progress) {
                    setState(() {
                      if (progress == 100) {
                        isLoading = false;
                      } else {
                        isLoading = true;
                      }
                      print("progress===" + progress.toString());
                    });
                  },
                ),
              ),
            ],
          ),
          ProgressIndicatorLoader(AppColors.primary, isLoading)
        ],
      ),
    );
  }
}

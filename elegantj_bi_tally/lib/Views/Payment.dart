import 'dart:async';

import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ConstantString.dart';
import 'package:elegantj_bi_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:elegantj_bi_tally/Helper/PreferenceHelper.dart';
import 'package:elegantj_bi_tally/Models/LoginResponse.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/DashBoardView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'IAP.dart';

//class Logs extends StatefulWidget {
//  final DashBoardViewState dashBoardViewState;
//
//  @override
//  _LogsState createState() => _LogsState();
//}
class Payment extends StatefulWidget {
  final DashBoardViewState dashBoardViewState;
  Widget currentView;

  // LogsView(this.dashBoardViewState,{Key key});
  Payment({Key key, this.dashBoardViewState}) : super(key: key);

  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  // Completer<WebViewController> _controller = Completer<WebViewController>();

  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;
  String clientId = "";
  String userId = "";
  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;
  TapGestureRecognizer _myTapGestureRecognizer;

  Future<void> getSharedPreferenceObject() async {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
      UserModel userModel = preferenceHelper.getUserData();
      clientId = userModel.clientId;
      userId = userModel.userId;
      // getLogHistory();
    });
  }

  @override
  void initState() {
    super.initState();
    getSharedPreferenceObject();
    print("on initState===");
    _myTapGestureRecognizer = TapGestureRecognizer()
      ..onTap = () {
        widget.dashBoardViewState.isFromPaymentHistory = true;
        //  widget.dashBoardViewState.onManutap("dashboard", 0);
        widget.dashBoardViewState.navigateToIAP();
      };
  }

  @override
  Widget build(BuildContext context) {
    print("on build===");
    return Scaffold(
      //resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      key: scaffoldGlobalKey,
      body: Stack(
        children: [
          Column(
            children: [
              // Padding(
              //   padding: EdgeInsets.all(Constant.size9),
              //   child: Stack(
              //     children: [
              //       Align(
              //         alignment: Alignment.centerLeft,
              //         child: InkWell(
              //           child: Icon(FontAwesomeIcons.longArrowAltLeft),
              //           onTap: () {
              //             widget.dashBoardViewState.onManutap("dashboard", 0);
              //           },
              //         ),
              //       ),
              //       Align(
              //         alignment: Alignment.center,
              //         child: Text(
              //           "PAYMENTS",
              //           style: TextStyle(
              //               fontFamily: 'fontawesome',
              //               fontSize: FontSize.s17,
              //               color: AppColors.BLACKCOLOR),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              // Container(
              //   height: Constant.size0_5,
              //   color: AppColors.appLightGray,
              // ),
              Expanded(
                  child:
                      // WebView(
                      //   initialUrl: 'http://35.154.99.45/tally_saas/mobile-app/mobile-payment.php?client_id='+ clientId + '?user_id=' + userId,
                      //
                      //  // initialUrl: 'http://35.154.99.45/tally_saas/mobile-app/mobile-payment.php',
                      //   onWebViewCreated: (WebViewController webViewController) {
                      //     _controller.complete(webViewController);
                      //   },
                      // )
                      InAppWebView(
                // initialUrl: ConstantString.base_url + 'mobile-payment.php?client_id=' + DashBoardViewState.client_Id + '&user_id=' + DashBoardViewState.user_Id,
                        initialUrlRequest: URLRequest(url: Uri.parse(ConstantString.base_url + 'mobile-payment.php?client_id=' + DashBoardViewState.client_Id + '&user_id=' + DashBoardViewState.user_Id)),

                        initialOptions: InAppWebViewGroupOptions(
                  crossPlatform: InAppWebViewOptions(
                      // debuggingEnabled: true,
                      useOnDownloadStart: true,
                      javaScriptEnabled: true,
                      verticalScrollBarEnabled: true,
                      useShouldOverrideUrlLoading: true),
                ),
                onWebViewCreated: (InAppWebViewController controller) {
                  DashBoardViewState.webViewController = controller;
                },
                shouldOverrideUrlLoading: (controller, request) async {
                  // return ShouldOverrideUrlLoadingAction.ALLOW;
                  return NavigationActionPolicy.ALLOW;
                },
                onProgressChanged:
                    (InAppWebViewController controller, int progress) {
                  setState(() {
                    if (progress == 100) {
                      isLoading = false;
                    } else {
                      isLoading = true;
                    }
                    print("progress===" + progress.toString());
                  });
                },
              )),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Expanded(

                       // alignment: Alignment.center,
                        child: Container(
                          height: 50,

                          padding: EdgeInsets.fromLTRB(0, 15, 0, 10),
                          decoration: BoxDecoration(
                            color: AppColors.appBlue,
                          ),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(children: [
                              TextSpan(
                                  text: 'Click here',
                                  style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,decoration: TextDecoration.underline),
                                  recognizer: _myTapGestureRecognizer),
                              TextSpan(
                                text: ' to choose your subscription plan',
                                style: TextStyle(color: Colors.white),
                              ),
                            ]),
                          ),
                        ),
                      )
                    ]),
//    Text("Choose subscription plans", style: TextStyle(
//                              fontFamily: 'fontawesome',
//                              fontSize: FontSize.s17,
//                              color: AppColors.BLACKCOLOR),),
              ),
//              Padding(
//                padding: const EdgeInsets.fromLTRB(15, 5, 15, 15),
//                child: TextButton(
//                  style: TextButton.styleFrom(
//                    primary: Colors.white,
//                    backgroundColor: Colors.blueAccent,
//                    onSurface: Colors.grey,
//                  ),
//                  child: Text("Subscribe"),
//                  onPressed: () {
//                    setState(() {
//                    widget.dashBoardViewState.onManutap("dashboard", 0);
//                });
////                    Navigator.pushReplacement(
////                context,
////                MaterialPageRoute(
////                    builder: (BuildContext context) => IAP()));
//
//                  },
//                ),
//              ),
            ],
          ),
          ProgressIndicatorLoader(AppColors.primary, isLoading)
        ],
      ),
    );
  }
}

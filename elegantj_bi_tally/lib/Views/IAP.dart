import 'dart:async';

import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ConstantString.dart';
import 'package:elegantj_bi_tally/Helper/PreferenceHelper.dart';
import 'package:elegantj_bi_tally/Models/LoginResponse.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/DashBoardView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_inapp_purchase/modules.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';

import 'package:flutter/services.dart';
import 'dart:convert';
import 'package:elegantj_bi_tally/Models/ErrorResponse.dart';
import 'package:elegantj_bi_tally/Utils/ApiUtils.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
// import 'package:package_info/package_info.dart';

class IAP extends StatefulWidget {
  final DashBoardViewState dashBoardViewState;
  Widget currentView;

  IAP({Key key, this.dashBoardViewState}) : super(key: key);

  @override
  _IAPState createState() => _IAPState();
}

class _IAPState extends State<IAP> {
  // Completer<WebViewController> _controller = Completer<WebViewController>();
  ObserverList<Function> _proStatusChangedListeners =
      new ObserverList<Function>();
  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;
  String clientId = "";
  String userId = "";
  String package_id = "";
  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;
  String subscriptionEndDate = "";
  String subscriptionStartDate = "";
  String subscriptionType = "";
  ObserverList<Function(String)> _errorListeners =
      new ObserverList<Function(String)>();
  String strPurchasedItem = "";
  var strType = "";
  var strDiff = "";
  var differenceInDays = 0;


  StreamSubscription _purchaseUpdatedSubscription;
  StreamSubscription _purchaseErrorSubscription;

//  final List<String> _productLists = Platform.isAndroid
//      ? [
//          'android.test.purchased',
//          'point_1000',
//          '5000_point',
//          'android.test.canceled',
//        ]
//      : ['SmartenTally', 'SmartenTally_WebMobile'];
  final List<String> _productLists = ['SmartenTally', 'SmartenTally_WebMobile'];
  final List<String> _productListsAndroid = ['smarten_tally',"smarten_tally_web_mobile"];
  String _platformVersion = 'Unknown';
  List<IAPItem> _items = [];
  List<PurchasedItem> _purchases = [];

  Future<void> getSharedPreferenceObject() async {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
      UserModel userModel = preferenceHelper.getUserData();
      clientId = userModel.clientId;
      userId = userModel.userId;

      setState(() {
        subscriptionEndDate = preferenceHelper.getSubscriptionEndDate();
        //subscriptionEndDate = userModel.subscriptionEndDate.toString();
        subscriptionStartDate = userModel.subscriptionStartDate.toString();
        subscriptionType = userModel.subscriptionType;
        package_id = preferenceHelper.getPackageID();
      //  package_id = userModel.package_id;
      });
      initData();
      _getProduct();
      _purchaseUpdatedSubscription = FlutterInappPurchase.purchaseUpdated.listen(_handlePurchaseUpdate);
      // _getPurchaseHistory();
    });
    print("subscriptionEndDate==="+subscriptionEndDate);

//    SharedPreferences pref = await SharedPreferences.getInstance();
//    Map json = jsonDecode(pref.getString('subscription_end_date'));
//    var user = UserModel.fromJson(json);
//    subscriptionEndDate = user.toString();
//
//    print(subscriptionEndDate);

  }

  @override
  void initState() {
    super.initState();
    getSharedPreferenceObject();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await FlutterInappPurchase.instance.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // prepare
    var result = await FlutterInappPurchase.instance.initConnection;
    print('result: $result');

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });

    // refresh items for android
    try {
      String msg = await FlutterInappPurchase.instance.consumeAllItems;
      print('consumeAllItems: $msg');
    } catch (err) {
      print('consumeAllItems error: $err');
    }

    _purchaseUpdatedSubscription =
        FlutterInappPurchase.purchaseUpdated.listen((productItem) {
      print('purchase-updated: $productItem');
    });

    _purchaseErrorSubscription =
        FlutterInappPurchase.purchaseError.listen((purchaseError) {
      print('purchase-error: $purchaseError');
    });
  }

  void _requestPurchase(IAPItem item) {
     isLoading = true;
     FlutterInappPurchase.instance.requestPurchase(item.productId);
     isLoading = false;
  }

  Future<Null> _getProduct() async {
    var result = await FlutterInappPurchase.instance.initConnection;
    print('result: $result');
    List<IAPItem> items = [];
    if(Platform.isIOS)
    items = await FlutterInappPurchase.instance.getProducts(_productLists);
    else
    items = await FlutterInappPurchase.instance.getSubscriptions(_productListsAndroid);
    print("this._items list ==="+items.toString());
    for (var item in items) {
      print('${item.toString()}');
      this._items.add(item);
    }

    setState(() {
      this._items = items;
      //this._purchases = [];
    });
    _getPurchases();
  }

  Future<Null> _getPurchases() async {
    print('purchase');

    List<PurchasedItem> items = await FlutterInappPurchase.instance.getAvailablePurchases();
    for (var item in items) {
      print('${item.toString()}');
      this._purchases.add(item);
    }
    print('_getPurchases');
    print(_purchases);
    setState(() {
      this._purchases = items;
    });
  }

  void initData() {
    print(this._items);
    double screenWidth = MediaQuery.of(context).size.width - 20;
    double buttonWidth = (screenWidth / 3) - 20;
    print("subscriptionEndDate=="+subscriptionEndDate.toString());

    DateTime dateTimeCreatedAt = DateTime.parse(subscriptionEndDate.toString());
    DateTime dateTimeNow = DateTime.now();
    var now = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd');
    String formattedDate = formatter.format(now);
    print(formattedDate);
    final String formatted = formatter.format(DateTime.parse(subscriptionEndDate.toString()));
    DateTime tempDate = new DateFormat("yyyy-MM-dd").parse(formatted);
    DateTime tempDate1 = new DateFormat("yyyy-MM-dd").parse(formattedDate);

    setState(() {
      differenceInDays = tempDate.difference(tempDate1).inDays;
    });

    print(DateTime.now());
    print("dateTimeCreatedAt=="+dateTimeCreatedAt.toString());
    print("differenceInDays=="+differenceInDays.toString());


    setState(() {
      if (package_id == '2') {
        Platform.isIOS?strType = "SmartenTally":strType = "smarten_tally";
      } else if (package_id == '3') {
        Platform.isIOS?strType = "SmartenTally_WebMobile":strType = "smarten_tally_web_mobile";
      } else {
        strType = '';
      }
    });

    var strSelectedPlan = "";
    if (package_id == '0') {
      strSelectedPlan = "Free Web & Mobile App";
    } else if (package_id == '1') {
      strSelectedPlan = 'Web Only';
    } else if (package_id == '2') {
      strSelectedPlan = 'Mobile App Only';
    } else {
      strSelectedPlan = "Paid Web & Mobile App";
    }
    final differenceInDaysCount = dateTimeNow.difference(dateTimeCreatedAt).inDays;
    var dayCount = 'has expired a day ago. ';
    if (differenceInDaysCount == 1){
      dayCount = 'has expired a day ago. ';
    }else if(differenceInDaysCount <= 2){
      dayCount = 'will expire in next few days.';
    }else{
      dayCount = 'has expired ' + differenceInDaysCount.toString() + ' days ago. ';
    }
    setState(() {
      if (differenceInDays == 0) {
        strDiff = "You have already subscribed to " + strSelectedPlan + ' plan.' + 'Your subscription has been ended today. Please choose and subscribe to a plan.';
      }
      else if (differenceInDays <= 2 && differenceInDays >= 1) {
        strDiff = "You have already subscribed to " + strSelectedPlan + ' plan.' + 'Your subscription is ending in next few days.';
      }else if (differenceInDays > 2) {
        strDiff = "You have already subscribed to " + strSelectedPlan + ' plan.' + 'Your subscription is ending in ' + differenceInDays.toString() +  ' days.';
      }else{
        strDiff =
        //'Your subscription has expired, please choose and subscribe to a plan.';
        'You had subscribed to ' + strSelectedPlan + ' plan. Your subscription ' + dayCount + 'Please choose and subscribe to a plan.';
      }
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        //padding: EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              height: Constant.size4,
              //color: AppColors.appLightGray,
            ),

            Container(
              height: 50.0,
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: Stack(

                alignment: Alignment.center,
                children: [
                  // differenceInDays < 0 ? Align() :
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      child: Icon(FontAwesomeIcons.longArrowAltLeft),
                      onTap: () {

//                      isPaymentFromIAP = true;
//                      widget.dashBoardViewState.isFromPaymentHistory = false;
//                      widget.dashBoardViewState.isDesktopVerification = true;
//                      widget.dashBoardViewState.isPayment = false;
//
//                      widget.dashBoardViewState.getCompanyListForDashboard();
//                      widget.dashBoardViewState.MenuList.clear();
//                      widget.dashBoardViewState.setMenuList();

                        widget.dashBoardViewState.navigateToPayment();

                        //widget.dashBoardViewState.onManutap("payments", 2);
                      },
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text("Subscription".toUpperCase(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: FontSize.s17,
                            color: AppColors.BLACKCOLOR,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'fontawesome')),
                  ),
                ],
              ),
            ),

//            Align(
//              alignment: Alignment.centerLeft,
//              child: InkWell(
//                  child: Icon(FontAwesomeIcons.longArrowAltLeft),
//                  onTap: () {
//                    isPaymentFromIAP = true;
//                    widget.dashBoardViewState.isDesktopVerification = true;
//                    widget.dashBoardViewState.isPayment = false;
//
//                    widget.dashBoardViewState.getCompanyListForDashboard();
//                    widget.dashBoardViewState.MenuList.clear();
//                    widget.dashBoardViewState.setMenuList();
//                  }),
//            ),
//            Align(
//              alignment: Alignment.center,
//              child: Text(
//                "Subscription".toUpperCase(),
//                style: TextStyle(
//                    fontFamily: 'fontawesome',
//                    fontSize: FontSize.s18,
//                    fontWeight: FontWeight.w600,
//                    color: AppColors.BLACKCOLOR),
//              ),
//            ),
            Container(
              height: Constant.size4,
              //color: AppColors.appLightGray,
            ),
            Container(
              height: Constant.size0_5,
              color: AppColors.appLightGray,
            ),
            Container(
              height: Constant.size15,
              // color: AppColors.appLightGray,
            ),
            Container(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),

              child: Align(
                alignment: Alignment.center,
                child: Text(
                  // ignore: unrelated_type_equality_checks
                  //differenceInDays > 0 ?

                      strDiff ,
                      //: strDiff,
                  style: TextStyle(
                    fontFamily: 'open-sens',
                    fontSize: FontSize.s16,
                    color: AppColors.BLACKCOLOR,
                    fontWeight: FontWeight.normal,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            /*Container(
              height: Constant.size15,
             // color: AppColors.appLightGray,
            ),

            Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[

                  Card(child:Container(
                    decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.blueAccent),
                     // borderRadius: const BorderRadius.all(const Radius.circular(8)),
                    ),
                    margin: const EdgeInsets.all(0),
                    child: ListTile(tileColor: const Color(0xffE6F1FF),
                      title: Text("Subscription type : " + subscriptionType.toUpperCase(),
                          style: TextStyle(fontWeight: FontWeight.w600 ,fontFamily: 'fontawesome',fontSize: FontSize.s15)),
                      subtitle: Text("Start date : " + subscriptionStartDate + "\nEnd date : " + subscriptionEndDate, style: TextStyle(fontFamily: 'fontawesome',fontSize: FontSize.s13)),
                        //isThreeLine: true,

                      )
      ),
                  ),

                ],
              ),
            ),*/
            Container(
              height: Constant.size10,
              // color: AppColors.appLightGray,
            ),
            /* Expanded(
           child: GridView.builder(
             //shrinkWrap: true,

             itemCount: this._items.length,
             gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                 crossAxisCount: 2),
             itemBuilder: (BuildContext context, int index) {
               return new Card(
                 child: new GridTile(

                   header: Container(
                       padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                     child: new Text(this._items[index].title + ' - ' + this._items[index].localizedPrice + '/' + this._items[index].subscriptionPeriodUnitIOS, style: TextStyle(
                         fontFamily: 'fontawesome',
                         fontSize: FontSize.s15,
                         color: AppColors.BLACKCOLOR,
                         fontWeight: FontWeight.w600)),
                   ),
                   child: Container(
                     padding: EdgeInsets.fromLTRB(10, 50, 10, 10),
                     child: Text(this._items[index].description, style: TextStyle(
                         fontFamily: 'fontawesome',
                         fontSize: FontSize.s14,
                         color: AppColors.appLightGray)),
                   ),
                   footer: Container(
                     padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                     child: TextButton(
                       style: TextButton.styleFrom(
                         primary: Colors.white,
                         backgroundColor: const Color(0xff0E64FE),
                         //  Color(88B475),
                         onSurface: Colors.grey,
                       ),
                       child: Text("Buy"),
                       onPressed: () {
                         widget.currentView = Container();

                         //this._requestPurchase(this._items[index]);
                       },
                     ),
                   ),

//                   new Text(this._items[index].description, style: TextStyle(
//                       fontFamily: 'fontawesome',
//                       fontSize: FontSize.s13,
//                       color: AppColors.appLightGray)),
                   //just for testing, will fill with image later
                 ),
               );
             },
           ),
        ),*/

              Expanded(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: ListView.separated(
                      shrinkWrap: true,
                      itemCount: _items.length,
                      separatorBuilder: (context, index) {
                        return Divider(
                          height: 1,
                          color: Colors.grey,
                        );
                      },
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
//                    decoration: BoxDecoration(
//                      border: Border.all(width: 1, color: Colors.blueAccent),
//                      //borderRadius: const BorderRadius.all(const Radius.circular(8)),
//                    ),
                          margin: EdgeInsets.all(10),
                          //height: 100,
                          child: Padding(
                            padding: EdgeInsets.only(right: 8, bottom: 8),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              children: [
//                          _items[index].productId != strType
//                              ? SizedBox(
//                                  height: 5,
//                                  width: 20,
//                                )
//                              : Container(
//                                  height: 20,
//                                  width: 20,
//                                  color: Colors.green,
//                                  child: Center(
//                                    child: FaIcon(
//                                      FontAwesomeIcons.check,
//                                      size: 10,
//                                      color: Colors.white,
//                                    ),
//                                  ),
//                                ),
//                          SizedBox(width: 5),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      SizedBox(height: 8),
                                      Text(_items[index].title.substring(0,_items[index].title.indexOf("(")),
//                                        +
//                                        ' - ' +
//                                        _items[index].localizedPrice +
//                                        '/' +
//                                        _items[index].subscriptionPeriodUnitIOS,
                                          style: TextStyle(
                                              fontFamily: 'open-sens',
                                              fontSize: FontSize.s13,
                                              color: AppColors.BLACKCOLOR,
                                              fontWeight: FontWeight.w600)),
                                      SizedBox(height: 5),
                                      Text(
                                          _items[index].localizedPrice +
                                              '/' +
                                              (Platform.isIOS?_items[index].subscriptionPeriodUnitIOS:_items[index].subscriptionPeriodAndroid),
                                          style: TextStyle(
                                              fontFamily: 'open-sens',
                                              fontSize: FontSize.s13,
                                              color: AppColors.BLACKCOLOR,
                                              fontWeight: FontWeight.w600)),
                                      SizedBox(height: 5),
                                      Text(_items[index].description,
                                          style: TextStyle(
                                              fontFamily: 'open-sens',
                                              fontSize: FontSize.s13,
                                              color: AppColors.appLightGray)),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),

                                _items[index].productId != strType
                                    ? TextButton(
                                        style: TextButton.styleFrom(
                                          primary: _items[index].productId != strType
                                              ? Colors.white
                                              : Colors.green,
                                          backgroundColor:
                                              _items[index].productId != strType
                                                  ? const Color(0xff0E64FE)
                                                  : Colors.white,
                                          //  Color(88B475),
                                          onSurface: Colors.grey,
                                        ),
                                        child: Text(differenceInDays < 0 ? "Buy" :
                                            _items[index].productId != strType
                                                ? "Buy"
                                                : "Subscribed" ,
                                            style: TextStyle(
                                                fontFamily: 'open-sens',
                                                fontSize: FontSize.s13,
                                                color: _items[index].productId !=
                                                        strType
                                                    ? AppColors.whiteTransperentColor
                                                    : AppColors.appGreen,
                                                fontWeight: FontWeight.w600)),
                                        onPressed: () {
                                          _items[index].productId == strType
                                              ? null
                                              :
//                                    isPaymentFromIAP = true;
//                                    widget.dashBoardViewState
//                                        .isDesktopVerification = true;
//                                    widget.dashBoardViewState.isPayment = true;
//
//                                    widget.dashBoardViewState
//                                        .getCompanyListForDashboard();
//                                    widget.dashBoardViewState.MenuList.clear();
//                                    widget.dashBoardViewState.setMenuList()
                                          this._requestPurchase(_items[index]);
                                        },
                                      )
                                    : TextButton(
                                        style: TextButton.styleFrom(
                                          primary: _items[index].productId != strType
                                              ? Colors.white
                                              : Colors.green,
                                          backgroundColor: differenceInDays < 0 ? const Color(0xff0E64FE) :
                                              _items[index].productId != strType
                                                  ? const Color(0xff0E64FE)
                                                  : Colors.white,
                                          //  Color(88B475),
                                          onSurface: Colors.grey,
                                        ),
                                        child: Text(differenceInDays < 0 ? "Buy" :
                                        _items[index].productId != strType
                                            ? "Buy"
                                            : "Subscribed",
//                                        _items[index].productId != strType
//                                            ? "Buy"
//                                            : "Subscribed",
                                            style: TextStyle(
                                                fontFamily: 'open-sens',
                                                fontSize: FontSize.s13,
                                                color: differenceInDays < 0 ? AppColors.whiteTransperentColor : _items[index].productId !=
                                                        strType
                                                    ? AppColors.whiteTransperentColor
                                                    : AppColors.appGreen,
                                                fontWeight: FontWeight.w600)),
                                        onPressed: null,
                                      ),
                                /* TextButton(

                                  style: TextButton.styleFrom(

                                    primary: _items[index].productId != strType
                                        ? Colors.white : Colors.green,
                                    backgroundColor: _items[index].productId != strType
                                        ? const Color(0xff0E64FE) : Colors.white,
                                    //  Color(88B475),
                                    onSurface: Colors.grey,
                                  ),


                                  child: Text(_items[index].productId != strType
                                      ? "Buy"
                                      : "Subscribed",style: TextStyle(
                                      fontFamily: 'open-sens',
                                      fontSize: FontSize.s13,
                                      color: _items[index].productId != strType ? AppColors.whiteTransperentColor : AppColors.appGreen,
                                      fontWeight: FontWeight.w600)),
                                  onPressed: () {
                                    _items[index].productId == strType
                                        ? null
                                        : isPaymentFromIAP = true;
                                    widget.dashBoardViewState.isDesktopVerification =
                                        true;
                                    widget.dashBoardViewState.isPayment = true;

                                    widget.dashBoardViewState
                                        .getCompanyListForDashboard();
                                    widget.dashBoardViewState.MenuList.clear();
                                    widget.dashBoardViewState.setMenuList();
                                    // this._requestPurchase(_items[index]);
                                  },

                                )*/
                              ],
                            ),
                          )
                          /*ListTile(leading: index == 1 ? SizedBox(height: 5) : Column(

                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                FaIcon(FontAwesomeIcons.solidCheckSquare,size: 15,color: Colors.green,),
                              ],
                            ),
                                tileColor: Colors.white, title: Text(_items[index].title + ' - ' + _items[index].localizedPrice + '/' + _items[index].subscriptionPeriodUnitIOS, style: TextStyle(
                                    fontFamily: 'fontawesome',
                                    fontSize: FontSize.s13,
                                    color: AppColors.BLACKCOLOR,
                                    fontWeight: FontWeight.w600)),

                                subtitle: Text(_items[index].description, style: TextStyle(
                                    fontFamily: 'fontawesome',
                                    fontSize: FontSize.s13,
                                    color: AppColors.appLightGray))
                                ,trailing:TextButton(
                                  style: TextButton.styleFrom(
                                    primary: Colors.white,
                                    backgroundColor: const Color(0xff0E64FE),
                                    //  Color(88B475),
                                    onSurface: Colors.grey,
                                  ),
                                  child: Text("Buy"),
                                  onPressed: () {
                                    isPaymentFromIAP = true;
                                    widget.dashBoardViewState.isDesktopVerification = true;
                                    widget.dashBoardViewState.isPayment = true;

                                    widget.dashBoardViewState.getCompanyListForDashboard();
                                    widget.dashBoardViewState.MenuList.clear();
                                    widget.dashBoardViewState.setMenuList();
                                    //widget.currentView = Container();
                                    //this._requestPurchase(item);
                                  },
                                ))*/
                          ,
                        );
                      }
                      //children: this._renderInApps(),
                      ),
                ),
              ),
          /*  differenceInDays > 0 ?
            Align(
              alignment: Alignment.center,
              child: TextButton(
                style: TextButton.styleFrom(
                  primary: Colors.white,
                  backgroundColor: Color(0xff0E64FE),
                  onSurface: Colors.grey,
                ),
                child: Text(
                   "Continue to Dashboard",
                    style: TextStyle(
                        fontFamily: 'open-sens',
                        fontSize: FontSize.s13,
                        color:AppColors.whiteTransperentColor,
                        fontWeight: FontWeight.w600)),
                onPressed:(){
                  isPaymentFromIAP = true;
                  widget.dashBoardViewState
                      .isDesktopVerification = true;
                  widget.dashBoardViewState.isPayment = true;
                  widget.dashBoardViewState
                      .isFromPaymentHistory = false;
                  widget.dashBoardViewState
                      .getCompanyListForDashboard();
                  widget.dashBoardViewState.MenuList.clear();
                  widget.dashBoardViewState.setMenuList();
                },

              ),
            ) : Align(),*/
//              Column(
//                children: this._renderPurchases(),
//              ),
          ],
        ),
      ),
    );
  }

  void _handlePurchaseUpdate(PurchasedItem productItem) async {
//    if (Platform.isAndroid) {
//      await _handlePurchaseUpdateAndroid(productItem);
//    } else {
      await _handlePurchaseUpdateIOS(productItem);
    //}
  }

  Future<void> _handlePurchaseUpdateIOS(PurchasedItem purchasedItem) async {
    print("_handlePurchaseUpdateIOS");
    print("PurchasedItem---"+purchasedItem.toString());

    if(Platform.isIOS) {
      switch (purchasedItem.transactionStateIOS) {
        case TransactionState.deferred:
          print("deferred");
          break;
        case TransactionState.failed:
        // _callErrorListeners("Transaction Failed");
          print("failed");
          FlutterInappPurchase.instance.finishTransaction(purchasedItem);
          break;
        case TransactionState.purchased:
          print("purchased");
          await _verifyAndFinishTransaction(purchasedItem);
          break;
        case TransactionState.purchasing:
          print("purchasing");
          Platform.isIOS ? FlutterInappPurchase.instance.finishTransactionIOS(purchasedItem.transactionId) : FlutterInappPurchase.instance.finishTransaction(
              purchasedItem);
          //  InAppPurchaseConnection.instance.completePurchase(purchase);
          break;
        case TransactionState.restored:
          print("restored transact");
          FlutterInappPurchase.instance.finishTransaction(purchasedItem);

          //FlutterInappPurchase.instance.finishTransaction(purchasedItem);
          //  await _verifyAndFinishTransaction(purchasedItem);

          break;
        default:
      }
    }
    else{
      switch (purchasedItem.purchaseStateAndroid) {
        case PurchaseState.unspecified:
          print("deferred");
          break;
        case PurchaseState.purchased:
          print("purchased");
          await _verifyAndFinishTransaction(purchasedItem);
          break;
        default:
      }
    }
  }


  _verifyAndFinishTransaction(PurchasedItem purchasedItem) async {
    print("finish");
    bool isValid = false;
    // try {
    // Call API
    isValid = await _verifyPurchase(purchasedItem);
    // }
    print('purchasedItem');
//    isPaymentFromIAP = false;
//    widget.dashBoardViewState.isDesktopVerification = false;
//    widget.dashBoardViewState.isPayment = true;
//  //  widget.dashBoardViewState.subscription_endDate = response.body;
//  //  widget.dashBoardViewState.getCompanyListForDashboard();
//    widget.dashBoardViewState.MenuList.clear();
//    widget.dashBoardViewState.setMenuList();
//    widget.dashBoardViewState.navigateToDashboard();

    //if (isValid) {
      //FlutterInappPurchase.instance.finishTransaction(purchasedItem);
      print('API Call');
      Platform.isIOS?
      doTrasaction(purchasedItem.transactionId, "1", purchasedItem.productId == "SmartenTally" ? "2" : "3"):doTrasaction(purchasedItem.transactionId, "1", purchasedItem.productId == "smarten_tally" ? "2" : "3");
      // save in sharedPreference here
      //_callProStatusChangedListeners();
//    } else {
//      print('Fail');
//
//      doTrasaction(purchasedItem.transactionId, "0",
//          purchasedItem.productId == "SmartenTally" ? "2" : "3");
//
//      //_callErrorListeners("Varification failed");
//    }
  }

  @override
  void dispose() {
    _purchases.clear();
    super.dispose();
  }

  Future doTrasaction(String transactionID, String status, String type) async {
    setState(() {
      isLoading = true;
    });
    try {
      // PackageInfo packageInfo = await PackageInfo.fromPlatform();
      strPurchasedItem = type;
      var body = {
        "user_id": userId,
        "action": "payment",
        "method": "Online",
        "order_id": transactionID,
        "payment_status": status,
        "subscription": type,
        "payment_from": Platform.isIOS?"Apple":"Google",
        // "ostype": Platform.isAndroid?"Android":"iOS"
      };
      print("payment req param : ----" + body.toString());
      print("payment request URL : ----" + ApiUtils.PAYMENT_URL);
      var response =
          await http.post(Uri.parse(ApiUtils.PAYMENT_URL), body: body);
      print("Payment response : ----" + response.body);
      print(response.statusCode);
      if (response.statusCode == 200 || response.statusCode == 201) {
        ErrorResponse errorResponse = new ErrorResponse.fromJson(json.decode(
            response.body
                .replaceFirst("associatedcompany", '"associatedcompany"')));
        setState(() {
          isLoading = false;
        });
        print('successs');

        if (errorResponse.success == "1") {
          print('successs11111');
          PaymentSuccess studentResponse;


          studentResponse = new PaymentSuccess.fromJson(json.decode(response.body));
          preferenceHelper.savePackageID(studentResponse.response.packageID);
//          SuccessTransact successTransact = new SuccessTransact.fromJson(json.decode(
//              response.body));
          preferenceHelper.saveSubscriptionEndDate(studentResponse.response.subscriptionEndDate.toString());
          print('preferenceHelper.getSubscriptionEndDate(): $preferenceHelper.getSubscriptionEndDate()');
          print(preferenceHelper.getPackageID());

          print(preferenceHelper.getSubscriptionEndDate());
          setState(() {
            isPaymentFromIAP = true;
            widget.dashBoardViewState.isDesktopVerification = true;
            widget.dashBoardViewState.isPayment = true;
            widget.dashBoardViewState.isFromPaymentHistory = false;
            widget.dashBoardViewState.getCompanyListForDashboard();
            widget.dashBoardViewState.MenuList.clear();
            widget.dashBoardViewState.setMenuList();
          });
        } else {
          scaffoldGlobalKey.currentState
              .showSnackBar(ErrorSnakbar(errorResponse.message));
        }
      } else if (response.statusCode == 400) {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState
            .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      } else {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState
            .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      }
    } on SocketException catch (e) {
      print(e.message);
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState
          .showSnackBar(ErrorSnakbar(ConstantString.network_error));
    } on Exception catch (e) {
      print(e.toString());
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState
          .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
    }
  }
  /*Future doLogin() async {
    setState(() {
      isLoading = true;
    });
    try {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      var body = {
        "email_phone": preferenceHelper.getUserData().userId,
        "password": _etPassword.text.trim(),
        "login_with": "M",
        "appversion": packageInfo.version,
        "firebase": fcmToken,
        "app_version": "3.0",
        // "ostype": Platform.isAndroid?"Android":"iOS"
      };
      var response =
      await http.post(ApiUtils.LOGIN_URL), body: body);
      print("Login response : ----" + response.body);
      if (response.statusCode == 200 || response.statusCode == 201) {
        ErrorResponse errorResponse = new ErrorResponse.fromJson(json.decode(
            response.body
                .replaceFirst("associatedcompany", '"associatedcompany"')));
        setState(() {
          isLoading = false;
        });
        if (errorResponse.success == "1") {
          isPaymentFromIAP = false;
          studentResponse = new LoginResponse.fromJson(json.decode(response.body
              .replaceFirst("associatedcompany", '"associatedcompany"')));
          scaffoldGlobalKey.currentState.showSnackBar(SnackBar(
              content: Text(studentResponse.message),
              backgroundColor: Colors.green));
          preferenceHelper.saveUserData(studentResponse.response);
          preferenceHelper.saveIsUserLoggedIn(true);

          preferenceHelper.saveSubscriptionEndDate(studentResponse.response.subscriptionEndDate.toString());


          var now = new DateTime.now();
          var formatter = new DateFormat('yyyy-MM-dd hh:mm:ss');
          String formattedDate = formatter.format(now);
          print(formattedDate);
          //  DateTime dateTimeCreatedAt = DateTime.parse(
          //studentResponse.response.subscriptionEndDate.toString());
          DateTime dateTimeCreatedAt = DateTime.parse(preferenceHelper.getSubscriptionEndDate());
          DateTime dateTimeNow = DateTime.now();
          final differenceInDays =
              dateTimeNow.difference(dateTimeCreatedAt).inDays;
          print(differenceInDays);
          //if (differenceInDays > 0) {
//            Navigator.pushReplacement(
//                context,
//                MaterialPageRoute(
//                    builder: (BuildContext context) => IAP()));
//          }
//          else {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => DashBoardView()));
//          }
        } else {
          scaffoldGlobalKey.currentState
              .showSnackBar(ErrorSnakbar(errorResponse.message));
        }
      } else if (response.statusCode == 400) {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState
            .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      } else {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState
            .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      }
    } on SocketException catch (e) {
      print(e.message);
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState
          .showSnackBar(ErrorSnakbar(ConstantString.network_error));
    } on Exception catch (e) {
      print(e.toString());
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState
          .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
    }
  }*/

  Widget ErrorSnakbar(title) {
    return new SnackBar(
      backgroundColor: Colors.redAccent,
      content: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Icon(
            FontAwesomeIcons.exclamationTriangle,
            size: Constant.size15,
            color: Colors.white,
          ),
          SizedBox(
            width: Constant.size8,
          ),
          Expanded(
            child: Text(
              title,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: FontSize.s15,
                  fontFamily: 'fontawesome'),
              overflow: TextOverflow.ellipsis,
              maxLines: 10,
            ),
          ),
        ],
      ),
    );
  }

  Future<bool> _verifyPurchase(PurchasedItem purchasedItem) async {
    bool isValid = false;
    print('_verifyPurchase');
    // Backend will require this to contact respective server(Google / Apple)
   // final osPlatform = Platform.isAndroid ? 'android' : 'ios';
    final osPlatform = 'ios';

    // For Android verification will bes based on purchaseToken and for iOS transactionReceipt
//    final purchaseToken = Platform.isAndroid
//        ? purchasedItem.purchaseToken
//        : purchasedItem.transactionReceipt;
    final purchaseToken =  purchasedItem.transactionReceipt;
    print('purchaseToken');

    IAPItem iapItem = _items.firstWhere((element) => element.productId == purchasedItem.productId);
    var receiptBody = {
      'receipt-data': purchasedItem.transactionReceipt,
      'password': '******'
    };
    var result = validateReceiptIos(receiptBody: receiptBody);

//    IAPItem iapItem = _products.firstWhere((element) => element.productId == purchasedItem.productId);
//
//    UserPackageType userPackageType = _isMonthly(iapItem) ? UserPackageType.MONTHLY : UserPackageType.ANNUAL;
//
//    PaymentTransaction paymentTransaction = PaymentTransaction(
//      orderId: purchasedItem.orderId,
//      transactionId: purchasedItem.transactionId,
//      osPlatform: osPlatform,
//      purchaseToken: purchaseToken,
//      packageType: userPackageType,
//      productId: purchasedItem.productId,
//      userId: Constants.userId,
//      amount: double.parse(iapItem.price),
//    );
//    // HTTP call to backend with requied info
//    isValid =
//    await PaymentRepository.getInstance().verifyReciept(paymentTransaction);

    return isValid;
  }

  Future<http.Response> validateReceiptIos({
    Map<String, String> receiptBody,
    bool isTest = true,
  }) async {
    assert(receiptBody != null);
    assert(isTest != null);

    final String url = isTest
        ? 'https://sandbox.itunes.apple.com/verifyReceipt'
        : 'https://buy.itunes.apple.com/verifyReceipt';
    return await http.post(
      Uri.parse(url),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: json.encode(receiptBody),
    );
    print(url);
  }

//  _verifyAndFinishTransaction(PurchasedItem purchasedItem) async {
//    bool isValid = false;
//    try {
//      // Call API
//      isValid = await _verifyPurchase(purchasedItem);
//    } on Exception {
//      _callErrorListeners("Something went wrong");
//      return;
//    }
//
//    if (isValid) {
//      FlutterInappPurchase.instance.finishTransaction(purchasedItem);
//      _isProUser = true;
//      // save in sharedPreference here
//      _callProStatusChangedListeners();
//      print('purchasedItemList');
//
//      print(purchasedItem);
//
//    } else {
//      _callErrorListeners("Varification failed");
//    }
//  }
  void _callProStatusChangedListeners() {
    _proStatusChangedListeners.forEach((Function callback) {
      callback();
    });
  }

  /// Call this method to notify all the subsctibers of _errorListeners
  void _callErrorListeners(String error) {
    _errorListeners.forEach((Function callback) {
      callback(error);
    });
  }
}

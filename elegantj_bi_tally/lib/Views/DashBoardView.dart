import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ConstantString.dart';
import 'package:elegantj_bi_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:elegantj_bi_tally/Helper/PreferenceHelper.dart';
import 'package:elegantj_bi_tally/Models/CompanyListResponse.dart';
import 'package:elegantj_bi_tally/Models/LogResponse.dart';
import 'package:elegantj_bi_tally/Models/MenuModel.dart';
import 'package:elegantj_bi_tally/Utils/ApiUtils.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/ComplanyList.dart';
import 'package:elegantj_bi_tally/Views/HowWorks.dart';
import 'package:elegantj_bi_tally/Views/InviteFriends.dart';
import 'package:elegantj_bi_tally/Views/SignInView.dart';
import 'package:elegantj_bi_tally/Views/SmartenApps.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'package:url_launcher/url_launcher.dart';

// import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:elegantj_bi_tally/Views/Logs.dart';
import 'package:elegantj_bi_tally/Views/Payment.dart';
import 'package:elegantj_bi_tally/Views/FaqView.dart';
import 'package:elegantj_bi_tally/Views/IAP.dart';
import 'IAP_New.dart';
import 'package:http/http.dart' as http;

import 'AboutUs.dart';
import 'CustomWebView1.dart';
import 'Support.dart';

import 'package:upgrader/upgrader.dart';

bool isPaymentFromIAP = false;

// ignore: must_be_immutable
class DashBoardView extends StatefulWidget {
  DashBoardView({Key key}) : super(key: key);
  Widget currentView;
  bool isWebLoading = false;

  @override
  DashBoardViewState createState() => DashBoardViewState();
}

class DashBoardViewState extends State<DashBoardView> {
  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();
  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;
  bool isLoading = false;
  bool isDesktopVerification = false;
  bool isPayment = false;
  bool isFromPaymentHistory = false;
  // final flutterWebviewPlugin = new FlutterWebviewPlugin();

  // WebViewController webViewController;
  List<MenuModel> MenuList = [];
  String fullName = "", company = "", currentDate = "", webUrl = "";
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  static String client_Id = "";
  static String user_Id = "";
  static String lastAttemp = "";
  String subscription_endDate = "";

  List<LogModel> LogsList = [];
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  // List<CompanyModel> companyList = [];
  NotificationDetails platformChannelSpecifics;
  static InAppWebViewController webViewController;
  int exitCounter = 0;
  final InAppReview _inAppReview = InAppReview.instance;
  final appcast = Appcast();
  String _homeScreenText = "Waiting for token...";
  String _messageText = "Waiting for message...";

  /// Create a [AndroidNotificationChannel] for heads up notifications
  // static  AndroidNotificationChannel channel = AndroidNotificationChannel(
  //   'high_importance_channel', // id
  //   'High Importance Notifications', // title
  //   'This channel is used for important notifications.', // description
  //   importance: Importance.high,
  // );

  /// Initialize the [FlutterLocalNotificationsPlugin] package.
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  @override
  void initState() {
    super.initState();
    getSharedPreferenceObject();

    getCurrentDate();      //temp comment
    initLocalNotification();

    _firebaseMessaging.configure(
      // onBackgroundMessage: myBackgroundMessageHandler,
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        setState(() {
          _messageText = "Push Messaging message: $message";
        });

        var title = "SmartenApps for tally";
        var body = "Notification body";

        if (message.containsKey('notification')) {
          title = message['notification']['title'] ?? 'title';
          body = message['notification']['body'] ?? 'body';
        } else {
          title = message['aps']['alert']['title'] ?? 'title';
          body = message['aps']['alert']['body'] ?? 'body';
        }
        print("title: $title");
        print("body: $body");

        await flutterLocalNotificationsPlugin.show(
            0, title, body, platformChannelSpecifics);
      },
      onLaunch: (Map<String, dynamic> message) async {
        setState(() {
          _messageText = "Push Messaging message: $message";
        });
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        setState(() {
          _messageText = "Push Messaging message: $message";
        });
        print("onResume: $message");
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });

  }

  reviewApp() async {

    // int temp = preferenceHelper.getInstallTime();
    DateTime temp = preferenceHelper.getUserData().subscriptionStartDate;
    print("time==="+temp.toString());
    // preferenceHelper.saveInstallTime(temp.millisecondsSinceEpoch);
    bool isAvailable = await _inAppReview.isAvailable();
    if(temp!=null) {
      DateTime dob = temp;
      Duration dur = DateTime.now().difference(dob);
      int difference1  = dur.inDays;
      print("difference=========="+difference1.toString());
    }
    if(isAvailable){
      print("in if condition-------------------------------");
      int difference = 0;
      if(difference!=0) {
        if (difference % 7 == 0) {
          _inAppReview.requestReview();
          preferenceHelper.saveInstallTime(DateTime.now().millisecondsSinceEpoch);
        }
      }
    }else{
      print("in else condition-------------------------------");
    }
  }

  initLocalNotification() async {
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    final initializationSettingsAndroid = AndroidInitializationSettings('mipmap/ic_launcher'); // <- default icon name is @mipmap/ic_launcher
    final initializationSettingsIOS = IOSInitializationSettings();

    var initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);

    flutterLocalNotificationsPlugin.initialize(initializationSettings);

    AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
            'high_importance_channel', // id
            'High Importance Notifications', // title
            'This channel is used for important notifications.', // description
            importance: Importance.max,
            priority: Priority.high,
            styleInformation: BigTextStyleInformation(''));

    IOSNotificationDetails iosPlatformChannelSpecifics = IOSNotificationDetails(
        presentAlert: true, presentBadge: true, presentSound: true);
    platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iosPlatformChannelSpecifics);

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
  }

  Future<void> getSharedPreferenceObject() async {
    setState(() {
      widget.isWebLoading = true;
    });
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
      setState(() {
        fullName = preferenceHelper.getUserData().fullName;
        company = preferenceHelper.getUserData().companyName;
        client_Id = preferenceHelper.getUserData().clientId;
        user_Id = preferenceHelper.getUserData().userId;
        subscription_endDate = preferenceHelper.getSubscriptionEndDate();
        //subscription_endDate =
        //preferenceHelper.getUserData().subscriptionEndDate.toString();
        isDesktopVerification =
            preferenceHelper.getUserData().desktopVarification != "0"
                ? true
                : false;
        print("isDesktopVerification : $isDesktopVerification");
        reviewApp();
        // temp comment
        // if (isDesktopVerification) {
        //   getCompanyListForDashboard();
        // } else {
        //   widget.currentView =
        //       HowWorks(key: UniqueKey(), dashBoardViewState: this);
        // }
        setMenuList();

        // load dashboard-------
        getCompanyListForDashboard();
        // setMenuList();
        getLastHistory();
      });
    });

  }

  Future getCompanyListForDashboard() async {
    isFromPaymentHistory = false;
    setState(() {
      isLoading = true;
    });
    try {
      var body = {"user_id": user_Id, "getcompanymappingdisplay": "1", "cuser_id": user_Id, "appversion": "3.0"};
      var response = await http.post(Uri.parse(ApiUtils.COMPANYLIST_URL), body: body);
      print("company response" + response.body);
      if (response.statusCode == 200 || response.statusCode == 201) {
        CompanyListResponse logResponse = new CompanyListResponse.fromJson(json.decode(response.body));
        setState(() {
          isLoading = false;
        });
        if (logResponse.success == "1") {
          setState(() {
            List<String> strFinal = [];
            for (int i = 0; i < logResponse.response.companyIds.length; i++) {
              List<String> arrFinal = logResponse.response.companyIds[i].split('\$*\$');

              if (arrFinal.length > 2) {
                   if(arrFinal[2].trim()=="1")
                   strFinal.add(arrFinal[0].trim());
              }
            }
            String companyParam = "";
            if (strFinal.length != 0)
              companyParam = strFinal.join(",");
              setDashboardWidget(companyParam);
          });
        } else {
          setDashboardWidget(preferenceHelper.getCompanyData());
          // handle
        }
      } else if (response.statusCode == 400) {
        setState(() {
          isLoading = false;
        });
        setDashboardWidget(preferenceHelper.getCompanyData());
      } else {
        setState(() {
          isLoading = false;
        });
        setDashboardWidget(preferenceHelper.getCompanyData());
      }
    } on SocketException catch (e) {
      print(e.message);
      setState(() {
        isLoading = false;
      });
      setDashboardWidget(preferenceHelper.getCompanyData());
    } on Exception catch (e) {
      print(e.toString());
      setState(() {
        isLoading = false;
      });
      setDashboardWidget(preferenceHelper.getCompanyData());
    }
  }

  void setDashboardWidget(String companyParams) {
    webUrl = (isDesktopVerification
            ? ConstantString.Dashboard_url
            : ConstantString.how_works_url) +
        "?client_id=" +
        client_Id +
        "&user_id=" +
        user_Id +
        "&company_id=" +
        companyParams +
        "&app_store_version=3.0" +
        "&ts=" +
        DateTime.now().millisecondsSinceEpoch.toString();

//    var now = new DateTime.now();
//    var formatter = new DateFormat('yyyy-MM-dd hh:mm:ss');
//    String formattedDate = formatter.format(now);
//    print(formattedDate);
//    DateTime dateTimeCreatedAt =
//        DateTime.parse(subscription_endDate.toString());
//    DateTime dateTimeNow = DateTime.now();
//    print('dateTimeNow: $dateTimeNow');
//    final differenceInDays = dateTimeCreatedAt.difference(dateTimeNow).inDays;

    var now = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd');
    String formattedDate = formatter.format(now);
    print(formattedDate);
    final String formatted =
        formatter.format(DateTime.parse(subscription_endDate.toString()));
    DateTime tempDate = new DateFormat("yyyy-MM-dd").parse(formatted);
    DateTime tempDate1 = new DateFormat("yyyy-MM-dd").parse(formattedDate);
    final differenceInDays = tempDate.difference(tempDate1).inDays;

    // print('differenceInDays: $differenceInDays');
    // print('isPaymentFromIAP: $isPaymentFromIAP');
    //
    // print('isFromPaymentHistory $isFromPaymentHistory');

    if (isFromPaymentHistory == true) {
      setState(() {
        widget.currentView = Platform.isIOS?IAP_New(
          dashBoardViewState: this,
        ):IAP(
          dashBoardViewState: this,
        );
        isFromPaymentHistory = false;
      });
    } else if (isPaymentFromIAP == true) {
      setState(() {
        widget.currentView = CustomWebView1(
            key: UniqueKey(), dashBoardViewState: this, webUrl: webUrl);
      });
    } else if (differenceInDays <= 0) {
      print(differenceInDays);
      setState(() {
        widget.currentView = Platform.isIOS?IAP_New(
          dashBoardViewState: this,
        ):IAP(
          dashBoardViewState: this,
        );
        //  isDesktopVerification = false;
        isPayment = false;
      });

//    }else if (isPaymentFromIAP == true){
//      print(isPaymentFromIAP);
//
//      widget.currentView = CustomWebView1(
//          key: UniqueKey(), dashBoardViewState: this, webUrl: webUrl);
    } else if (isDesktopVerification) {
      print('isDesktopVerification');
      setState(() {
        widget.currentView = CustomWebView1(
            key: UniqueKey(), dashBoardViewState: this, webUrl: webUrl);
      });
//    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => DashBoardView()));
    } else {
      setState(() {
        widget.currentView = HowWorks(
            key: UniqueKey(), dashBoardViewState: this, webUrl: webUrl);
      });
    }
  }

  Future getLastHistory() async {
    try {
      var body = {
        "client_id": client_Id,
      };
      var response =
          await http.post(Uri.parse(ApiUtils.LOGS_URL), body: body);
      if (response.statusCode == 200 || response.statusCode == 201) {
        LogResponse logResponse =
            new LogResponse.fromJson(json.decode(response.body));

        if (logResponse.success == "1") {
          setState(() {
            LogsList.clear();
            LogsList.addAll(logResponse.response);
            if (LogsList.length != 0) {
              lastAttemp = "Last data update attempted\n" +
                  new DateFormat('dd-MMM-yyyy HH:mm:ss')
                      .format(LogsList[0].extractionEndTime);
            }
          });
        } else {
          //  scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(logResponse.message));
        }
      } else if (response.statusCode == 400) {
        // scaffoldGlobalKey.currentState
        //     .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      } else {
        // scaffoldGlobalKey.currentState
        //     .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      }
    } on SocketException catch (e) {
      print(e.message);

      // scaffoldGlobalKey.currentState
      //     .showSnackBar(ErrorSnakbar(ConstantString.network_error));
    } on Exception catch (e) {
      print(e.toString());

      // scaffoldGlobalKey.currentState
      //     .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
    }
  }

  @override
  Widget build(BuildContext context) {
    Constant.setScreenAwareConstant(context);
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        drawer: SizedBox(
            width: MediaQuery.of(context).size.width * 0.70,
            // elevation: 0,
            child: Container(
              color: Colors.black,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: Constant.size12),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      SizedBox(width: Constant.size12),
                      Image.asset("assets/images/logo.jpg",
                          width: Constant.size44, height: Constant.size44),
                      SizedBox(width: Constant.size6),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  left: Constant.size8, bottom: Constant.size4),
                              child: Text(
                                fullName,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'open-sens',
                                    fontSize: FontSize.s16),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: Constant.size8,
                                  bottom: Constant.size4,
                                  right: Constant.size4),
                              child: Text(company,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'open-sens',
                                      fontSize: FontSize.s15),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 4),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: Constant.size15, bottom: Constant.size4),
                    color: AppColors.appLightGray,
                    height: Constant.size0_5,
                  ),
                  ListView.builder(
                    scrollDirection: Axis.vertical,
                    addSemanticIndexes: true,
                    itemCount: MenuList.length,
                    itemBuilder: (context, index) {
                      return menuItemView(/*promoList[index], */ index);
                    },
                    shrinkWrap: true,
                  ),
                  Padding(
                    padding: EdgeInsets.all(Constant.size8),
                    child: Text(lastAttemp,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'fontawesome',
                            fontSize: FontSize.s14)),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: Constant.size4),
                    color: AppColors.greenButtonBg,
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: Constant.size12, bottom: Constant.size12),
                      child: InkWell(
                        onTap: () {
                          preferenceHelper.clearAll();
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      SignInView()));
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              FontAwesomeIcons.signOutAlt,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: Constant.size8,
                            ),
                            Text("SIGN OUT",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'fontawesome',
                                    fontSize: FontSize.s16))
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ) // Populate the Drawer in the next step.
            ),
        //  resizeToAvoidBottomPadding: true,
        key: scaffoldGlobalKey,
        body: WillPopScope(
          onWillPop: () async {
            // Earlier working function of backbutton
            // print("in WillPopScope exitCounter=="+exitCounter.toString());
            // if (MenuList[0].isSelected)
            // {
            //   if (webViewController != null) {
            //     if (await webViewController.canGoBack()) {
            //       exitCounter = 0;
            //       webViewController.goBack();
            //     } else {
            //       if (exitCounter == 0) {
            //         scaffoldGlobalKey.currentState.showSnackBar(new SnackBar(
            //           backgroundColor: Colors.black,
            //           content: Row(
            //             crossAxisAlignment: CrossAxisAlignment.center,
            //             mainAxisSize: MainAxisSize.max,
            //             children: [
            //               Icon(
            //                 FontAwesomeIcons.exclamationTriangle,
            //                 size: Constant.size15,
            //                 color: Colors.white,
            //               ),
            //               SizedBox(
            //                 width: Constant.size8,
            //               ),
            //               Expanded(
            //                 child: Text(
            //                   "Press again to close App",
            //                   style: TextStyle(
            //                       color: Colors.white,
            //                       fontSize: FontSize.s15,
            //                       fontFamily: 'fontawesome'),
            //                   overflow: TextOverflow.ellipsis,
            //                   maxLines: 10,
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ));
            //         exitCounter++;
            //       } else {
            //         print("in if condition   0");
            //         SystemChannels.platform.invokeMethod('SystemNavigator.pop');
            //       }
            //     }
            //   } else {
            //     _showExitDialog();
            //   }
            // } else {
            //   _showExitDialog();
            // }
            _showExitDialog();
            return;
          },
          child: UpgradeAlert(
            debugLogging: true,
            showIgnore: true,
            showLater: true,
            dialogStyle: Platform.isIOS
                ?UpgradeDialogStyle.cupertino:UpgradeDialogStyle.material,
            onUpdate: (){
              String url = Platform.isIOS?"https://apps.apple.com/in/app/smartenapps-for-tally/id1386111600":"market://details?id=io.cordova.ejbiTally";
              _launchInWebViewWithJavaScript(url);
              return true;
            },
            messages: MyCustomMessages(),
            child: Stack(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height,
                  color: AppColors.progressPathShadowColor,
                  child: Column(
                    children: [
                      Stack(
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Row(
                              children: [
                                InkWell(
                                    onTap: () {
                                      scaffoldGlobalKey.currentState.openDrawer();
                                      getLastHistory();
                                      // Navigator.of(context).pop();
                                    },
                                    child: Padding(
                                      padding: EdgeInsets.all(Constant.size9),
                                      child: Icon(FontAwesomeIcons.bars,color: Colors.black),
                                    )),

                                // Text(
                                //   "Smarten Apps for Tally",
                                //   style: TextStyle(
                                //       fontFamily: 'open-sans',
                                //       fontSize: FontSize.s19,
                                //       fontWeight: FontWeight.w600,
                                //       color: AppColors.darkBlueTop),
                                // ),
                                Image.asset("assets/images/inner-logo.png",
                                    fit: BoxFit.scaleDown)
                              ],
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  currentDate,
                                  style: TextStyle(
                                      fontFamily: 'open-sans',
                                      fontSize: FontSize.s14,
                                      color: AppColors.greenButtonBg),
                                ),
                                InkWell(
                                    onTap: () {
                                      _refresh();
                                    },
                                    child: Padding(
                                      padding: EdgeInsets.all(Constant.size9),
                                      child: Icon(
                                        FontAwesomeIcons.syncAlt,
                                        color: Colors.black,
                                        size: Constant.size22,
                                      ),
                                    )),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Container(
                        height: Constant.size0_5,
                        color: AppColors.appLightGray,
                      ),
                      Expanded(
                          child: widget.currentView != null
                              ? widget.currentView
                              : Center(
                                  child: CircularProgressIndicator(),
                                )
                          //
                          ),
                    ],
                  ),
                ),
                ProgressIndicatorLoader(AppColors.primary, isLoading)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _showExitDialog() async {
    String saveMessage = "";
    saveMessage =
        "Back button is disabled on SmartenApps for Tally. You can use navigation options available within mobile app.";
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alert',
              style: TextStyle(
                  color: AppColors.greenButtonBorder, fontSize: FontSize.s15)),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(saveMessage),
              ],
            ),
          ),
          actions: <Widget>[
            InkWell(
              child: Padding(
                padding: EdgeInsets.all(Constant.size6),
                child: Text('OK',
                    style: TextStyle(
                        color: AppColors.greenButtonBorder,
                        fontSize: FontSize.s15)),
              ),
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget menuItemView(int index) {
    return Column(
      children: [
        InkWell(
          onTap: () {
            onManutap(MenuList[index].name, index);
          },
          child: Container(
            color: MenuList[index].isSelected
                ? AppColors.greenButtonBg
                : Colors.transparent,
            child: Padding(
              padding:
                  EdgeInsets.only(top: Constant.size3, bottom: Constant.size3),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    width: Constant.size8,
                  ),
                  // Image.asset(
                  //   MenuList[index].image,
                  //   height: Constant.size20,
                  //   width: Constant.size24,
                  //   color: MenuList[index].isActive
                  //       ? Colors.white
                  //       : AppColors.transparentWhite,
                  // ),
                  Icon(
                    MenuList[index].iconData,
                    size: Constant.size20,
                    color: MenuList[index].isActive
                        ? Colors.white
                        : AppColors.transparentWhite,
                  ),
                  SizedBox(
                    width: Constant.size4,
                  ),
                  Padding(
                    padding: EdgeInsets.all(Constant.size12),
                    child: Text(MenuList[index].name,
                        style: TextStyle(
                            color: MenuList[index].isActive
                                ? Colors.white
                                : AppColors.transparentWhite,
                            fontFamily: 'open-sens',
                            fontWeight: FontWeight.w600,
                            fontSize: FontSize.s15)),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          color: AppColors.lightGreyColor,
          height: 0.2,
        )
      ],
    );
  }

  void setMenuList() {
//    var now = new DateTime.now();
//    var formatter = new DateFormat('yyyy-MM-dd hh:mm:ss');
//    String formattedDate = formatter.format(now);
//    print(formattedDate);
//    DateTime dateTimeCreatedAt =
//        DateTime.parse(subscription_endDate.toString());
//    DateTime dateTimeNow = DateTime.now();
//    final differenceInDays = dateTimeCreatedAt.difference(dateTimeNow).inDays;

    var now = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd');
    String formattedDate = formatter.format(now);
    print(formattedDate);
    final String formatted =
        formatter.format(DateTime.parse(subscription_endDate.toString()));
    DateTime tempDate = new DateFormat("yyyy-MM-dd").parse(formatted);
    DateTime tempDate1 = new DateFormat("yyyy-MM-dd").parse(formattedDate);
    final differenceInDays = tempDate.difference(tempDate1).inDays;
    print("differenceInDays: $differenceInDays");

    MenuList.add(new MenuModel(
        "HOME",
        "assets/images/home-icon.png",
        FontAwesomeIcons.home,
        differenceInDays <= 0 ? false : true && isDesktopVerification,
        differenceInDays <= 0 ? false : true));
    MenuList.add(new MenuModel(
        "SELECT COMPANY",
        "assets/images/company-icon.png",
        FontAwesomeIcons.city,
        differenceInDays <= 0 ? false : true && isDesktopVerification,
        false));
    // MenuList.add(new MenuModel("PAYMENTS", "assets/images/refresh-icon.png",
    //FontAwesomeIcons.moneyBillAlt, true, true));
//    MenuList.add(new MenuModel(
//        "PAYMENTS",
//        "assets/images/refresh-icon.png",
//        FontAwesomeIcons.moneyBillAlt,
//        true,
//        differenceInDays < 0 ? true : false));
    MenuList.add(new MenuModel("PAYMENTS", "assets/images/refresh-icon.png",
        FontAwesomeIcons.moneyBillAlt, true, false));
    //MenuList.add(new MenuModel("PAYMENTS", "assets/images/refresh-icon.png",
    // FontAwesomeIcons.moneyBillAlt, true, true));
//    MenuList.add(
//        new MenuModel("REFRESH", "assets/images/refresh-icon.png", false));
//    MenuList.add(
//        new MenuModel("PROFILE", "assets/images/profile-icon.png", false));
    MenuList.add(new MenuModel(
        "LOGS",
        "assets/images/setting-icon.png",
        FontAwesomeIcons.history,
        differenceInDays <= 0 ? false : true && isDesktopVerification,
        false));
    MenuList.add(new MenuModel("FAQS", "assets/images/faq-icon.png",
        FontAwesomeIcons.question, true, false));
    MenuList.add(new MenuModel(
        "INVITE FRIENDS",
        "assets/images/invite-icon.png",
        FontAwesomeIcons.userPlus,
        true,
        false));
    // MenuList.add(
    //     new MenuModel("SUPPORT", "assets/images/support-icon.png", false));
    //MenuList.add(
    //  new MenuModel("ELEGANT BI", "assets/images/elegant-icon2.png", false));
    // MenuList.add(new MenuModel("SmartenApps", "assets/images/elegant-icon2.png", false));
    MenuList.add(new MenuModel("ABOUT US", "assets/images/about-icon.png",
        FontAwesomeIcons.users, true, false));
    print(MenuList);
  }

  void getCurrentDate() {
    var now = new DateTime.now();
    var formatter = new DateFormat('dd-MMM-yyyy');
    setState(() {
      currentDate = formatter.format(now);
    });
  }

  void navigateToPayment() {
    setState(() {
      widget.currentView = Payment(key: UniqueKey(), dashBoardViewState: this);
      for (int i = 0; i < MenuList.length; i++) {
        MenuList[i].isSelected = false;
      }
      MenuList[2].isSelected = true;
    });
  }

  void navigateToDashboard() {
    setState(() {
      getCompanyListForDashboard();
      for (int i = 0; i < MenuList.length; i++) {
        MenuList[i].isSelected = false;
      }
      MenuList[0].isSelected = true;
    });
  }

  void navigateToIAP() {
    setState(() {
      //getCompanyListForDashboard();
      widget.currentView = Platform.isIOS?IAP_New(
        dashBoardViewState: this,
      ):IAP(
        dashBoardViewState: this,
      );
      for (int i = 0; i < MenuList.length; i++) {
        MenuList[i].isSelected = false;
      }
      MenuList[0].isSelected = true;
    });
  }

  void onManutap(String name, int pos) {
    if (MenuList[pos].isActive) // temp edit
    {
      setState(() {
        for (int i = 0; i < MenuList.length; i++) {
          MenuList[i].isSelected = false;
        }
        MenuList[pos].isSelected = true;
      });

      if (name.toLowerCase() == "logs") {
        Navigator.of(context).pop();
        // Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //         builder: (BuildContext context) =>
        //             LogsView()));
//      setState(() {
//        widget.currentView = LogsView(key: UniqueKey(),dashBoardViewState: this);
//      });
        setState(() {
          widget.currentView = Logs(key: UniqueKey(), dashBoardViewState: this);
        });
      }

      if (name.toLowerCase() == "payments") {
        Navigator.of(context).pop();
        setState(() {
          widget.currentView =
              Payment(key: UniqueKey(), dashBoardViewState: this);
        });
      }
      if (name.toLowerCase() == "about us") {
        Navigator.of(context).pop();
        setState(() {
          widget.currentView =
              AboutUs(key: UniqueKey(), dashBoardViewState: this);
        });
      }

      if (name.toLowerCase() == "support") {
        Navigator.of(context).pop();
        setState(() {
          widget.currentView =
              Support(key: UniqueKey(), dashBoardViewState: this);
        });
      }

      if (name.toLowerCase() == "invite friends") {
        Navigator.of(context).pop();
        setState(() {
          widget.currentView =
              InviteFriends(key: UniqueKey(), dashBoardViewState: this);
        });
      }

      if (name.toLowerCase() == "smartenapps") {
        Navigator.of(context).pop();
        setState(() {
          widget.currentView =
              SmartenApps(key: UniqueKey(), dashBoardViewState: this);
        });
      }

      if (name.toLowerCase() == "faqs") {
        Navigator.of(context).pop();
        setState(() {
          widget.currentView = FaqView(
              key: UniqueKey(), dashBoardViewState: this, isBack: false);
        });
      }

      if (name.toLowerCase() == "select company") {
        Navigator.of(context).pop();
        setState(() {
          widget.currentView =
              CompanyList(key: UniqueKey(), dashBoardViewState: this);
        });
      }
      if (name.toLowerCase() == "dashboard" || name.toLowerCase() == "home") {
        if (scaffoldGlobalKey.currentState.isDrawerOpen)
          Navigator.of(context).pop();
        setState(() {
          // if (isDesktopVerification) {
          //   widget.isWebLoading = true;
          //   getCompanyListForDashboard();
          // } else {
          //   widget.currentView =
          //       HowWorks(key: UniqueKey(), dashBoardViewState: this);
          // }
          getCompanyListForDashboard();
        });
      }
    } else {
      Navigator.of(context).pop();
    }
  }

  _refresh() {
    String sel_name = "";
    for (int i = 0; i < MenuList.length; i++) {
      if (MenuList[i].isSelected && MenuList[i].isActive) {
        sel_name = MenuList[i].name;
        break;
      }
    }
    if (sel_name.toLowerCase() == "home") {
      setState(() {
        isFromPaymentHistory = false;
        // if (isDesktopVerification) {
        //   if (webViewController != null)
        //     webViewController.reload();
        //   else
        //     getCompanyListForDashboard(); //temp comment for testing
        // } else {
        //   if (webViewController != null)
        //     webViewController.reload();
        //   else
        //     widget.currentView =
        //         HowWorks(key: UniqueKey(), dashBoardViewState: this);
        // }
        if (webViewController != null)
          webViewController.reload();
        else
          getCompanyListForDashboard(); //temp comment for testing
      });
    } else if (sel_name.toLowerCase() == "logs") {
      setState(() {
        if (webViewController != null)
          webViewController.reload();
        else
          widget.currentView = Logs(key: UniqueKey(), dashBoardViewState: this);
      });
    } else if (sel_name.toLowerCase() == "payments") {
      setState(() {
        if (webViewController != null)
          webViewController.reload();
        else
          widget.currentView =
              Payment(key: UniqueKey(), dashBoardViewState: this);
      });
    } else if (sel_name.toLowerCase() == "about us") {
      setState(() {
        widget.currentView =
            AboutUs(key: UniqueKey(), dashBoardViewState: this);
      });
    } else if (sel_name.toLowerCase() == "support") {
      setState(() {
        widget.currentView =
            Support(key: UniqueKey(), dashBoardViewState: this);
      });
    } else if (sel_name.toLowerCase() == "invite friends") {
      setState(() {
        widget.currentView =
            InviteFriends(key: UniqueKey(), dashBoardViewState: this);
      });
    } else if (sel_name.toLowerCase() == "smartenapps") {
      setState(() {
        widget.currentView =
            SmartenApps(key: UniqueKey(), dashBoardViewState: this);
      });
    } else if (sel_name.toLowerCase() == "faqs") {
      setState(() {
        if (webViewController != null)
          webViewController.reload();
        else
          widget.currentView = FaqView(
              key: UniqueKey(), dashBoardViewState: this, isBack: false);
      });
    }
    if (sel_name.toLowerCase() == "select company") {
      setState(() {
        widget.currentView =
            CompanyList(key: UniqueKey(), dashBoardViewState: this);
      });
    }
  }

  Future<void> _launchInWebViewWithJavaScript(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        // forceSafariVC: true,
        // forceWebView: true,
        enableJavaScript: true,
      );
    } else {
      throw 'Could not launch $url';
    }
  }

}

class MyCustomMessages extends UpgraderMessages {
  /// Override the message function to provide custom language localization.
  @override
  String message(UpgraderMessage messageKey) {
      switch (messageKey) {
        case UpgraderMessage.body:
          return 'A new version of SmartenApps for Tally is available on store.';
        case UpgraderMessage.buttonTitleIgnore:
          return 'Ignore';
        case UpgraderMessage.buttonTitleLater:
          return 'Later';
        case UpgraderMessage.buttonTitleUpdate:
          return 'Update Now';
        case UpgraderMessage.prompt:
          return 'Would you like to update it now?';
        case UpgraderMessage.title:
          return 'Update App ?';
      }
    // Messages that are not provided above can still use the default values.
    return super.message(messageKey);
  }
}

//import 'package:flutter/material.dart';
//import 'package:webview_flutter/webview_flutter.dart';
//import 'dart:async';
//
//class FaqView extends StatefulWidget {
//  @override
//  _FaqViewState createState() => _FaqViewState();
//}
//
//class _FaqViewState extends State<FaqView> {
//  Completer<WebViewController> _controller = Completer<WebViewController>();
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: const Text('FAQ'),
//      ),
//      body: WebView(
//        initialUrl: 'http://35.154.99.45/tally_saas/mobile-faqs.php',
//        onWebViewCreated: (WebViewController webViewController) {
//          _controller.complete(webViewController);
//        },
//      ),
//    );
//  }
//}

import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ConstantString.dart';
import 'package:elegantj_bi_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/DashBoardView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

//class Logs extends StatefulWidget {
//  final DashBoardViewState dashBoardViewState;
//
//  @override
//  _LogsState createState() => _LogsState();
//}
class FaqView extends StatefulWidget {
  final DashBoardViewState dashBoardViewState;
  final bool isBack;

  // LogsView(this.dashBoardViewState,{Key key});
  FaqView({Key key, this.dashBoardViewState,this.isBack}) : super(key: key);

  @override
  _FaqViewState createState() => _FaqViewState();
}

class _FaqViewState extends State<FaqView> {
  // Completer<WebViewController> _controller = Completer<WebViewController>();
  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    print("on build===");
    return Scaffold(
      //resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      key: scaffoldGlobalKey,
      body: Stack(
        children: [
          Column(
            children: [
              widget.isBack?Padding(
                padding: EdgeInsets.all(Constant.size9),
                child:
                Stack(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: InkWell(
                        child: Icon(FontAwesomeIcons.longArrowAltLeft),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      ),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        "FAQs",
                        style: TextStyle(
                            fontFamily: 'fontawesome',
                            fontSize: FontSize.s17,
                            color: AppColors.BLACKCOLOR),
                      ),
                    ),
                  ],
                ),
              ):new Container(),
              // Container(
              //   height: Constant.size0_5,
              //   color: AppColors.appLightGray,
              // ),
              Expanded(
                child: InAppWebView(
                  initialUrlRequest: URLRequest(url: Uri.parse(ConstantString.Faqs_url)),
                  // initialUrl: ConstantString.Faqs_url/*ConstantString.Faqs_login_url*/,
                  initialOptions: InAppWebViewGroupOptions(
                    crossPlatform: InAppWebViewOptions(
                        clearCache: false,
                        supportZoom: true,
                        // debuggingEnabled: true,
                        useOnDownloadStart: true,
                        javaScriptEnabled: true,
                        verticalScrollBarEnabled: true,
                        useShouldInterceptAjaxRequest: true,
                        useShouldOverrideUrlLoading: true,
                      javaScriptCanOpenWindowsAutomatically: true),
                      android: AndroidInAppWebViewOptions(
                        useShouldInterceptRequest: true,
                        useWideViewPort: true,
                        allowContentAccess: true,
                        allowFileAccess: true,
                        overScrollMode: AndroidOverScrollMode.OVER_SCROLL_IF_CONTENT_SCROLLS
                      ),
                      ios: IOSInAppWebViewOptions(
                        allowsInlineMediaPlayback: true,
                      )
                  ),
                  onWebViewCreated: (InAppWebViewController controller) {
                    DashBoardViewState.webViewController = controller;
                  },
                  shouldOverrideUrlLoading: (controller, request) async {
                    // return ShouldOverrideUrlLoadingAction.ALLOW;
                    return NavigationActionPolicy.ALLOW;
                  },
                  onProgressChanged: (InAppWebViewController controller, int progress) {
                    setState(() {
                      if (progress == 100) {
                        isLoading = false;
                      } else {
                        isLoading = true;
                      }
                      print("progress===" + progress.toString());
                    });
                  },
                ),
              ),
            ],
          ),
          ProgressIndicatorLoader(AppColors.primary, isLoading)
        ],
      ),
    );
  }
}

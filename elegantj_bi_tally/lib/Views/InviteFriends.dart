import 'dart:async';

import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:elegantj_bi_tally/Helper/PreferenceHelper.dart';
import 'package:elegantj_bi_tally/Models/LogResponse.dart';
import 'package:elegantj_bi_tally/Models/LoginResponse.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/DashBoardView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
// import 'package:flutter_share/flutter_share.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:fluttercontactpicker/fluttercontactpicker.dart';
import 'package:share/share.dart';



import 'package:shared_preferences/shared_preferences.dart';

//class Logs extends StatefulWidget {
//  final DashBoardViewState dashBoardViewState;
//
//  @override
//  _LogsState createState() => _LogsState();
//}F
class InviteFriends extends StatefulWidget {

  final DashBoardViewState dashBoardViewState;

  // InviteFriendsView(this.dashBoardViewState,{Key key});
  InviteFriends({
    Key key,
    this.dashBoardViewState
  }):super(key:key);
  @override
  _InviteFriendsState createState() => _InviteFriendsState();
}
class _InviteFriendsState extends State<InviteFriends> {
  // Completer<WebViewController> _controller = Completer<WebViewController>();
  InAppWebViewController _webViewController;

  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;
  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;
  String clientId = "";
  String shareContent = "Check this out. It's FREE. SmartenApps for Tally. I love it. https://www.smartenapps.com/tallyerp/";
  @override
  void initState() {
    super.initState();
    getSharedPreferenceObject();
    print("on initState===");
  }
  Future<void> getSharedPreferenceObject() async {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
      UserModel userModel = preferenceHelper.getUserData();
      clientId = userModel.clientId;
     // getLogHistory();
    });
  }

  @override
  Widget build(BuildContext context) {
    print("on build===");
    return Scaffold(
      //resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      key: scaffoldGlobalKey,
      backgroundColor: AppColors.WHITECOLOR,
      body: Stack(
        children: [
          Column(
            children: [
              Padding(
                padding: EdgeInsets.all(Constant.size9),
                child:
                Stack(
                  children: [
                    // Align(
                    //   alignment: Alignment.centerLeft,
                    //   child: InkWell(
                    //     child: Icon(FontAwesomeIcons.longArrowAltLeft),
                    //     onTap: () {
                    //       widget.dashBoardViewState.onManutap("dashboard", 0);
                    //     },
                    //   ),
                    // ),
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        "INVITE FRIENDS",
                        style: TextStyle(
                            fontFamily: 'fontawesome',
                            fontSize: FontSize.s17,
                            fontWeight: FontWeight.w600,
                            color: AppColors.BLACKCOLOR),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: Constant.size0_5,
                color: AppColors.appLightGray,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                    child: Expanded(
                      child: Container(
                        margin: EdgeInsets.only(
                            left: Constant.size15,
                            right: Constant.size15,
                            bottom: Constant.size4,
                            top: Constant.size10),
                        padding: EdgeInsets.all(Constant.size12),

                        child: Text(
                          "Share SmartenApps for Tally to your friends and family.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: AppColors.darkBlueTop,
                              fontSize: FontSize.s17,
                              fontFamily: 'fontawesome',
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                    child: Expanded(
                      child: Container(
                        margin: EdgeInsets.only(
                            left: Constant.size15,
                            right: Constant.size15,
                            bottom: Constant.size12,
                            top: Constant.size10),
                        padding: EdgeInsets.all(Constant.size12),
                        decoration: BoxDecoration(
                          color: AppColors.APPBLUE,
                        ),
                        child: InkWell(
                          onTap: () async {
                            //shareText();
                            final PhoneContact contact =
                                await FlutterContactPicker.pickPhoneContact(askForPermission: true);
                            print(contact);
                            setState(() {
                              List<String> temp = [];
                              temp.add(contact.phoneNumber.toString());
                              _sendSMS(shareContent, temp);
                           //   _phoneContact = contact;
                            });
                          },
                          child: Text(
                            "Address Book",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: FontSize.s17,
                                fontFamily: 'fontawesome',
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )   ,
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                    child: Expanded(
                      child: Container(
                        margin: EdgeInsets.only(
                            left: Constant.size15,
                            right: Constant.size15,
                            bottom: Constant.size12),
                        padding: EdgeInsets.all(Constant.size12),
                        decoration: BoxDecoration(
                          color: AppColors.APPBLUE,
                        ),
                        child: InkWell(
                          onTap: () async {
                            shareText();
                          },
                          child: Text(
                            "Share App",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: FontSize.s17,
                                fontFamily: 'fontawesome',
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )

            ],
          ),
          ProgressIndicatorLoader(AppColors.primary, isLoading)
        ],
      ),
    );
  }

  void _sendSMS(String message1, List<String> recipents) async {
    print("call _sendSMS---------");
    String _result = await sendSMS(message: message1, recipients: recipents)
        .catchError((onError) {
      print(onError);
    });
    print(_result);
  }

  Future<void> shareText() async {
    // await FlutterShare.share(
    //     title: 'SmartenApps for Tally', text: shareContent);

    final RenderBox box = context.findRenderObject() as RenderBox;
      await Share.share(shareContent,
          subject: "shareContent",
          sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);


  }

//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: const Text('Logs'),
//      ),
//      body: WebView(
//        initialUrl: 'http://35.154.99.45/tally_saas/mobile-logs.php',
//        onWebViewCreated: (WebViewController webViewController) {
//          _controller.complete(webViewController);
//        },
//      ),
//    );
//  }
}
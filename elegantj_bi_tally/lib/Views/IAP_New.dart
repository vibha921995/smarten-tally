// Copyright 2013 The Flutter Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';
import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ConstantString.dart';
import 'package:elegantj_bi_tally/Helper/PreferenceHelper.dart';
import 'package:elegantj_bi_tally/Models/LoginResponse.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/DashBoardView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'dart:convert';
import 'package:elegantj_bi_tally/Models/ErrorResponse.dart';
import 'package:elegantj_bi_tally/Utils/ApiUtils.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
// import 'package:package_info/package_info.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:in_app_purchase_android/billing_client_wrappers.dart';
import 'package:in_app_purchase_android/in_app_purchase_android.dart';
import 'package:in_app_purchase_ios/in_app_purchase_ios.dart';
import 'package:in_app_purchase_ios/store_kit_wrappers.dart';
import 'Consumable_store.dart';

class IAP_New extends StatefulWidget {
  final DashBoardViewState dashBoardViewState;
  Widget currentView;

  IAP_New({Key key, this.dashBoardViewState}) : super(key: key);

  @override
  IAP_NewState createState() => IAP_NewState();
}

const bool _kAutoConsume = true;

const String _kConsumableId = 'consumable';
const String _kUpgradeId = 'upgrade';
const String _kSilverSubscriptionId = 'SmartenTally';

const String _kGoldSubscriptionId = 'SmartenTally_WebMobile';
//const List<String> _kProductIds = <String>[
//  _kSilverSubscriptionId,
//  _kGoldSubscriptionId,
//];
List<String> _kProductIds;

class IAP_NewState extends State<IAP_New> {
  final InAppPurchase _inAppPurchase = InAppPurchase.instance;
  StreamSubscription<List<PurchaseDetails>> _subscription;
  List<String> _notFoundIds = [];
  List<ProductDetails> _products = [];
  List<PurchaseDetails> _purchases = [];
  List<String> _consumables = [];
  bool _isAvailable = false;
  bool _purchasePending = false;
  bool _loading = true;
  String _queryProductError;

  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;
  String clientId = "";
  String userId = "";
  String package_id = "";
  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;
  String subscriptionEndDate = "";
  String subscriptionStartDate = "";
  String subscriptionType = "";
//  ObserverList<Function(String)> _errorListeners =
//      new ObserverList<Function(String)>();
  String strPurchasedItem = "";
  var strType = "";
  var strDiff = "";
  var differenceInDays = 0;

  @override
  void initState() {
    if (Platform.isIOS) {
      _kProductIds = <String>[
        'SmartenTally',
        'SmartenTally_WebMobile',
      ];
    } else {
      _kProductIds = <String>['smarten_tally', "smarten_tally_web_mobile"];
    }
    final Stream<List<PurchaseDetails>> purchaseUpdated =
        _inAppPurchase.purchaseStream;
    _subscription = purchaseUpdated.listen((purchaseDetailsList) {
      _listenToPurchaseUpdated(purchaseDetailsList);
    }, onDone: () {
      _subscription.cancel();
    }, onError: (error) {
      // handle error here.
    });
    getSharedPreferenceObject();

    super.initState();
  }

  Future<void> getSharedPreferenceObject() async {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
      UserModel userModel = preferenceHelper.getUserData();
      clientId = userModel.clientId;
      userId = userModel.userId;

      setState(() {
        subscriptionEndDate = preferenceHelper.getSubscriptionEndDate();
        subscriptionStartDate = userModel.subscriptionStartDate.toString();
        subscriptionType = userModel.subscriptionType;
        package_id = preferenceHelper.getPackageID();
      });
      initData();
    });
    initStoreInfo();

    print("subscriptionEndDate===" + subscriptionEndDate);
  }

  void initData() {
    double screenWidth = MediaQuery.of(context).size.width - 20;
    double buttonWidth = (screenWidth / 3) - 20;
    print("subscriptionEndDate==" + subscriptionEndDate.toString());

    DateTime dateTimeCreatedAt = DateTime.parse(subscriptionEndDate.toString());
    DateTime dateTimeNow = DateTime.now();
    var now = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd');
    String formattedDate = formatter.format(now);
    print(formattedDate);
    final String formatted =
        formatter.format(DateTime.parse(subscriptionEndDate.toString()));
    DateTime tempDate = new DateFormat("yyyy-MM-dd").parse(formatted);
    DateTime tempDate1 = new DateFormat("yyyy-MM-dd").parse(formattedDate);

    setState(() {
      differenceInDays = tempDate.difference(tempDate1).inDays;
    });

    print(DateTime.now());
    print("dateTimeCreatedAt==" + dateTimeCreatedAt.toString());
    print("differenceInDays==" + differenceInDays.toString());

    setState(() {
      if (package_id == '2') {
        Platform.isIOS ? strType = "SmartenTally" : strType = "smarten_tally";
      } else if (package_id == '3') {
        Platform.isIOS
            ? strType = "SmartenTally_WebMobile"
            : strType = "smarten_tally_web_mobile";
      } else {
        strType = '';
      }
    });

    var strSelectedPlan = "";
    if (package_id == '0') {
      strSelectedPlan = "Free Web & Mobile App";
    } else if (package_id == '1') {
      strSelectedPlan = 'Web Only';
    } else if (package_id == '2') {
      strSelectedPlan = 'Mobile App Only';
    } else {
      strSelectedPlan = "Paid Web & Mobile App";
    }
    final differenceInDaysCount =
        dateTimeNow.difference(dateTimeCreatedAt).inDays;
    var dayCount = 'has expired a day ago. ';
    if (differenceInDaysCount == 1) {
      dayCount = 'has expired a day ago. ';
    } else if (differenceInDaysCount <= 2) {
      dayCount = 'will expire in next few days.';
    } else {
      dayCount =
          'has expired ' + differenceInDaysCount.toString() + ' days ago. ';
    }
    setState(() {
      if (differenceInDays == 0) {
        strDiff = "You have already subscribed to " +
            strSelectedPlan +
            ' plan.' +
            'Your subscription has been ended today. Please choose and subscribe to a plan.';
      } else if (differenceInDays <= 2 && differenceInDays >= 1) {
        strDiff = "You have already subscribed to " +
            strSelectedPlan +
            ' plan.' +
            'Your subscription is ending in next few days.';
      } else if (differenceInDays > 2) {
        strDiff = "You have already subscribed to " +
            strSelectedPlan +
            ' plan.' +
            'Your subscription is ending in ' +
            differenceInDays.toString() +
            ' days.';
      } else {
        strDiff = 'You had subscribed to ' +
            strSelectedPlan +
            ' plan. Your subscription ' +
            dayCount +
            'Please choose and subscribe to a plan.';
      }
    });
  }

  Future<void> initStoreInfo() async {
    final bool isAvailable = await _inAppPurchase.isAvailable();
    if (!isAvailable) {
      setState(() {
        _isAvailable = isAvailable;
        _products = [];
        _purchases = [];
        _notFoundIds = [];
        _consumables = [];
        _purchasePending = false;
        _loading = false;
      });
      return;
    }

    if (Platform.isIOS) {
      var iosPlatformAddition = _inAppPurchase.getPlatformAddition<InAppPurchaseIosPlatformAddition>();
      await iosPlatformAddition.setDelegate(ExamplePaymentQueueDelegate());
    }

    ProductDetailsResponse productDetailResponse =
        await _inAppPurchase.queryProductDetails(_kProductIds.toSet());
    if (productDetailResponse.error != null) {
      setState(() {
        _queryProductError = productDetailResponse.error.message;
        _isAvailable = isAvailable;
        _products = productDetailResponse.productDetails;
        _purchases = [];
        _notFoundIds = productDetailResponse.notFoundIDs;
        _consumables = [];
        _purchasePending = false;
        _loading = false;
      });
      return;
    }

    if (productDetailResponse.productDetails.isEmpty) {
      setState(() {
        _queryProductError = null;
        _isAvailable = isAvailable;
        _products = productDetailResponse.productDetails;
        _purchases = [];
        _notFoundIds = productDetailResponse.notFoundIDs;
        _consumables = [];
        _purchasePending = false;
        _loading = false;
      });
      return;
    }

    List<String> consumables = await ConsumableStore.load();
    setState(() {
      _isAvailable = isAvailable;
      _products = productDetailResponse.productDetails;
      _notFoundIds = productDetailResponse.notFoundIDs;
      _consumables = consumables;
      _purchasePending = false;
      _loading = false;
    });
  }

  @override
  void dispose() {
    if (Platform.isIOS) {
      var iosPlatformAddition = _inAppPurchase
          .getPlatformAddition<InAppPurchaseIosPlatformAddition>();
      iosPlatformAddition.setDelegate(null);
    }
    _subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Map<String, PurchaseDetails> purchases =
        Map.fromEntries(_purchases.map((PurchaseDetails purchase) {
      if (purchase.pendingCompletePurchase) {
        _inAppPurchase.completePurchase(purchase);
      }
      return MapEntry<String, PurchaseDetails>(purchase.productID, purchase);
    }));
    return Scaffold(
      body: Container(
        //padding: EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              height: Constant.size4,
              //color: AppColors.appLightGray,
            ),
            Container(
              height: 50.0,
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: Stack(
                alignment: Alignment.center,
                children: [
                  // differenceInDays < 0 ? Align() :
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      child: Icon(FontAwesomeIcons.longArrowAltLeft),
                      onTap: () {
                        widget.dashBoardViewState.navigateToPayment();
                      },
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text("Subscription".toUpperCase(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: FontSize.s17,
                            color: AppColors.BLACKCOLOR,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'fontawesome')),
                  ),
                ],
              ),
            ),
            Container(
              height: Constant.size4,
              //color: AppColors.appLightGray,
            ),
            Container(
              height: Constant.size0_5,
              color: AppColors.appLightGray,
            ),
            Container(
              height: Constant.size15,
              // color: AppColors.appLightGray,
            ),
            Container(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  // ignore: unrelated_type_equality_checks
                  //differenceInDays > 0 ?

                  strDiff,
                  //: strDiff,
                  style: TextStyle(
                    fontFamily: 'open-sens',
                    fontSize: FontSize.s16,
                    color: AppColors.BLACKCOLOR,
                    fontWeight: FontWeight.normal,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Container(
              height: Constant.size10,
              // color: AppColors.appLightGray,
            ),

            ListView.separated(
              shrinkWrap: true,
              itemCount: _products.length,
              separatorBuilder: (context, index) {
                return Divider(
                  height: 1,
                  color: Colors.grey,
                );
              },
              itemBuilder: (BuildContext context, int index) {
                // return
                return Container(
                  margin: EdgeInsets.all(10),
                  //height: 100,
                  child: Padding(
                    padding: EdgeInsets.only(right: 8, bottom: 8),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              SizedBox(height: 8),
                              Text(_products[index].title,
//                                        +
//                                        ' - ' +
//                                        _items[index].localizedPrice +
//                                        '/' +
//                                        _items[index].subscriptionPeriodUnitIOS,
                                  style: TextStyle(
                                      fontFamily: 'open-sens',
                                      fontSize: FontSize.s13,
                                      color: AppColors.BLACKCOLOR,
                                      fontWeight: FontWeight.w600)),
                              SizedBox(height: 5),
                              Text(_products[index].price + '/YEAR',
                                  style: TextStyle(
                                      fontFamily: 'open-sens',
                                      fontSize: FontSize.s13,
                                      color: AppColors.BLACKCOLOR,
                                      fontWeight: FontWeight.w600)),
                              SizedBox(height: 5),
                              Text(_products[index].description,
                                  style: TextStyle(
                                      fontFamily: 'open-sens',
                                      fontSize: FontSize.s13,
                                      color: AppColors.appLightGray)),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        _products[index].id != strType
                            ? TextButton(
                                style: TextButton.styleFrom(
                                  primary: _products[index].id != strType
                                      ? Colors.white
                                      : Colors.green,
                                  backgroundColor:
                                      _products[index].id != strType
                                          ? const Color(0xff0E64FE)
                                          : Colors.white,
                                  //  Color(88B475),
                                  onSurface: Colors.grey,
                                ),
                                child: Text(
                                    differenceInDays < 0
                                        ? "Buy"
                                        : _products[index].id != strType
                                            ? "Buy"
                                            : "Subscribed",
                                    style: TextStyle(
                                        fontFamily: 'open-sens',
                                        fontSize: FontSize.s13,
                                        color: _products[index].id != strType
                                            ? AppColors.whiteTransperentColor
                                            : AppColors.appGreen,
                                        fontWeight: FontWeight.w600)),
                                onPressed: () {
                                  _products[index].id == strType
                                      ? null
                                      : this._requestPurchase(
                                          _products[index], purchases);
                                },
                              )
                            : TextButton(
                                style: TextButton.styleFrom(
                                  primary: _products[index].id != strType
                                      ? Colors.white
                                      : Colors.green,
                                  backgroundColor: differenceInDays < 0
                                      ? const Color(0xff0E64FE)
                                      : _products[index].id != strType
                                          ? const Color(0xff0E64FE)
                                          : Colors.white,
                                  //  Color(88B475),
                                  onSurface: Colors.grey,
                                ),
                                child: Text(
                                    differenceInDays < 0
                                        ? "Buy"
                                        : _products[index].id != strType
                                            ? "Buy"
                                            : "Subscribed",
//                                        _items[index].productId != strType
//                                            ? "Buy"
//                                            : "Subscribed",
                                    style: TextStyle(
                                        fontFamily: 'open-sens',
                                        fontSize: FontSize.s13,
                                        color: differenceInDays < 0
                                            ? AppColors.whiteTransperentColor
                                            : _products[index].id != strType
                                                ? AppColors
                                                    .whiteTransperentColor
                                                : AppColors.appGreen,
                                        fontWeight: FontWeight.w600)),
                                onPressed: null,
                              ),
                      ],
                    ),
                  ),
                );
              },
            ),

            // _buildProductList(),
          ],
        ),
      ),
    );
  }

  @override
  Widget build1(BuildContext context) {
    List<Widget> stack = [];
    if (_queryProductError == null) {
      stack.add(
        ListView(
          children: [
            _buildProductList(),
          ],
        ),
      );
    } else {
      stack.add(Center(
        child: Text(_queryProductError),
      ));
    }
    if (_purchasePending) {
      stack.add(
        Stack(
          children: [
            Opacity(
              opacity: 0.3,
              child: const ModalBarrier(dismissible: false, color: Colors.grey),
            ),
            Center(
              child: CircularProgressIndicator(),
            ),
          ],
        ),
      );
    }

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('IAP Example'),
        ),
        body: Stack(
          children: stack,
        ),
      ),
    );
  }

  Card _buildProductList() {
    if (_loading) {
      return Card(
          child: (ListTile(
              leading: CircularProgressIndicator(),
              title: Text('Fetching products...'))));
    }
    if (!_isAvailable) {
      print('UNKNOWN');
      return Card();
    }
    final ListTile productHeader = ListTile(title: Text('Products for Sale'));
    List<ListTile> productList = <ListTile>[];
    if (_notFoundIds.isNotEmpty) {
      productList.add(ListTile(
          title: Text('[${_notFoundIds.join(", ")}] not found',
              style: TextStyle(color: ThemeData.light().errorColor)),
          subtitle: Text(
              'This app needs special configuration to run. Please see example/README.md for instructions.')));
    }

    // This loading previous purchases code is just a demo. Please do not use this as it is.
    // In your app you should always verify the purchase data using the `verificationData` inside the [PurchaseDetails] object before trusting it.
    // We recommend that you use your own server to verify the purchase data.
    Map<String, PurchaseDetails> purchases =
        Map.fromEntries(_purchases.map((PurchaseDetails purchase) {
      if (purchase.pendingCompletePurchase) {
        _inAppPurchase.completePurchase(purchase);
      }
      return MapEntry<String, PurchaseDetails>(purchase.productID, purchase);
    }));
    print('strType' + strType);
    productList.addAll(_products.map(
      (ProductDetails productDetails) {
        PurchaseDetails previousPurchase = purchases[productDetails.id];

        return ListTile(
          title: Text(
            '${productDetails.title}'
            '\n${productDetails.price + '/YEAR'}',
            style: TextStyle(
                fontFamily: 'open-sens',
                fontSize: FontSize.s13,
                color: AppColors.BLACKCOLOR,
                fontWeight: FontWeight.w600),
          ),
          subtitle: Text(productDetails.description,
              style: TextStyle(
                  fontFamily: 'open-sens',
                  fontSize: FontSize.s13,
                  color: AppColors.appLightGray)),
          trailing: previousPurchase != null
              ? IconButton(
                  onPressed: () => confirmPriceChange(context),
                  icon: Icon(Icons.upgrade))
              : productDetails.id != strType
                  ? TextButton(
                      style: TextButton.styleFrom(
                        primary: productDetails.id != strType
                            ? Colors.white
                            : Colors.green,
                        backgroundColor: productDetails.id != strType
                            ? const Color(0xff0E64FE)
                            : Colors.white,
                        //  Color(88B475),
                        onSurface: Colors.grey,
                      ),
                      child: Text(
                          differenceInDays < 0
                              ? "Buy"
                              : productDetails.id != strType
                                  ? "Buy"
                                  : "Subscribed",
                          style: TextStyle(
                              fontFamily: 'open-sens',
                              fontSize: FontSize.s13,
                              color: productDetails.id != strType
                                  ? AppColors.whiteTransperentColor
                                  : AppColors.appGreen,
                              fontWeight: FontWeight.w600)),
                      onPressed: () {
                        productDetails.id == strType
                            ? null
                            : this._requestPurchase(productDetails, purchases);
                      },
                    )
                  : TextButton(
                      style: TextButton.styleFrom(
                        primary: productDetails.id != strType
                            ? Colors.white
                            : Colors.green,
                        backgroundColor: differenceInDays < 0
                            ? const Color(0xff0E64FE)
                            : productDetails.id != strType
                                ? const Color(0xff0E64FE)
                                : Colors.white,
                        //  Color(88B475),
                        onSurface: Colors.grey,
                      ),
                      child: Text(
                          differenceInDays < 0
                              ? "Buy"
                              : productDetails.id != strType
                                  ? "Buy"
                                  : "Subscribed",
                          style: TextStyle(
                              fontFamily: 'open-sens',
                              fontSize: FontSize.s13,
                              color: differenceInDays < 0
                                  ? AppColors.whiteTransperentColor
                                  : productDetails.id != strType
                                      ? AppColors.whiteTransperentColor
                                      : AppColors.appGreen,
                              fontWeight: FontWeight.w600)),
                      onPressed: null,
                    ),
          /* TextButton(
              style: TextButton.styleFrom(
                primary: productDetails.id != strType
                    ? Colors.white
                    : Colors.green,
                backgroundColor:
                productDetails.id != strType
                    ? const Color(0xff0E64FE)
                    : Colors.white,
                //  Color(88B475),
                onSurface: Colors.grey,
              ),
                    child:
                    Text(differenceInDays < 0 ? "Buy" :
                    productDetails.id != strType
                        ? "Buy"
                        : "Subscribed" ,
                        style: TextStyle(
                            fontFamily: 'open-sens',
                            fontSize: FontSize.s13,
                            color: productDetails.id !=
                                strType
                                ? AppColors.whiteTransperentColor
                                : AppColors.appGreen,
                            fontWeight: FontWeight.w600)),
//                    Text(productDetails.price),
//                    style: TextButton.styleFrom(
//                      backgroundColor: Colors.green[800],
//                      primary: Colors.white,
//                    ),
                    onPressed: () {
                      PurchaseParam purchaseParam;

                      if (Platform.isAndroid) {
                        // NOTE: If you are making a subscription purchase/upgrade/downgrade, we recommend you to
                        // verify the latest status of you your subscription by using server side receipt validation
                        // and update the UI accordingly. The subscription purchase status shown
                        // inside the app may not be accurate.
                        final oldSubscription =
                            _getOldSubscription(productDetails, purchases);

                        purchaseParam = GooglePlayPurchaseParam(
                            productDetails: productDetails,
                            applicationUserName: null,
                            changeSubscriptionParam: (oldSubscription != null)
                                ? ChangeSubscriptionParam(
                                    oldPurchaseDetails: oldSubscription,
                                    prorationMode: ProrationMode
                                        .immediateWithTimeProration,
                                  )
                                : null);
                      } else {
                        purchaseParam = PurchaseParam(
                          productDetails: productDetails,
                          applicationUserName: null,
                        );
                      }

                      if (productDetails.id == _kConsumableId) {
                        _inAppPurchase.buyConsumable(
                            purchaseParam: purchaseParam,
                            autoConsume: _kAutoConsume || Platform.isIOS);
                      } else {
                        _inAppPurchase.buyNonConsumable(
                            purchaseParam: purchaseParam);
                      }
                    },
                  ),*/
        );
      },
    ));

    return Card(child: Column(children: productList));
  }

  void _requestPurchase(
      ProductDetails productDetails, Map<String, PurchaseDetails> purchases) {
    isLoading = true;
    FlutterInappPurchase.instance.requestPurchase(productDetails.id);
    isLoading = false;

    PurchaseParam purchaseParam;

    if (Platform.isAndroid) {
      // NOTE: If you are making a subscription purchase/upgrade/downgrade, we recommend you to
      // verify the latest status of you your subscription by using server side receipt validation
      // and update the UI accordingly. The subscription purchase status shown
      // inside the app may not be accurate.
      final oldSubscription = _getOldSubscription(productDetails, purchases);

      purchaseParam = GooglePlayPurchaseParam(
          productDetails: productDetails,
          applicationUserName: null,
          changeSubscriptionParam: (oldSubscription != null)
              ? ChangeSubscriptionParam(
                  oldPurchaseDetails: oldSubscription,
                  prorationMode: ProrationMode.immediateWithTimeProration,
                )
              : null);
    } else {
      purchaseParam = PurchaseParam(
        productDetails: productDetails,
        applicationUserName: null,
      );
    }

    if (productDetails.id == _kConsumableId) {
      _inAppPurchase.buyConsumable(
          purchaseParam: purchaseParam,
          autoConsume: _kAutoConsume || Platform.isIOS);
    } else {
      _inAppPurchase.buyNonConsumable(purchaseParam: purchaseParam);
    }
  }

  Future<void> consume(String id) async {
    await ConsumableStore.consume(id);
    final List<String> consumables = await ConsumableStore.load();
    setState(() {
      _consumables = consumables;
    });
  }

  void showPendingUI() {
    setState(() {
      _purchasePending = true;
    });
  }

  void deliverProduct(PurchaseDetails purchaseDetails) async {
    // IMPORTANT!! Always verify purchase details before delivering the product.
    if (purchaseDetails.productID == _kConsumableId) {
      await ConsumableStore.save(purchaseDetails.purchaseID);
      List<String> consumables = await ConsumableStore.load();
      setState(() {
        _purchasePending = false;
        _consumables = consumables;
      });
    } else {
      setState(() {
        _purchases.add(purchaseDetails);
        _purchasePending = false;
      });
    }
  }

  void handleError(IAPError error) {
    setState(() {
      _purchasePending = false;
    });
  }

  Future<bool> _verifyPurchase(PurchaseDetails purchaseDetails) {
    // IMPORTANT!! Always verify a purchase before delivering the product.
    // For the purpose of an example, we directly return true.
    return Future<bool>.value(true);
  }

  void _handleInvalidPurchase(PurchaseDetails purchaseDetails) {
    // handle invalid purchase here if  _verifyPurchase` failed.
  }

  void _listenToPurchaseUpdated(List<PurchaseDetails> purchaseDetailsList) {
    print(purchaseDetailsList);
    print("Purchased" + purchaseDetailsList.length.toString());
    purchaseDetailsList.forEach((PurchaseDetails purchaseDetails) async {
      if (purchaseDetails.status == PurchaseStatus.pending) {
        showPendingUI();
      } else {
        if (purchaseDetails.status == PurchaseStatus.error) {
          handleError(purchaseDetails.error);
        } else if (purchaseDetails.status == PurchaseStatus.purchased) {
          bool valid = await _verifyPurchase(purchaseDetails);
          if (valid) {
            deliverProduct(purchaseDetails);
            Platform.isIOS
                ? doTrasaction(purchaseDetails.purchaseID, "1",
                    purchaseDetails.productID == "SmartenTally" ? "2" : "3")
                : doTrasaction(purchaseDetails.purchaseID, "1",
                    purchaseDetails.productID == "smarten_tally" ? "2" : "3");
          } else {
            _handleInvalidPurchase(purchaseDetails);
            Platform.isIOS
                ? doTrasaction(purchaseDetails.purchaseID, "0",
                    purchaseDetails.productID == "SmartenTally" ? "2" : "3")
                : doTrasaction(purchaseDetails.purchaseID, "0",
                    purchaseDetails.productID == "smarten_tally" ? "2" : "3");

            return;
          }
        }
        if (Platform.isAndroid) {
          if (!_kAutoConsume && purchaseDetails.productID == _kConsumableId) {
            final InAppPurchaseAndroidPlatformAddition androidAddition =
                _inAppPurchase.getPlatformAddition<
                    InAppPurchaseAndroidPlatformAddition>();
            await androidAddition.consumePurchase(purchaseDetails);
          }
        }
        if (purchaseDetails.pendingCompletePurchase) {
          await _inAppPurchase.completePurchase(purchaseDetails);
        }
      }
    });
  }

  Future doTrasaction(String transactionID, String status, String type) async {
    setState(() {
      isLoading = true;
    });
    try {
      // PackageInfo packageInfo = await PackageInfo.fromPlatform();
      strPurchasedItem = type;
      var body = {
        "user_id": userId,
        "action": "payment",
        "method": "Online",
        "order_id": transactionID,
        "payment_status": status,
        "subscription": type,
        "payment_from": Platform.isIOS ? "Apple" : "Google",
        // "ostype": Platform.isAndroid?"Android":"iOS"
      };
      print("payment req param : ----" + body.toString());
      print("payment request URL : ----" + ApiUtils.PAYMENT_URL);
      var response =
          await http.post(Uri.parse(ApiUtils.PAYMENT_URL), body: body);
      print("Payment response : ----" + response.body);
      print(response.statusCode);
      if (response.statusCode == 200 || response.statusCode == 201) {
        ErrorResponse errorResponse = new ErrorResponse.fromJson(json.decode(
            response.body
                .replaceFirst("associatedcompany", '"associatedcompany"')));
        setState(() {
          isLoading = false;
        });
        print('successs');

        if (errorResponse.success == "1") {
          print('successs11111');
          PaymentSuccess studentResponse;

          studentResponse =
              new PaymentSuccess.fromJson(json.decode(response.body));
          preferenceHelper.savePackageID(studentResponse.response.packageID);
//          SuccessTransact successTransact = new SuccessTransact.fromJson(json.decode(
//              response.body));
          preferenceHelper.saveSubscriptionEndDate(
              studentResponse.response.subscriptionEndDate.toString());
          print(
              'preferenceHelper.getSubscriptionEndDate(): $preferenceHelper.getSubscriptionEndDate()');
          print(preferenceHelper.getPackageID());

          print(preferenceHelper.getSubscriptionEndDate());
          setState(() {
            isPaymentFromIAP = true;
            widget.dashBoardViewState.isDesktopVerification = true;
            widget.dashBoardViewState.isPayment = true;
            widget.dashBoardViewState.isFromPaymentHistory = false;
            widget.dashBoardViewState.getCompanyListForDashboard();
            widget.dashBoardViewState.MenuList.clear();
            widget.dashBoardViewState.setMenuList();
          });
        } else {
          scaffoldGlobalKey.currentState
              .showSnackBar(ErrorSnakbar(errorResponse.message));
        }
      } else if (response.statusCode == 400) {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState
            .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      } else {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState
            .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      }
    } on SocketException catch (e) {
      print(e.message);
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState
          .showSnackBar(ErrorSnakbar(ConstantString.network_error));
    } on Exception catch (e) {
      print(e.toString());
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState
          .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
    }
  }

  Widget ErrorSnakbar(title) {
    return new SnackBar(
      backgroundColor: Colors.redAccent,
      content: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Icon(
            FontAwesomeIcons.exclamationTriangle,
            size: Constant.size15,
            color: Colors.white,
          ),
          SizedBox(
            width: Constant.size8,
          ),
          Expanded(
            child: Text(
              title,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: FontSize.s15,
                  fontFamily: 'fontawesome'),
              overflow: TextOverflow.ellipsis,
              maxLines: 10,
            ),
          ),
        ],
      ),
    );
  }

  Future<void> confirmPriceChange(BuildContext context) async {
    if (Platform.isAndroid) {
      final InAppPurchaseAndroidPlatformAddition androidAddition =
          _inAppPurchase
              .getPlatformAddition<InAppPurchaseAndroidPlatformAddition>();
      var priceChangeConfirmationResult =
          await androidAddition.launchPriceChangeConfirmationFlow(
        sku: 'purchaseId',
      );
      if (priceChangeConfirmationResult.responseCode == BillingResponse.ok) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('Price change accepted'),
        ));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
            priceChangeConfirmationResult.debugMessage ??
                "Price change failed with code ${priceChangeConfirmationResult.responseCode}",
          ),
        ));
      }
    }
    if (Platform.isIOS) {
      var iapIosPlatformAddition = _inAppPurchase
          .getPlatformAddition<InAppPurchaseIosPlatformAddition>();
      await iapIosPlatformAddition.showPriceConsentIfNeeded();
    }
  }

  GooglePlayPurchaseDetails _getOldSubscription(
      ProductDetails productDetails, Map<String, PurchaseDetails> purchases) {
    // This is just to demonstrate a subscription upgrade or downgrade.
    // This method assumes that you have only 2 subscriptions under a group, 'subscription_silver' & 'subscription_gold'.
    // The 'subscription_silver' subscription can be upgraded to 'subscription_gold' and
    // the 'subscription_gold' subscription can be downgraded to 'subscription_silver'.
    // Please remember to replace the logic of finding the old subscription Id as per your app.
    // The old subscription is only required on Android since Apple handles this internally
    // by using the subscription group feature in iTunesConnect.
    GooglePlayPurchaseDetails oldSubscription;
    if (productDetails.id == _kSilverSubscriptionId &&
        purchases[_kGoldSubscriptionId] != null) {
      oldSubscription =
          purchases[_kGoldSubscriptionId] as GooglePlayPurchaseDetails;
    } else if (productDetails.id == _kGoldSubscriptionId &&
        purchases[_kSilverSubscriptionId] != null) {
      oldSubscription =
          purchases[_kSilverSubscriptionId] as GooglePlayPurchaseDetails;
    }
    return oldSubscription;
  }
}

/// Example implementation of the
/// [`SKPaymentQueueDelegate`](https://developer.apple.com/documentation/storekit/skpaymentqueuedelegate?language=objc).
///
/// The payment queue delegate can be implementated to provide information
/// needed to complete transactions.
class ExamplePaymentQueueDelegate implements SKPaymentQueueDelegateWrapper {
  @override
  bool shouldContinueTransaction(
      SKPaymentTransactionWrapper transaction, SKStorefrontWrapper storefront) {
    return true;
  }

  @override
  bool shouldShowPriceConsent() {
    return false;
  }
}

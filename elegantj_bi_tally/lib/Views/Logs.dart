import 'dart:async';

import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ConstantString.dart';
import 'package:elegantj_bi_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:elegantj_bi_tally/Helper/PreferenceHelper.dart';
import 'package:elegantj_bi_tally/Models/LogResponse.dart';
import 'package:elegantj_bi_tally/Models/LoginResponse.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/DashBoardView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

//class Logs extends StatefulWidget {
//  final DashBoardViewState dashBoardViewState;
//
//  @override
//  _LogsState createState() => _LogsState();
//}
class Logs extends StatefulWidget {

  final DashBoardViewState dashBoardViewState;

  // LogsView(this.dashBoardViewState,{Key key});
  Logs({
    Key key,
    this.dashBoardViewState
  }):super(key:key);
  @override
  _LogsState createState() => _LogsState();
}
class _LogsState extends State<Logs> {
  // Completer<WebViewController> _controller = Completer<WebViewController>();

  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;
  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;
  List<LogModel> LogsList = [];

  @override
  void initState() {

    super.initState();
  //  getSharedPreferenceObject();
    print("on initState===");
  }
  Future<void> getSharedPreferenceObject() async {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
      UserModel userModel = preferenceHelper.getUserData();
    //  setState(() {
       // clientId = userModel.clientId;
       // _webViewController.loadUrl(url: 'http://35.154.99.45/tally_saas/mobile-app/mobile-logs.php?client_id='+ clientId);

    //  });
     // getLogHistory();
    });
  }

  @override
  Widget build(BuildContext context) {
    print("on build===");
    return Scaffold(
      //resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      key: scaffoldGlobalKey,
      body: Stack(
        children: [
          Column(
            children: [
              // Padding(
              //   padding: EdgeInsets.all(Constant.size9),
              //   child:
              //   Stack(
              //     children: [
              //       Align(
              //         alignment: Alignment.centerLeft,
              //         child: InkWell(
              //           child: Icon(FontAwesomeIcons.longArrowAltLeft),
              //           onTap: () {
              //             widget.dashBoardViewState.onManutap("dashboard", 0);
              //           },
              //         ),
              //       ),
              //       Align(
              //         alignment: Alignment.center,
              //         child: Text(
              //           "LOGS",
              //           style: TextStyle(
              //               fontFamily: 'fontawesome',
              //               fontSize: FontSize.s17,
              //               color: AppColors.BLACKCOLOR),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              // Container(
              //   height: Constant.size0_5,
              //   color: AppColors.appLightGray,
              // ),
              Expanded(
                child:
                // WebView(
                //   initialUrl: 'http://35.154.99.45/tally_saas/mobile-app/mobile-logs.php?client_id='+ clientId,
                //   //initialUrl: 'http://35.154.99.45/tally_saas/mobile-logs.php',
                //   onWebViewCreated: (WebViewController webViewController) {
                //     _controller.complete(webViewController);
                //   },
                // ),
                  InAppWebView(
                   initialUrlRequest: URLRequest(url: Uri.parse(ConstantString.base_url+'mobile-logs.php?client_id='+ DashBoardViewState.client_Id)),

                    // initialUrl: ConstantString.base_url+'mobile-logs.php?client_id='+ DashBoardViewState.client_Id,
                    initialOptions: InAppWebViewGroupOptions(
                      crossPlatform: InAppWebViewOptions(
                          // debuggingEnabled: true,
                          useOnDownloadStart: true,
                          javaScriptEnabled: true,
                          verticalScrollBarEnabled: true,
                          useShouldOverrideUrlLoading: true),
                    ),
                    onWebViewCreated: (InAppWebViewController controller) {
                      DashBoardViewState.webViewController = controller;
                    },
                    shouldOverrideUrlLoading: (controller, request) async {
                      // return ShouldOverrideUrlLoadingAction.ALLOW;
                      return NavigationActionPolicy.ALLOW;
                    },
                    onProgressChanged: (InAppWebViewController controller, int progress) {
                      setState(() {
                        if (progress == 100) {
                          isLoading = false;
                        } else {
                          isLoading = true;
                        }
                        print("progress===" + progress.toString());
                      });
                    },
                  )
              ),
            ],
          ),
          ProgressIndicatorLoader(AppColors.primary, isLoading)
        ],
      ),
    );
  }
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: const Text('Logs'),
//      ),
//      body: WebView(
//        initialUrl: 'http://35.154.99.45/tally_saas/mobile-logs.php',
//        onWebViewCreated: (WebViewController webViewController) {
//          _controller.complete(webViewController);
//        },
//      ),
//    );
//  }
}
import 'dart:convert';
import 'dart:io';

import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ConstantString.dart';
import 'package:elegantj_bi_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:elegantj_bi_tally/Helper/PreferenceHelper.dart';
import 'package:elegantj_bi_tally/Models/ErrorResponse.dart';
import 'package:elegantj_bi_tally/Models/FpassResponse.dart';
import 'package:elegantj_bi_tally/Utils/ApiUtils.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/SignInView.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:elegantj_bi_tally/Models/VerificationResponseModel.dart';

class Verification extends StatefulWidget {
  Verification({Key key}) : super(key: key);

  @override
  _VerificationState createState() => _VerificationState();
}

class _VerificationState extends State<Verification> {
  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();

  // PreferenceHelper preferenceHelper;
  // SharedPreferences prefs;
  bool isLoading = false;
  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;
  VerificationResponseModel studentResponse;

  // LoginResponse studentResponse;
  TextEditingController _etUsername = new TextEditingController();
  TextEditingController _etPassword = new TextEditingController();
  FocusNode fnusername = new FocusNode();
  FocusNode fnpassword = new FocusNode();
  bool isError = false;
  String errorText = "";
  FpassResponse fpassResponse;

  @override
  void initState() {
    super.initState();
    getSharedPreferenceObject();
  }

  Future<void> getSharedPreferenceObject() async {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
    });
  }

  _checkInternetConnection() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print("Internet result  = " + result.toString());
        setState(() {
          isLoading = true;
        });
        doLogin();
      }
    } on SocketException catch (e) {
      e.toString();
      setState(() {
        isLoading = false;
      });
//      if (!isDialogShowing) {
//        _showDialog();
//      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Constant.setScreenAwareConstant(context);
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomPadding: true,
        resizeToAvoidBottomInset: true,
        key: scaffoldGlobalKey,
        body: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height,
              color: AppColors.progressPathShadowColor,
              child: ListView(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
//                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("assets/images/inner-logo.png",
                      fit: BoxFit.scaleDown),
                  Container(
                    height: Constant.size0_5,
                    color: AppColors.appLightGray,
                  ),
                  Padding(
                    padding: EdgeInsets.all(Constant.size8),
                    child: Text("VERIFICATION",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: FontSize.s17,
                            color: AppColors.BLACKCOLOR,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'open-sens')),
                  ),
                  Container(
                    height: Constant.size0_5,
                    color: AppColors.appLightGray,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: Constant.size20,
                            left: Constant.size15,
                            right: Constant.size15),
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            WhitelistingTextInputFormatter(RegExp(r'[0-9]')),
                          ],
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: FontSize.s16,
                              fontFamily: 'open-sans',
                              fontWeight: FontWeight.w600),
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(Constant.size17),
                              hintText: "Code",
                              hintStyle: TextStyle(
                                  color: AppColors.hintTextColor,
                                  fontSize: FontSize.s16,
                                  fontFamily: 'open-sans',
                                  fontWeight: FontWeight.w500),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black),
                                  borderRadius:
                                      BorderRadius.circular(Constant.size3)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: AppColors.APPBLUE),
                                  borderRadius:
                                      BorderRadius.circular(Constant.size3)),
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.circular(Constant.size3))),
                          controller: _etUsername,
                          onFieldSubmitted: (String value) {
                            FocusScope.of(context).requestFocus(fnpassword);
                          },
                          focusNode: fnusername,
                          textInputAction: TextInputAction.next,
                          textAlign: TextAlign.start,
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          if (_etUsername.text == "") {
                            setState(() {
                              scaffoldGlobalKey.currentState.showSnackBar(
                                  ErrorSnakbar('Enter valid code'));
                              fnusername.requestFocus();
                            });
                          } else {
                            doLogin();
                            //  _checkInternetConnection();
                          }
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              left: Constant.size15,
                              right: Constant.size15,
                              bottom: Constant.size12,
                              top: Constant.size18),
                          padding: EdgeInsets.all(Constant.size12),
                          decoration: BoxDecoration(
                            color: AppColors.APPBLUE,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Container(
                                child: Expanded(
                                  child: Text(
                                    "VERIFY",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: FontSize.s18,
                                        fontFamily: 'fontawesome',
                                        fontWeight: FontWeight.w400),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          doSendOTP();
                        },
                        child: Padding(
                          padding:  EdgeInsets.only(top: Constant.size8),
                          child: Text(
                            "Didn't receive? Resend Code",
                            textAlign: TextAlign.end,
                            style: TextStyle(
                                color: AppColors.APPBLUE,
                                fontSize: FontSize.s16,
                                fontFamily: 'fontawesome'),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      SignInView()));
                        },
                        child: Padding(
                          padding:  EdgeInsets.only(top: Constant.size12),
                          child: Text(
                            "Already registered?",
                            textAlign: TextAlign.end,
                            style: TextStyle(
                                color: AppColors.APPBLUE,
                                fontSize: FontSize.s16,
                                fontFamily: 'fontawesome'),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            ProgressIndicatorLoader(AppColors.primary, isLoading)
          ],
        ),
      ),
    );
  }

  Future doSendOTP() async {
    setState(() {
      isLoading = true;
    });
    try {
      final counter = prefs.getString('counter') ?? 0;
      var body = {"action": "generatenewcode", "mobilenumber": counter};
      print("url===body===" + json.encode(body));
      var response = await http.post(Uri.parse(ApiUtils.FPASS_URL),
          headers: {ApiUtils.ACCEPT: "application/json"}, body: body);
      print("fPass response : ----" + response.body);
      if (response.statusCode == 200 || response.statusCode == 201) {
        setState(() {
          isLoading = false;
        });
        ErrorResponse errorResponse;
        try {
          errorResponse =
              new ErrorResponse.fromJson(json.decode(response.body));
          // fpassResponse = new FpassResponse.fromJson(json.decode(response.body));
        } on Exception catch (e) {
          print(e.toString());
          scaffoldGlobalKey.currentState
              .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
        }
        setState(() {
          isLoading = false;
        });
        if (errorResponse.success == "1") {
          fpassResponse =
              new FpassResponse.fromJson(json.decode(response.body));
          scaffoldGlobalKey.currentState.showSnackBar(SnackBar(
              content: Text(fpassResponse.message),
              backgroundColor: Colors.green));
          //    widget.mobileOtp = fpassResponse.response.verificationcode;

        } else {
          scaffoldGlobalKey.currentState
              .showSnackBar(ErrorSnakbar(errorResponse.message));
        }
      } else if (response.statusCode == 400) {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState
            .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      } else {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState
            .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      }
    } on SocketException catch (e) {
      print(e.message);
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState
          .showSnackBar(ErrorSnakbar(ConstantString.network_error));
    } on Exception catch (e) {
      print(e.toString());
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState
          .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
    }
  }

  Widget ErrorSnakbar(title) {
    return new SnackBar(
      backgroundColor: Colors.redAccent,
      content: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Icon(
            FontAwesomeIcons.exclamationTriangle,
            size: Constant.size15,
            color: Colors.white,
          ),
          SizedBox(
            width: Constant.size8,
          ),
          Expanded(
            child: Text(
              title,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: FontSize.s15,
                  fontFamily: 'fontawesome'),
              overflow: TextOverflow.ellipsis,
              maxLines: 10,
            ),
          ),
        ],
      ),
    );
  }

  Future doLogin() async {
    setState(() {
      isLoading = true;
    });
    try {
      // PackageInfo packageInfo = await PackageInfo.fromPlatform();
      final prefs = await SharedPreferences.getInstance();

// Try reading data from the counter key. If it doesn't exist, return 0.
      final counter = prefs.getString('counter') ?? 0;
      var body = {
        "hd_email": counter,
        "mobile_no": counter,
        "action": "verification",
        "varification_code": _etUsername.text.trim()
      };
      var response = await http
          .post(Uri.parse(ApiUtils.REGISTER_VERIFICATION_URL), body: body);
      print("varification response : ----" + response.body);
      if (response.statusCode == 200 || response.statusCode == 201) {
        ErrorResponse errorResponse = new ErrorResponse.fromJson(json.decode(
            response.body
                .replaceFirst("associatedcompany", '"associatedcompany"')));
        setState(() {
          isLoading = false;
        });
        if (errorResponse.success == "1") {
          studentResponse = new VerificationResponseModel.fromJson(json.decode(
              response.body
                  .replaceFirst("associatedcompany", '"associatedcompany"')));
          scaffoldGlobalKey.currentState.showSnackBar(SnackBar(
              content: Text(studentResponse.message),
              backgroundColor: Colors.green));
          FirebaseAnalytics().logSignUp(signUpMethod: "");

          Future.delayed(const Duration(milliseconds: 2000), () {
            setState(() {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => SignInView()));
            });
          });
        } else {
          scaffoldGlobalKey.currentState
              .showSnackBar(ErrorSnakbar(errorResponse.message));
        }
      } else if (response.statusCode == 400) {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState
            .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      } else {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState
            .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      }
    } on SocketException catch (e) {
      print(e.message);
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState
          .showSnackBar(ErrorSnakbar(ConstantString.network_error));
    } on Exception catch (e) {
      print(e.toString());
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState
          .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
    }
  }
}

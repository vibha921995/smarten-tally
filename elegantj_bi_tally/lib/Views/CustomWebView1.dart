import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:downloads_path_provider/downloads_path_provider.dart';
import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ConstantString.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/DashBoardView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';


import 'dart:io' as Io;

import 'package:share/share.dart';

class CustomWebView1 extends StatefulWidget {
  final DashBoardViewState dashBoardViewState;
  final String webUrl;

  // CustomWebView(this.dashBoardViewState,{Key key});
  CustomWebView1({Key key, this.dashBoardViewState, this.webUrl})
      : super(key: key);

  @override
  _CustomWebView1State createState() => _CustomWebView1State();
}

class _CustomWebView1State extends State<CustomWebView1> {
  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();
  bool isWebLoading = false;
  String _localPath,_sharePath;

  // static var httpClient = new HttpClient();
  String progress = '0';
  String loadingText = "Downloading file....Please wait";
  ReceivePort _port = ReceivePort();

  @override
  void initState() {
    super.initState();
    main();
  }

  Future main() async {
    await Permission.storage.request();
    // await [
    //   Permission.storage,
    //   Permission.manageExternalStorage
    // ].request();

    // await Permission.manageExternalStorage.request();
    // await Permission.manageExternalStorage.request();
    initFlutterDownloader();
    _bindBackgroundIsolate();

    print("Web loading url==" + widget.webUrl);
    if (Platform.isIOS) {
      Directory dir = (await getApplicationDocumentsDirectory());
      _sharePath = dir.path;
      _localPath = dir.path;
    } else {
      _sharePath = (await (getExternalStorageDirectory())).path;
      _localPath = (await DownloadsPathProvider.downloadsDirectory).path; // temp comment
      print("LOCAL path==" + _localPath);
      final savedDir = Directory(_localPath);
      bool hasExisted = await savedDir.exists();
      if (!hasExisted) {
        savedDir.create();
      }
    }
  }

  Future<void> initFlutterDownloader() async {
    WidgetsFlutterBinding.ensureInitialized();
    try {
      await FlutterDownloader.initialize(
        debug: true, // optional: set false to disable printing logs to console
      );
    } catch (Ex) {}
    FlutterDownloader.registerCallback(downloadCallback);
  }

  void _bindBackgroundIsolate() {
    bool isSuccess = IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send_port');
    if (!isSuccess) {
      _unbindBackgroundIsolate();
      _bindBackgroundIsolate();
      return;
    }

    _port.listen((dynamic data) {
      String id = data[0];
      DownloadTaskStatus status = data[1];
      int progress = data[2];
      print("Data listen ===" + progress.toString());
      if (status == DownloadTaskStatus.complete) {
        print('Call download completed');
        if (mounted) {
          setState(() {
            isWebLoading = false;
            Navigator.of(context).pop();
            _showFinishDialog(id);
          });
        }
      }
      if (progress == -1) {
        print("error ===" + data.toString());

        if (mounted) {
          setState(() {
            isWebLoading = false;
            Navigator.of(context).pop();
            _showErrorDialog(id);
          });
        }
      }
    });
  }

  void _unbindBackgroundIsolate() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
  }

  // @override
  // void dispose() {
  //   _unbindBackgroundIsolate();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      key: scaffoldGlobalKey,
      body: Stack(children: [
        InAppWebView(
          initialUrlRequest: URLRequest(url: Uri.parse(widget.webUrl)),
      //    initialUrl: widget.webUrl,
          initialOptions: InAppWebViewGroupOptions(
              crossPlatform: InAppWebViewOptions(
                  horizontalScrollBarEnabled: true,
                  disableVerticalScroll: false,
               //   debuggingEnabled: true,
                  useOnDownloadStart: true,
                  javaScriptEnabled: true,
                  preferredContentMode: UserPreferredContentMode.MOBILE,
                  supportZoom: true,
                  verticalScrollBarEnabled: true,
                  useShouldOverrideUrlLoading: true),
              android: AndroidInAppWebViewOptions(
                  useShouldInterceptRequest: true,
                  loadWithOverviewMode: true,
                  useWideViewPort: false,
                  allowContentAccess: true),
              ios: IOSInAppWebViewOptions(
                allowsInlineMediaPlayback: true,
              )),
          onWebViewCreated: (InAppWebViewController controller) {
            DashBoardViewState.webViewController = controller;
          },
          shouldOverrideUrlLoading: (controller, request) async {
            var url = request.request.url;
            var uri = url;
            print("shouldOverrideUrlLoading.. $url");

            // if(Platform.isIOS)
            // {
            if (uri.toString().contains("export.php") ||
                uri.toString().contains("exportdetails.php")) {
              print(
                  "onDownloadStart====.shouldOverrideUrlLoading Method. $url");
              _showMyDialog();
//                String savePath = await getFilePath("tallyFile_"+DateTime.now().millisecondsSinceEpoch.toString());
//                download2(url, savePath,true);
              final taskId = await FlutterDownloader.enqueue(
                  url: url.toString(),
                  // savedDir: (await getApplicationDocumentsDirectory()).path,
                  savedDir: _localPath,
                  showNotification: true,
                  // show download progress in status bar (for Android)
                  openFileFromNotification: true);
              // }
            }
            // return ShouldOverrideUrlLoadingAction.ALLOW;
            return NavigationActionPolicy.ALLOW;
          },
          onConsoleMessage: (controller, ConsoleMessage consoleMessage) {
            print("Webview console message======" + consoleMessage.message);
            if (consoleMessage.message.startsWith("DoShareUrl")) {
              print("share call...");
              List<String> urls = consoleMessage.message.split(":");
              print("urls:$urls");

              shareLink(urls[1]);
            }
          },
          onDownloadStart: (controller, url) async {
            // print("onDownloadStart Method==-----------== $url");
//             if(Platform.isAndroid) {
//               if (url.toString().contains("export.php") ||
//                   url.toString().contains("exportdetails.php")) {
//                 print(
//                     "onDownloadStart====.shouldOverrideUrlLoading Method. $url");
//                 _showMyDialog();
// //                String savePath = await getFilePath("tallyFile_"+DateTime.now().millisecondsSinceEpoch.toString());
// //                download2(url, savePath,true);
//                 final taskId = await FlutterDownloader.enqueue(
//                     url: url,
//                     // savedDir: (await getApplicationDocumentsDirectory()).path,
//                     savedDir: _localPath,
//                     showNotification: true,
//                     // show download progress in status bar (for Android)
//                     openFileFromNotification: true);
//               }
//             }
          },
          onProgressChanged: (InAppWebViewController controller, int progress) {
            setState(() {
              if (progress == 100) {
                isWebLoading = false;
              } else {
                isWebLoading = true;
              }
              print("progress===" + progress.toString());
            });
          },
        ),
        // ProgressIndicatorLoader(AppColors.primary, isWebLoading)

        isWebLoading
            ? Center(
          child: CircularProgressIndicator(),
        )
            : Container()
      ]),
    );
  }

  // Future<File> _downloadFile(String url, String filename) async {
  //   var request = await httpClient.getUrl(Uri.parse(url));
  //   var response = await request.close();
  //   var bytes = await consolidateHttpClientResponseBytes(response);
  //   String dir = (await getApplicationDocumentsDirectory()).path;
  //   File file = new File('$dir/$filename');
  //   await file.writeAsBytes(bytes);
  //   return file;
  // }

  Future<void> shareLink(String shareUrl) async {
    shareUrl = ConstantString.base_url + shareUrl;
    _showMyDialog();
//    downloadFile(shareUrl, "tallyFile_"+DateTime.now().millisecondsSinceEpoch.toString());
    String filename = "";
    print('shareUrl:$shareUrl');
    if (shareUrl.contains("export.php")) {
      List<String> tempFilename = shareUrl.split("&");
      String page = '',
          type = '',
          group = '';

      for (int i = 0; i < tempFilename.length; i++) {
        if (tempFilename[i].contains("page")) {
          List<String> temppage = tempFilename[i].split("=");
          if (temppage.length > 1) page = capitalize(temppage[1]);
        }
        if (tempFilename[i].contains("type")) {
          List<String> temptype = tempFilename[i].split("=");
          if (temptype.length > 1) type = capitalize(temptype[1]);
        }
        if (tempFilename[i].contains("group")) {
          List<String> tempgroup = tempFilename[i].split("=");
          if (tempgroup.length > 1)
            if (tempgroup[1].length != 0)
              group = capitalize(tempgroup[1]);
            else
              group = "All";
          else
            group = "All";
        }
      }
      filename = page + "-" + type + "-" + group;
    } else if (shareUrl.contains("exportdetails.php")) {
      var now = new DateTime.now();
      var formatter = new DateFormat('dd MMM yyyy');
      String currentDate = formatter.format(now);
      filename = "Voucher Details - " + currentDate;
    }
    String savePath = await getFilePath(filename);
    print('savePath:$savePath');
    print('filename:$filename');
    download2(shareUrl, savePath, filename, false);
  }

  Future<String> getFilePath(uniqueFileName) async {
    String path = '';
//    Directory dir = _findLocalPath());
    print('_sharePath:$_sharePath');
    print(uniqueFileName);
    //path = _sharePath + '$uniqueFileName.pdf';
    path = _sharePath + Platform.pathSeparator + '$uniqueFileName.pdf';
    return path;
  }

  String capitalize(String string) {
    if (string.isEmpty) {
      return string;
    }

    return string[0].toUpperCase() + string.substring(1);
  }

  Future download2(String url, String savePath, String filename,
      bool isExport) async {
    Dio dio = Dio();
    dio.options.connectTimeout = 120 * 1000;
    try {
      Response response = await dio.get(
        url,
        onReceiveProgress: (rcv, total) {
          print(
              'received: ${rcv.toStringAsFixed(0)} out of total: ${total
                  .toStringAsFixed(0)}');
          setState(() {
            progress = ((rcv / total) * 100).toStringAsFixed(0);
          });
          if (progress == '100') {
            Navigator.of(context).pop();

            if (isExport == true) {
              _showFinishDialog("0");
            } else {
              // old package method
              // FlutterShare.shareFile(
              //         title: 'Share file',
              //         text: 'Share ' + filename + ".pdf",
              //         filePath: savePath)
              //     .whenComplete(() {
              //   isWebLoading = false;
              // });

              // new share code
              final RenderBox box = context.findRenderObject() as RenderBox;
              List<String> imagePaths = [];
              imagePaths.add(savePath);
              Share.shareFiles(imagePaths,
                  //text: "SmartenApps for tally",
                  subject: 'Share ' + filename + ".pdf",
                  sharePositionOrigin: box.localToGlobal(Offset.zero) & box
                      .size)
                  .whenComplete(() {
                    setState(() {
                      isWebLoading = false;
                    });
                });
            }
          } else if (double.parse(progress) < 100) {}
        },
        //Received data with List<int>
        options: Options(
            responseType: ResponseType.bytes,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            }),
      );
      print(response.headers);
      File file = File(savePath);
      var raf = file.openSync(mode: FileMode.write);
      // response.data is List<int> type
      raf.writeFromSync(response.data);
      await raf.close();
    } catch (e) {
      print(e);
    }
  }

  // Future<void> downloadFile(uri, fileName) async {
  //   print(uri);
  //   print(fileName);
  //   String savePath = await getFilePath(fileName);
  //   Dio dio = Dio();
  //   dio.options.connectTimeout = 60000;
  //   dio.options.receiveTimeout = 60000;
  //   // dio.options.connectTimeout = 360*1000;
  //   dio.download(
  //     uri,
  //     savePath,
  //     onReceiveProgress: (rcv, total) {
  //       print(
  //           'received: ${rcv.toStringAsFixed(0)} out of total: ${total
  //               .toStringAsFixed(0)}');
  //       setState(() {
  //         progress = ((rcv / total) * 100).toStringAsFixed(0);
  //       });
  //       if (progress == '100') {
  //         setState(() {
  //           isWebLoading = false;
  //         });
  //       } else if (double.parse(progress) < 100) {}
  //     },
  //     deleteOnError: true,
  //   ).then((_) {
  //     if (progress == '100') {
  //       Navigator.of(context).pop();
  //       FlutterShare.shareFile(
  //           title: 'Share file',
  //           text: 'Share Tallyfile',
  //           filePath: savePath)
  //           .whenComplete(() {
  //         isWebLoading = false;
  //       });
  //     }
  //   });
  // }

  // which will be used to save the file to that path in the downloadFile method

  // Future<String> _findLocalPath() async {
  //   final directory = Platform.isAndroid
  //       ? await getD()
  //       : await getApplicationDocumentsDirectory();
  //   return directory.path;
  // }

  static void downloadCallback(String id, DownloadTaskStatus status,
      int progress) {
    print(
        'Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    final SendPort send =
    IsolateNameServer.lookupPortByName('downloader_send_port');
    send.send([id, status, progress]);
  }



  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: WillPopScope(
            onWillPop: () {},
            child: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(loadingText),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Future<void> _showFinishDialog(String taskId) async {
    String saveMessage = "";
    if (Platform.isIOS)
      saveMessage =
      "File downloaded successfully on you phone under Files -> SmartenApps For Tally folder";
    else
      saveMessage =
      "File downloaded successfully on your phone under Downloads folder";

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Downloaded Successfully',
              style: TextStyle(
                  color: AppColors.greenButtonBorder, fontSize: FontSize.s15)),
          content: WillPopScope(
            onWillPop: () {},
            child: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(saveMessage),
                ],
              ),
            ),
          ),
          actions: <Widget>[
            InkWell(
              child: Padding(
                padding: EdgeInsets.all(Constant.size4),
                child: Text('OK',
                    style: TextStyle(
                        color: AppColors.greenButtonBorder,
                        fontSize: FontSize.s15)),
              ),
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
            // InkWell(
            //   child: Padding(
            //     padding: EdgeInsets.all(Constant.size4),
            //     child: Text('OPEN',
            //         style: TextStyle(
            //             color: AppColors.greenButtonBorder,
            //             fontSize: FontSize.s15)),
            //   ),
            //   onTap: () {
            //     Navigator.of(context).pop();
            //     FlutterDownloader.open(taskId: taskId);
            //   },
            // ),
          ],
        );
      },
    );
  }

  Future<void> _showErrorDialog(String taskId) async {
    String saveMessage = "";
    saveMessage =
    "Something went wrong with the download file !! Please try again after some time.";

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Downloaded Failed',
              style: TextStyle(
                  color: AppColors.greenButtonBorder, fontSize: FontSize.s15)),
          content: WillPopScope(
            onWillPop: () {},
            child: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(saveMessage),
                ],
              ),
            ),
          ),
          actions: <Widget>[
            InkWell(
              child: Padding(
                padding: EdgeInsets.all(Constant.size6),
                child: Text('OK',
                    style: TextStyle(
                        color: AppColors.greenButtonBorder,
                        fontSize: FontSize.s15)),
              ),
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
            // InkWell(
            //   child: Padding(
            //     padding: EdgeInsets.all(Constant.size4),
            //     child: Text('OPEN',
            //         style: TextStyle(
            //             color: AppColors.greenButtonBorder,
            //             fontSize: FontSize.s15)),
            //   ),
            //   onTap: () {
            //     Navigator.of(context).pop();
            //     FlutterDownloader.open(taskId: taskId);
            //   },
            // ),
          ],
        );
      },
    );
  }

  // static shareFile() {
  //   print("TAG in shareFile==========");
  //
  //   FlutterShare.shareFile(
  //     title: 'Example share',
  //     text: 'Example share text',
  //     filePath: "tellyfile.pdf"
  //   );
  //
  //   // Share.shareFile(File('$documentDirectory/flutter.png'),
  //   //     subject: 'URL File Share',
  //   //     text: 'Hello, check your share files!',
  //   //     sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  // }

  // Future<Io.File> getImageFromNetwork(String url) async {
  //   var cacheManager = await CacheManager.getInstance();
  //   Io.File file = await cacheManager.getFile(url);
  //   return file;
  // }

  static Future<void> shareFile() async {
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = new File('$dir/tallyfileshare.pdf');

    // await FlutterShare.shareFile(
    //     title: 'Share file', text: 'Example share text', filePath: file.path);

    // await Share.shareFiles([file.path],
    //     text: 'Share file',
    //     subject: 'Share file',
    //     sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }

  Widget ErrorSnakbar(title) {
    return new SnackBar(
      backgroundColor: Colors.redAccent,
      content: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Icon(
            FontAwesomeIcons.exclamationTriangle,
            size: Constant.size15,
            color: Colors.white,
          ),
          SizedBox(
            width: Constant.size8,
          ),
          Expanded(
            child: Text(
              title,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: FontSize.s15,
                  fontFamily: 'fontawesome'),
              overflow: TextOverflow.ellipsis,
              maxLines: 10,
            ),
          ),
        ],
      ),
    );
  }
}

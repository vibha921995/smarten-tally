import 'dart:convert';
import 'dart:io';

import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ConstantString.dart';

import 'package:elegantj_bi_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:elegantj_bi_tally/Models/ErrorResponse.dart';
import 'package:elegantj_bi_tally/Models/FpassResponse.dart';
import 'package:elegantj_bi_tally/Utils/ApiUtils.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/ResetPasswordView.dart';

import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;


class OTPPasswordView extends StatefulWidget {

  String mobile,mobileOtp;

  OTPPasswordView(this.mobile,this.mobileOtp);

  @override
  OTPPasswordViewState createState() => OTPPasswordViewState();
}

class OTPPasswordViewState extends State<OTPPasswordView> {
  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();

  // PreferenceHelper preferenceHelper;
  // SharedPreferences prefs;
  bool isLoading = false;

  // LoginResponse studentResponse;
  TextEditingController _etUsername = new TextEditingController();
  FocusNode fnusername = new FocusNode();
  bool isError = false;
  String errorText = "";
  ErrorResponse studentResponse;
  FpassResponse fpassResponse;

  @override
  void initState() {
    super.initState();
    print("Mobile OTP=== "+widget.mobileOtp);
    // getSharedPreferenceObject();
  }

  @override
  Widget build(BuildContext context) {
    Constant.setScreenAwareConstant(context);
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomPadding: true,
        resizeToAvoidBottomInset: true,
        key: scaffoldGlobalKey,
        body: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height,
              color: AppColors.progressPathShadowColor,
              child: ListView(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
//                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("assets/images/inner-logo.png",
                      fit: BoxFit.scaleDown),
                  Container(
                    height: Constant.size0_5,
                    color: AppColors.appLightGray,
                  ),
                  Padding(
                    padding: EdgeInsets.all(Constant.size8),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: InkWell(
                            child: Icon(FontAwesomeIcons.longArrowAltLeft),
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Text("VERIFICATION CODE",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: FontSize.s17,
                                  color: AppColors.BLACKCOLOR,
                                  fontWeight: FontWeight.w700,
                                  fontFamily: 'open-sens')),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: Constant.size0_5,
                    color: AppColors.appLightGray,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: Constant.size17,
                            left: Constant.size15,
                            right: Constant.size15),
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            WhitelistingTextInputFormatter(RegExp(r'[0-9]')),
                          ],
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: FontSize.s16,
                              fontFamily: 'open-sans',
                              fontWeight: FontWeight.w600),
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(Constant.size17),
                              hintText: "Enter Verification Code",
                              hintStyle: TextStyle(
                                  color: AppColors.hintTextColor,
                                  fontSize: FontSize.s16,
                                  fontFamily: 'open-sans',
                                  fontWeight: FontWeight.w500),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black),
                                  borderRadius:
                                      BorderRadius.circular(Constant.size3)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: AppColors.APPBLUE),
                                  borderRadius:
                                      BorderRadius.circular(Constant.size3)),
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.circular(Constant.size3))),
                          controller: _etUsername,
                          focusNode: fnusername,
                          textInputAction: TextInputAction.done,
                          textAlign: TextAlign.start,
                        ),
                      ),
                      SizedBox(
                        height: Constant.size10,
                      ),
                      InkWell(
                        onTap: () {
                          if (_etUsername.text == "") {
                            setState(() {
                              scaffoldGlobalKey.currentState.showSnackBar(
                                  ErrorSnakbar(
                                      'Enter Verification Code'));
                              fnusername.requestFocus();
                            });
                          } else {
                              if(_etUsername.text == widget.mobileOtp){
                                scaffoldGlobalKey.currentState.showSnackBar(
                                    SuccessSnakbar(
                                        'Verification Successful'));
                                Future.delayed(const Duration(milliseconds: 2000), () {
                                  setState(() {
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (BuildContext context) => ResetPasswordView(widget.mobile,widget.mobileOtp)));
                                  });
                                });

                              }else{
                                scaffoldGlobalKey.currentState.showSnackBar(
                                    ErrorSnakbar(
                                        'Verification code is invalid or expired'));
                              }
                          }
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              left: Constant.size15,
                              right: Constant.size15,
                              bottom: Constant.size12,
                              top: Constant.size10),
                          padding: EdgeInsets.all(Constant.size12),
                          decoration: BoxDecoration(
                            color: AppColors.APPBLUE,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Container(
                                child: Expanded(
                                  child: Text(
                                    "SUBMIT",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: FontSize.s18,
                                        fontFamily: 'fontawesome',
                                        fontWeight: FontWeight.w400),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: Constant.size18,
                            right: Constant.size18,
                            bottom: Constant.size12,
                            top: Constant.size4),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            InkWell(
                              onTap: (){
                                doSendOTP();
                              },
                              child: Text(
                                "Regenerate Verification Code",
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                    color: AppColors.APPBLUE,
                                    fontSize: FontSize.s16,
                                    fontFamily: 'fontawesome'),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            ProgressIndicatorLoader(AppColors.primary, isLoading)
          ],
        ),
      ),
    );
  }

  Future doSendOTP() async {
    setState(() {
      isLoading = true;
    });
    try {
      var body = {
        "action": "resendcode",
        "mobilenumber": widget.mobile
      };
      print("url===body===" + json.encode(body));
      var response = await http.post(Uri.parse(ApiUtils.FPASS_URL),
          headers: {ApiUtils.ACCEPT: "application/json"},
          body:body);
      print("fPass response : ----" + response.body);
      if (response.statusCode == 200 || response.statusCode == 201) {
        try {
          fpassResponse = new FpassResponse.fromJson(json.decode(response.body));
        } on Exception catch (e) {
          print(e.toString());
          setState(() {
            isLoading = false;
          });
          scaffoldGlobalKey.currentState
              .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
        }
        setState(() {
          isLoading = false;
        });
        if (fpassResponse.success == "1") {
          scaffoldGlobalKey.currentState.showSnackBar(SnackBar(
              content: Text(fpassResponse.message),
              backgroundColor: Colors.green));
          widget.mobileOtp = fpassResponse.response.verificationcode;

        } else {
          scaffoldGlobalKey.currentState
              .showSnackBar(ErrorSnakbar(fpassResponse.message));
        }
      } else if (response.statusCode == 400) {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState
            .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      } else {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState
            .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      }
    } on SocketException catch (e) {
      print(e.message);
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState
          .showSnackBar(ErrorSnakbar(ConstantString.network_error));
    } on Exception catch (e) {
      print(e.toString());
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState
          .showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
    }
  }

  Widget ErrorSnakbar(title) {
    return new SnackBar(
      backgroundColor: Colors.redAccent,
      content: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Icon(
            FontAwesomeIcons.exclamationTriangle,
            size: Constant.size15,
            color: Colors.white,
          ),
          SizedBox(
            width: Constant.size8,
          ),
          Expanded(
            child: Text(
              title,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: FontSize.s15,
                  fontFamily: 'fontawesome'),
              overflow: TextOverflow.ellipsis,
              maxLines: 10,
            ),
          ),
        ],
      ),
    );
  }

  Widget SuccessSnakbar(title) {
    return new SnackBar(
      backgroundColor: Colors.green,
      content: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            FontAwesomeIcons.exclamationTriangle,
            size: Constant.size15,
          ),
          SizedBox(
            width: Constant.size8,
          ),
          Text(
            title,
            style: TextStyle(
                color: Colors.white,
                fontSize: FontSize.s15,
                fontFamily: 'fontawesome'),
          ),
        ],
      ),
    );
  }
}

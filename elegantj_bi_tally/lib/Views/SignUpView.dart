
import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/SignInView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:convert';
import 'dart:io';

import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ConstantString.dart';
import 'package:elegantj_bi_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:elegantj_bi_tally/Helper/PreferenceHelper.dart';
import 'package:elegantj_bi_tally/Models/ErrorResponse.dart';
import 'package:elegantj_bi_tally/Models/RegisterResponse.dart';
import 'package:elegantj_bi_tally/Utils/ApiUtils.dart';

import 'package:flutter/services.dart';
// import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:elegantj_bi_tally/Views/Verification.dart';
import 'package:url_launcher/url_launcher.dart';

class SignUpView extends StatefulWidget {
  SignUpView({Key key}) : super(key: key);

  @override
  _SignUpViewState createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {
  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();

  // PreferenceHelper preferenceHelper;
  // SharedPreferences prefs;
  bool isLoading = false;
  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;
  // LoginResponse studentResponse;
  TextEditingController _etFullName = new TextEditingController();
  TextEditingController _etCName = new TextEditingController();
  TextEditingController _etEmail = new TextEditingController();
  TextEditingController _etMobile = new TextEditingController();
  TextEditingController _etPromoCode = new TextEditingController();
  TextEditingController _etPassword = new TextEditingController();
  TextEditingController _etCpassword = new TextEditingController();
  FocusNode fnFullName = new FocusNode();
  FocusNode fnCName = new FocusNode();
  FocusNode fnEmail = new FocusNode();
  FocusNode fnMobile = new FocusNode();
  FocusNode fnPromocode = new FocusNode();
  FocusNode fnPassword = new FocusNode();
  FocusNode fnCPassword = new FocusNode();
  bool isError = false;
  bool checkedValue = false;
  String errorText = "";
  RegisterResponse studentResponse;


  @override
  void initState() {
    super.initState();
  }
  Future<void> getSharedPreferenceObject() async {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
    });
  }
  @override
  Widget build(BuildContext context) {
    Constant.setScreenAwareConstant(context);
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomPadding: true,
        resizeToAvoidBottomInset: true,
        key: scaffoldGlobalKey,
        body: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height,
              color: AppColors.progressPathShadowColor,
              child: ListView(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                children: [
                  Image.asset("assets/images/inner-logo.png",
                      fit: BoxFit.scaleDown),
                  Container(
                    height: Constant.size0_5,
                    color: AppColors.appLightGray,
                  ),
                  Padding(
                    padding: EdgeInsets.all(Constant.size8),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: InkWell(
                            child: Icon(FontAwesomeIcons.longArrowAltLeft),
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Text("REGISTER",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: FontSize.s17,
                                  color: AppColors.BLACKCOLOR,
                                  fontWeight: FontWeight.w700,
                                  fontFamily: 'open-sens')),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: Constant.size0_5,
                    color: AppColors.appLightGray,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: Constant.size17,
                            left: Constant.size15,
                            right: Constant.size15),
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          // inputFormatters: <TextInputFormatter>[
                          //   FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z0-9 ]')),
                          // ],
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: FontSize.s16,
                              fontFamily: 'open-sans',
                              fontWeight: FontWeight.w600),
                          decoration: InputDecoration(

                              contentPadding: EdgeInsets.all(Constant.size17),
                              hintText: "Full Name *",
                              hintStyle: TextStyle(
                                  color: AppColors.hintTextColor,
                                  fontSize: FontSize.s16,
                                  fontFamily: 'open-sans',
                                  fontWeight: FontWeight.w500),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black),
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                  BorderSide(color: AppColors.APPBLUE),
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                              border: OutlineInputBorder(
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3))),
                          controller: _etFullName,
                          onFieldSubmitted: (String value) {
                            FocusScope.of(context).requestFocus(fnCName);
                          },
                          focusNode: fnFullName,
                          textInputAction: TextInputAction.next,
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: Constant.size16,
                            left: Constant.size15,
                            right: Constant.size15),
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          // inputFormatters: <TextInputFormatter>[
                          //   FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z0-9 ]')),
                          // ],
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: FontSize.s16,
                              fontFamily: 'open-sans',
                              fontWeight: FontWeight.w600),
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(Constant.size17),
                              hintText: "Company Name *",
                              hintStyle: TextStyle(
                                  color: AppColors.hintTextColor,
                                  fontSize: FontSize.s16,
                                  fontFamily: 'open-sans',
                                  fontWeight: FontWeight.w500),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black),
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                  BorderSide(color: AppColors.APPBLUE),
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                              border: OutlineInputBorder(
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3))),
                          controller: _etCName,
                          onFieldSubmitted: (String value) {
                            FocusScope.of(context).requestFocus(fnEmail);
                          },
                          focusNode: fnCName,
                          textInputAction: TextInputAction.next,
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: Constant.size16,
                            left: Constant.size15,
                            right: Constant.size15),
                        child: TextFormField(
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: FontSize.s16,
                              fontFamily: 'open-sans',
                              fontWeight: FontWeight.w600),
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(Constant.size17),
                              hintText: "Email",
                              hintStyle: TextStyle(
                                  color: AppColors.hintTextColor,
                                  fontSize: FontSize.s16,
                                  fontFamily: 'open-sans',
                                  fontWeight: FontWeight.w500),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black),
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                  BorderSide(color: AppColors.APPBLUE),
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                              border: OutlineInputBorder(
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3))),
                          controller: _etEmail,
                          onFieldSubmitted: (String value) {
                            FocusScope.of(context).requestFocus(fnMobile);
                          },
                          focusNode: fnEmail,
                          textInputAction: TextInputAction.next,
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: Constant.size16,
                            left: Constant.size15,
                            right: Constant.size15),
                        child: TextFormField(
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            new LengthLimitingTextInputFormatter(10),
                            WhitelistingTextInputFormatter(RegExp(r'[0-9]')),
                          ],
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: FontSize.s16,
                              fontFamily: 'open-sans',
                              fontWeight: FontWeight.w600),
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(Constant.size17),
                              hintText: "Mobile (e.g. 99999 99999) *",
                              hintStyle: TextStyle(
                                  color: AppColors.hintTextColor,
                                  fontSize: FontSize.s16,
                                  fontFamily: 'open-sans',
                                  fontWeight: FontWeight.w500),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black),
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                  BorderSide(color: AppColors.APPBLUE),
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                              border: OutlineInputBorder(
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3))),
                          controller: _etMobile,
                          focusNode: fnMobile,
                          onFieldSubmitted: (String value) {
                            FocusScope.of(context).requestFocus(fnPromocode);
                          },
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: Constant.size16,
                            left: Constant.size15,
                            right: Constant.size15),
                        child: TextFormField(

                          obscureText: true,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: FontSize.s16,
                              fontFamily: 'open-sans',
                              fontWeight: FontWeight.w600),
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(Constant.size17),
                              hintText: "Password *",
                              hintStyle: TextStyle(
                                  color: AppColors.hintTextColor,
                                  fontSize: FontSize.s16,
                                  fontFamily: 'open-sans',
                                  fontWeight: FontWeight.w500),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black),
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                  BorderSide(color: AppColors.APPBLUE),
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                              border: OutlineInputBorder(
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3))),
                          controller: _etPassword,
                          focusNode: fnPassword,
                          textInputAction: TextInputAction.next,
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: Constant.size16,
                            left: Constant.size15,
                            right: Constant.size15),
                        child: TextFormField(
                          obscureText: true,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: FontSize.s16,
                              fontFamily: 'open-sans',
                              fontWeight: FontWeight.w600),
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(Constant.size17),
                              hintText: "Confirm Password *",
                              hintStyle: TextStyle(
                                  color: AppColors.hintTextColor,
                                  fontSize: FontSize.s16,
                                  fontFamily: 'open-sans',
                                  fontWeight: FontWeight.w500),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black),
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                  BorderSide(color: AppColors.APPBLUE),
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                              border: OutlineInputBorder(
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3))),
                          controller: _etCpassword,
                          focusNode: fnCPassword,
                          textInputAction: TextInputAction.next,
                          textAlign: TextAlign.start,
                        ),
                      ),
                      /*Padding(
                        padding: EdgeInsets.only(
                            top: Constant.size16,
                            left: Constant.size15,
                            right: Constant.size15),
                        child: TextFormField(
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: FontSize.s16,
                              fontFamily: 'open-sans',
                              fontWeight: FontWeight.w600),
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(Constant.size17),
                              hintText: "Partner Reference Code (if any)",
                              hintStyle: TextStyle(
                                  color: AppColors.hintTextColor,
                                  fontSize: FontSize.s16,
                                  fontFamily: 'open-sans',
                                  fontWeight: FontWeight.w500),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black),
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide:
                                  BorderSide(color: AppColors.APPBLUE),
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3)),
                              border: OutlineInputBorder(
                                  borderRadius:
                                  BorderRadius.circular(Constant.size3))),
                          controller: _etPromoCode,
                          onFieldSubmitted: (String value) {
                            FocusScope.of(context).requestFocus(fnMobile);
                          },
                          focusNode: fnPromocode,
                          textInputAction: TextInputAction.done,
                          textAlign: TextAlign.start,
                        ),
                      ),*/
                      SizedBox(
                        height: Constant.size10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Theme(
                            data: ThemeData(unselectedWidgetColor: AppColors.APPBLUE),
                            child: Checkbox(
                              value: checkedValue,
                              checkColor: AppColors.WHITECOLOR,
                              activeColor: AppColors.APPBLUE,
                              onChanged: (newValue) {
                                setState(() {
                                  checkedValue = newValue;
                                  //   changeOverLapState(isOverlap);
                                });
                              },
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: RichText(
                                text: TextSpan(
                                  style: TextStyle(color: Colors.black),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: 'I have read and agree to the ',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'fontawesome',
                                            fontSize: FontSize.s15)),
                                    TextSpan(
                                        text:
                                        String.fromCharCodes(new Runes('Terms of SmartenApps for Tally\u00AE EULA')),
                                        style: TextStyle(
                                            color: AppColors.APPBLUE,
                                            fontSize: FontSize.s15),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            _launchInWebViewWithJavaScript(ConstantString.terms_url);
                                            //print('Terms of Service"');
                                          }),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      /*InkWell(
                        onTap: () {
                          // if (_etUsername.text == "") {
                          //   setState(() {
                          //     errorText = "Please enter UserName !!";
                          //     isError = true;
                          //   });
                          // } else if (_etPassword.text == "") {
                          //   setState(() {
                          //     errorText = "Please enter Password !!";
                          //     isError = true;
                          //   });
                          // } else {
                          //   setState(() {
                          //     errorText = "";
                          //     isError = false;
                          //   });
                          // }
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              left: Constant.size15,
                              right: Constant.size15,
                              bottom: Constant.size12,
                              top: Constant.size18),
                          padding: EdgeInsets.all(Constant.size12),
                          decoration: BoxDecoration(
                            color: AppColors.APPBLUE,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Container(
                                child: Expanded(
                                  child: Text(
                                    "REGISTER",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: FontSize.s18,
                                        fontFamily: 'fontawesome',
                                        fontWeight: FontWeight.w400),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),*/
                      InkWell(
                        onTap: () {
                          print("Sign up");
                            if (_etFullName.text == "") {
                              setState(() {
                                scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar('Enter full name'));
                                fnFullName.requestFocus();
                              });
                            } else if (_etCName.text == "") {
                              scaffoldGlobalKey.currentState.showSnackBar(
                                  ErrorSnakbar('Enter company name'));
                              fnCName.requestFocus();
                            }
                            // else if (_etEmail.text == "") {
                            //   setState(() {
                            //     scaffoldGlobalKey.currentState
                            //         .showSnackBar(ErrorSnakbar('Enter email'));
                            //     fnEmail.requestFocus();
                            //   });
                            // }
                            // else if ((RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(_etEmail.text)) == false) {
                            //   setState(() {
                            //     scaffoldGlobalKey.currentState
                            //         .showSnackBar(ErrorSnakbar('Enter valid email'));
                            //     fnEmail.requestFocus();
                            //   });
                            // }
                            else if (_etMobile.text == "") {
                              setState(() {
                                scaffoldGlobalKey.currentState
                                    .showSnackBar(ErrorSnakbar('Enter mobile number'));
                                fnMobile.requestFocus();
                              });
                            }else if (_etMobile.text.length < 10) {
                              scaffoldGlobalKey.currentState.showSnackBar(
                                  ErrorSnakbar('Enter valid Mobile Number'));
                              fnCName.requestFocus();
                            } else if (_etPassword.text == "") {
                              setState(() {
                                scaffoldGlobalKey.currentState
                                    .showSnackBar(ErrorSnakbar('Enter password'));
                                fnPassword.requestFocus();
                              });
                            } else if (_etCpassword.text == "") {
                              setState(() {
                                scaffoldGlobalKey.currentState
                                    .showSnackBar(ErrorSnakbar('Enter confirm password'));
                                fnCPassword.requestFocus();
                              });
                            }else if (_etCpassword.text != _etPassword.text) {
                              setState(() {
                                scaffoldGlobalKey.currentState
                                    .showSnackBar(ErrorSnakbar('Password and confirm password must be same.'));
                                fnCPassword.requestFocus();
                              });
                            }else if (checkedValue!=true) {
                              setState(() {
                                scaffoldGlobalKey.currentState
                                    .showSnackBar(ErrorSnakbar('Please accept Terms.'));
                              });
                            }else {
                              doLogin();
                              //  _checkInternetConnection();
                            }
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              left: Constant.size15,
                              right: Constant.size15,
                              bottom: Constant.size12,
                              top: Constant.size18),
                          padding: EdgeInsets.all(Constant.size12),
                          decoration: BoxDecoration(
                            color: AppColors.APPBLUE,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Container(
                                child: Expanded(
                                  child: Text(
                                    "REGISTER",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: FontSize.s18,
                                        fontFamily: 'fontawesome',
                                        fontWeight: FontWeight.w400),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: Constant.size18,
                            right: Constant.size18,
                            bottom: Constant.size12,
                            top: Constant.size4),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            InkWell(
                              onTap: (){
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) => SignInView()));
                              },
                              child: Text(
                                "Already registered?",
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                    color: AppColors.BLACKCOLOR,
                                    fontSize: FontSize.s16,
                                    fontFamily: 'fontawesome'),
                              ),
                            ),
//                            Text(
//                              "Already registered?",
//                              textAlign: TextAlign.end,
//                              style: TextStyle(
//                                  color: AppColors.BLACKCOLOR,
//                                  fontSize: FontSize.s16,
//                                  fontFamily: 'fontawesome'),
//                            ),
                            SizedBox(
                              width: Constant.size2,
                            ),
                            InkWell(
                              onTap: (){
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) => SignInView()));
                              },
                              child: Text(
                                "Login Here.",
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                    color: AppColors.APPBLUE,
                                    fontSize: FontSize.s16,
                                    fontFamily: 'fontawesome'),
                              ),
                            ),
//                            Text(
//                              "Login Here.",
//                              textAlign: TextAlign.end,
//                              style: TextStyle(
//                                  color: AppColors.APPBLUE,
//                                  fontSize: FontSize.s16,
//                                  fontFamily: 'fontawesome'),
//                            ),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            ProgressIndicatorLoader(AppColors.primary, isLoading)
          ],
        ),
      ),
    );
  }
  Widget ErrorSnakbar(title) {
    return new SnackBar(
      backgroundColor: Colors.redAccent,
      content: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Icon(
            FontAwesomeIcons.exclamationTriangle,
            size: Constant.size15,
            color: Colors.white,
          ),
          SizedBox(
            width: Constant.size8,
          ),
          Expanded(
            child: Text(
              title,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: FontSize.s15,
                  fontFamily: 'fontawesome'),
              overflow: TextOverflow.ellipsis,
              maxLines: 10,
            ),
          ),
        ],
      ),
    );
  }
  Future doLogin() async {
    setState(() {
      isLoading = true;
    });
    try {
      // PackageInfo packageInfo = await PackageInfo.fromPlatform();
      var body = {
        "mobile_no": _etMobile.text.trim(),
        "full_name": _etFullName.text.trim(),
        "company_name": _etCName.text.trim(),
        "email": _etEmail.text.trim(),
        "password": _etPassword.text.trim(),
        "promocode": _etPromoCode.text.trim(),
        "login_with": "M",
        "ostype": Platform.isAndroid?"Android":"iOS"
      };
      var response = await http.post(Uri.parse(ApiUtils.REGISTER_URL), body: body);
      print("Singup response : ----" + response.body);
      if (response.statusCode == 200 || response.statusCode == 201) {
        ErrorResponse errorResponse = new ErrorResponse.fromJson(json.decode(response.body.replaceFirst("associatedcompany", '"associatedcompany"')));
        setState(() {
          isLoading = false;
        });
        if (errorResponse.success == "1") {

          studentResponse = new RegisterResponse.fromJson(json.decode(response.body.replaceFirst("associatedcompany", '"associatedcompany"')));
          scaffoldGlobalKey.currentState.showSnackBar(SnackBar(
              content: Text(studentResponse.message),
              backgroundColor: Colors.green));
          final prefs = await SharedPreferences.getInstance();

// set value
          prefs.setString('counter', studentResponse.response.mobileNo);
          //preferenceHelper.saveUserDataRegister(studentResponse.response);
          //preferenceHelper.saveIsUserLoggedIn(true);
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => Verification()));
        } else {
          scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(errorResponse.message));
        }
      } else if (response.statusCode == 400) {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      } else {
        setState(() {
          isLoading = false;
        });
        scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
      }
    } on SocketException catch (e) {
      print(e.message);
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.network_error));
    } on Exception catch (e) {
      print(e.toString());
      setState(() {
        isLoading = false;
      });
      scaffoldGlobalKey.currentState.showSnackBar(ErrorSnakbar(ConstantString.wrong_error));
    }
  }


  Future<void> _launchInWebViewWithJavaScript(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        // forceSafariVC: true,
        // forceWebView: true,
        enableJavaScript: true,
      );
    } else {
      throw 'Could not launch $url';
    }
  }
//   Future studentLoginBySAP() async {
//
//     print("Header==="+"Authorization:"+"Basic " + base64.encode(utf8.encode(_etUsername.text + ':' + _etPassword.text)));
//     try {
//       final ioc = new HttpClient();
//       ioc.badCertificateCallback =
//           (X509Certificate cert, String host, int port) => true;
//       final http = new IOClient(ioc);
//       var response =
//           await http.get(ApiUtils.stdLoginUrl), headers: {
// //        ApiUtils.ACCEPT: ApiUtils.HEADER_TYPE,
//         "Authorization": "Basic " + base64.encode(utf8.encode(_etUsername.text + ':' + _etPassword.text))
//           });
//       print("Login response : " + response.body);
//       print("Login response statusCode: " + response.statusCode.toString());
//       if (response.statusCode == 200 || response.statusCode == 201)
//       {
//         print("Login response : " + response.body);
//         // temp comment
//         studentResponse = new LoginResponse.fromJson(json.decode(response.body));
// //        studentResponse = new LoginResponse.fromJson(json.decode('{"statuscode":200,"message":"","data":[{"StudentId":"50009407","StudyId":"50009408","ProgramId":"50002066","StudentNo":"5000002764","StudentName":"Test22 Course booking-1","ProgramName":"BBA - Bachelor in Business Administration","Year":"2020","YearDesc":"Acad. Year 2020","Period":"030","PeriodDesc":"Summer","Adviser":"10000009","IsCatalog":"X","IsOpenSrch":"X"},{"StudentId":"50009414","StudyId":"50009415","ProgramId":"50002066","StudentNo":"5000002765","StudentName":"Test23 Course booking-2","ProgramName":"BBA - Bachelor in Business Administration","Year":"2020","YearDesc":"Acad. Year 2020","Period":"030","PeriodDesc":"Summer","Adviser":"10000009","IsCatalog":"X","IsOpenSrch":"X"},{"StudentId":"50009416","StudyId":"50009417","ProgramId":"50002066","StudentNo":"5000002766","StudentName":"Test24 Course booking-2","ProgramName":"BBA - Bachelor in Business Administration","Year":"2020","YearDesc":"Acad. Year 2020","Period":"030","PeriodDesc":"Summer","Adviser":"10000009","IsCatalog":"X","IsOpenSrch":"X"},{"StudentId":"50009437","StudyId":"50009438","ProgramId":"50002066","StudentNo":"5000002767","StudentName":"Test25 Course booking-3","ProgramName":"BBA - Bachelor in Business Administration","Year":"2020","YearDesc":"Acad. Year 2020","Period":"030","PeriodDesc":"Summer","Adviser":"10000009","IsCatalog":"X","IsOpenSrch":"X"}]}'));
//         if (studentResponse.d.results.length >= 1) {
//           preferenceHelper.saveUserDataArray(studentResponse);
//           preferenceHelper.saveUserData(studentResponse.d.results[0]);
//           preferenceHelper.setUname(_etUsername.text);
//           preferenceHelper.setPass(_etPassword.text);
// //          preferenceHelper.setUname("5000002764");
// //          preferenceHelper.setPass("init@123");
//           setState(() {
//             isLoading = false;
//           });
//           scaffoldGlobalKey.currentState.showSnackBar(SnackBar(
//               content: Text("Login Successfully"),
//               backgroundColor: Colors.green));
//           if (studentResponse.d.results.length == 1 &&
//               studentResponse.d.results[0].adviser.length == 0) {
//             preferenceHelper.saveLoginType("student");
//             Navigator.pushReplacement(
//                 context,
//                 MaterialPageRoute(
//                     builder: (BuildContext context) => StudentHome()));
//           } else {
//             preferenceHelper.saveLoginType("advisor");
//             Navigator.pushReplacement(
//                 context,
//                 MaterialPageRoute(
//                     builder: (BuildContext context) => AdvisorHome()));
//           }
//         } else {
//           setState(() {
//             isLoading = false;
//           });
//           scaffoldGlobalKey.currentState.showSnackBar(SnackBar(
//               content: Text(studentResponse.toString()),
//               backgroundColor: Colors.red));
//         }
//       }else if(response.statusCode == 400){
//         setState(() {
//           isLoading = false;
//         });
//         ErrorResponse errorResponse = new ErrorResponse.fromJson(json.decode(response.body));
//         scaffoldGlobalKey.currentState.showSnackBar(SnackBar(
//             content:
//             Text(errorResponse.error.message.value),
//             backgroundColor: Colors.red));
//       } else {
//         setState(() {
//           isLoading = false;
//         });
//
//         scaffoldGlobalKey.currentState.showSnackBar(SnackBar(
//             content: Text("Unauthorized"),
//             backgroundColor: Colors.red));
// //        throw Exception('Failed to load post');
//       }
//     } on SocketException catch (_) {
//       setState(() {
//         isLoading = false;
//       });
//       scaffoldGlobalKey.currentState.showSnackBar(SnackBar(
//           content:
//               Text("Network error!! Please check your internet connection"),
//           backgroundColor: Colors.red));
//     } on Exception catch (_) {
//       setState(() {
//         isLoading = false;
//       });
//       scaffoldGlobalKey.currentState.showSnackBar(SnackBar(
//           content: Text("Something want wrong!! Try again"),
//           backgroundColor: Colors.red));
//     }
//   }
}

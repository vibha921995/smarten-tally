import 'dart:async';

import 'package:elegantj_bi_tally/Contants/AppColors.dart';
import 'package:elegantj_bi_tally/Contants/ConstantString.dart';
import 'package:elegantj_bi_tally/Contants/ProgressIndicatorLoader.dart';
import 'package:elegantj_bi_tally/Helper/PreferenceHelper.dart';
import 'package:elegantj_bi_tally/Models/LogResponse.dart';
import 'package:elegantj_bi_tally/Models/LoginResponse.dart';
import 'package:elegantj_bi_tally/Utils/screen_util.dart';
import 'package:elegantj_bi_tally/Views/DashBoardView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';


//class Logs extends StatefulWidget {
//  final DashBoardViewState dashBoardViewState;
//
//  @override
//  _LogsState createState() => _LogsState();
//}
class AboutUs extends StatefulWidget {
  final DashBoardViewState dashBoardViewState;

  // AboutUsView(this.dashBoardViewState,{Key key});
  AboutUs({Key key, this.dashBoardViewState}) : super(key: key);

  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  // Completer<WebViewController> _controller = Completer<WebViewController>();
  InAppWebViewController _webViewController;

  GlobalKey<ScaffoldState> scaffoldGlobalKey = GlobalKey<ScaffoldState>();
  bool isLoading = false;
  PreferenceHelper preferenceHelper;
  SharedPreferences prefs;
  String clientId = "";

  @override
  void initState() {
    super.initState();
    //getSharedPreferenceObject();
    print("on initState===");
  }

  Future<void> getSharedPreferenceObject() async {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      prefs = sp;
      preferenceHelper = new PreferenceHelper(prefs);
      UserModel userModel = preferenceHelper.getUserData();
      clientId = userModel.clientId;
      // getLogHistory();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //resizeToAvoidBottomPadding: true,
      resizeToAvoidBottomInset: true,
      backgroundColor: AppColors.WHITECOLOR,
      key: scaffoldGlobalKey,
      body: Stack(
        children: [
          Column(
            children: [
              Padding(
                padding: EdgeInsets.all(Constant.size9),
                child: Stack(
                  children: [
                    // Align(
                    //   alignment: Alignment.centerLeft,
                    //   child: InkWell(
                    //     child: Icon(FontAwesomeIcons.longArrowAltLeft),
                    //     onTap: () {
                    //       widget.dashBoardViewState.onManutap("dashboard", 0);
                    //     },
                    //   ),
                    // ),
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        "ABOUT",
                        style: TextStyle(
                            fontFamily: 'fontawesome',
                            fontSize: FontSize.s17,
                            fontWeight: FontWeight.w600,
                            color: AppColors.BLACKCOLOR),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: Constant.size0_5,
                color: AppColors.appLightGray,
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "SmartenApps for Tally\nVersion: 3.0",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'fontawesome',
                      fontSize: FontSize.s17,
                      fontWeight: FontWeight.w700,
                      color: AppColors.BLACKCOLOR),
                ),
              ),
              Container(
                margin: EdgeInsets.all(Constant.size4) ,
                decoration: BoxDecoration(
                  border: Border.all(
                      color: AppColors.appBlue, width: Constant.size1),
                ),
                child: Padding(
                  padding:  EdgeInsets.all(Constant.size16),
                  //here
                  child:  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      style: TextStyle(color: Colors.black),
                      children: <TextSpan>[
                        TextSpan(
                            text: 'This Software is protected by an End User License Agreement. If you do not agree with the terms of the EULA, do not download, install, copy or use the Software. By installing, copying or otherwise using the Software, you agree to be bound by the terms of the EULA. If you do not agree to the terms of the EULA, Elegant MicroWeb is unwilling to license the Software to you. If you do not have the copy of the EULA,',
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'open-sans',
                                letterSpacing: 0.9,
                                fontSize: FontSize.s17)),
                        TextSpan(
                            text:
                            String.fromCharCodes(new Runes('Click here')),
                            style: TextStyle(
                                color: AppColors.APPBLUE,
                                fontFamily: 'open-sans',
                                letterSpacing: 0.9,
                                fontSize: FontSize.s18),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                _launchInWebViewWithJavaScript(ConstantString.terms_url);
                                //print('Terms of Service"');
                              }),
                        TextSpan(
                            text: ' to view EULA or contact our Support desk.',
                            style: TextStyle(
                                color: Colors.black,
                                letterSpacing: 0.9,
                                fontSize: FontSize.s17)),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Disclaimer : Any other company and product names mentioned are used for identification purpose only, and may be trademarks of their respective owners and duly acknowledged.",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'open-sens',
                      fontSize: FontSize.s15,
                      fontWeight: FontWeight.w600,
                      color: AppColors.BLACKCOLOR),
                ),
              ),
            ],
          ),
          ProgressIndicatorLoader(AppColors.primary, isLoading)
        ],
      ),
    );
  }

  Future<void> _launchInWebViewWithJavaScript(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        // forceSafariVC: true,
        // forceWebView: true,
        enableJavaScript: true,
      );
    } else {
      throw 'Could not launch $url';
    }
  }
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: const Text('Logs'),
//      ),
//      body: WebView(
//        initialUrl: 'http://35.154.99.45/tally_saas/mobile-logs.php',
//        onWebViewCreated: (WebViewController webViewController) {
//          _controller.complete(webViewController);
//        },
//      ),
//    );
//  }
}

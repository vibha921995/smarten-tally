class ConstantString {


  static const String base_url = "https://finance.smartenapps.com/mobile-app/";
  static const String wrong_error = "Something want wrong. Please try Again !!!";
  static const String network_error = "Network error. Please try Again !!!";
  static const String doller_sign = "\$";
  static const String Faqs_url = base_url+"mobile-faqs.php";
  static const String Faqs_login_url = "https://www.smartenapps.com/tallyerp/faq/";
  static const String Dashboard_url = base_url+"index.php";
  static const String terms_url = "https://finance.smartenapps.com/pdf/EULA.pdf";
  static const String how_works_url = base_url+"mobile-howitworks.php";

}

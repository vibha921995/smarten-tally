import 'dart:convert';

ErrorResponse errorResponseFromJson(String str) => ErrorResponse.fromJson(json.decode(str));

String errorResponseToJson(ErrorResponse data) => json.encode(data.toJson());

class ErrorResponse {
  ErrorResponse({
    this.success,
    this.message,
  });

  String success;
  String message;

  factory ErrorResponse.fromJson(Map<String, dynamic> json) => ErrorResponse(
    success: json["success"] == null ? null : json["success"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "message": message == null ? null : message,
  };
}

class SuccessTransact {
  SuccessTransact({
    this.success,
    this.message,
    this.response,

  });

  String success;
  String message;
  DateTime response;

  factory SuccessTransact.fromJson(Map<String, dynamic> json) => SuccessTransact(
    success: json["success"] == null ? null : json["success"],
    message: json["message"] == null ? null : json["message"],
    //response: json["response"] == null ? null : json["response"],
    response: json["response"] == null ? null : DateTime.parse(json["response"]),

  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "message": message == null ? null : message,
    //"response": response == null ? null : response,
    "response": response == null ? null : response.toIso8601String(),

  };
}
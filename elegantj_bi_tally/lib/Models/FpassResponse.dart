// To parse this JSON data, do
//
//     final fpassResponse = fpassResponseFromJson(jsonString);

import 'dart:convert';

FpassResponse fpassResponseFromJson(String str) => FpassResponse.fromJson(json.decode(str));

String fpassResponseToJson(FpassResponse data) => json.encode(data.toJson());

class FpassResponse {
  FpassResponse({
    this.success,
    this.response,
    this.message,
  });

  String success;
  Response response;
  String message;

  factory FpassResponse.fromJson(Map<String, dynamic> json) => FpassResponse(
    success: json["success"] == null ? null : json["success"],
    response: json["response"] == null ? null : Response.fromJson(json["response"]),
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "response": response == null ? null : response.toJson(),
    "message": message == null ? null : message,
  };
}

class Response {
  Response({
    this.verificationcode,
  });

  String verificationcode;

  factory Response.fromJson(Map<String, dynamic> json) => Response(
    verificationcode: json["verificationcode"] == null ? null : json["verificationcode"],
  );

  Map<String, dynamic> toJson() => {
    "verificationcode": verificationcode == null ? null : verificationcode,
  };
}

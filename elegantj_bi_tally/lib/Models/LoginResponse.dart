import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    this.success,
    this.response,
    // this.associatedcompany,
    this.message,
    this.appVersion,
    this.dpLink,
    this.token,
    this.package_id,
  });

  String success;
  UserModel response;
  // List<Associatedcompany> associatedcompany;
  String message;
  int appVersion;
  String dpLink;
  String token;
  String package_id;
  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    success: json["success"] == null ? null : json["success"],
    response: json["response"] == null ? null : UserModel.fromJson(json["response"]),
    // associatedcompany: json["associatedcompany"] == null ? null : List<Associatedcompany>.from(json["associatedcompany"].map((x) => Associatedcompany.fromJson(x))),
    message: json["message"] == null ? null : json["message"],
    appVersion: json["app_version"] == null ? null : json["app_version"],
    dpLink: json["dp_link"] == null ? null : json["dp_link"],
    token: json["token"] == null ? null : json["token"],
    package_id: json["package_id"] == null ? null : json["package_id"],


  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "response": response == null ? null : response.toJson(),
    // "associatedcompany": associatedcompany == null ? null : List<dynamic>.from(associatedcompany.map((x) => x.toJson())),
    "message": message == null ? null : message,
    "app_version": appVersion == null ? null : appVersion,
    "dp_link": dpLink == null ? null : dpLink,
    "token": token == null ? null : token,
    "package_id": package_id == null ? null : package_id,


  };
}
class PaymentSuccess {
  PaymentSuccess({
    this.success,
    this.response,
    this.message,
  });

  String success;
  UserModelPayment response;
  String message;

  factory PaymentSuccess.fromJson(Map<String, dynamic> json) => PaymentSuccess(
    success: json["success"] == null ? null : json["success"],
    response: json["response"] == null ? null : UserModelPayment.fromJson(json["response"]),
    message: json["message"] == null ? null : json["message"],



  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "response": response == null ? null : response.toJson(),
    "message": message == null ? null : message,


  };
}
//class PaymentSuccess {
//  PaymentSuccess({
//    this.success,
//    this.response,
//    this.message,
//
//  });
//
//  String success;
//  UserModelPayment response;
//  String message;
//
//  factory PaymentSuccess.fromJson(Map<String, dynamic> json) => PaymentSuccess(
//    success: json["success"] == null ? null : json["success"],
//    response: json["response"] == null ? null : UserModelPayment.fromJson(json["response"]),
//    message: json["message"] == null ? null : json["message"],
//
//
//  );
//
//  Map<String, dynamic> toJson() => {
//    "success": success == null ? null : success,
//    "response": response == null ? null : response.toJson(),
//    "message": message == null ? null : message,
//
//
//
//  };
//}
class Associatedcompany {
  Associatedcompany({
    this.mapCompanyId,
  });

  String mapCompanyId;

  factory Associatedcompany.fromJson(Map<String, dynamic> json) => Associatedcompany(
    mapCompanyId: json["map_company_id"] == null ? null : json["map_company_id"],
  );

  Map<String, dynamic> toJson() => {
    "map_company_id": mapCompanyId == null ? null : mapCompanyId,
  };
}

class UserModel {
  UserModel({
    this.userId,
    this.fullName,
    this.isDesktopadmin,
    this.desktopVarification,
    this.tallyScheduling,
    this.mobileNumber,
    this.mobileVarification,
    this.companyName,
    this.subscriptionType,
    this.expiryDate,
    this.clientId,
    this.status,
    this.cloudcontainername,
    this.email,
    this.address,
    this.city,
    this.state,
    this.country,
    this.subscriptionStartDate,
    this.subscriptionEndDate,
    this.package_id,
  });

  String userId;
  String fullName;
  String isDesktopadmin;
  String desktopVarification;
  String tallyScheduling;
  String mobileNumber;
  String mobileVarification;
  String companyName;
  String subscriptionType;
  DateTime expiryDate;
  String clientId;
  String status;
  String cloudcontainername;
  String email;
  String address;
  String city;
  String state;
  String country;
  DateTime subscriptionStartDate;
  DateTime subscriptionEndDate;
  String package_id;
  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
    userId: json["user_id"] == null ? null : json["user_id"],
    fullName: json["full_name"] == null ? null : json["full_name"],
    isDesktopadmin: json["is_desktopadmin"] == null ? null : json["is_desktopadmin"],
    desktopVarification: json["desktop_varification"] == null ? null : json["desktop_varification"],
    tallyScheduling: json["tally_scheduling"] == null ? null : json["tally_scheduling"],
    mobileNumber: json["mobile_number"] == null ? null : json["mobile_number"],
    mobileVarification: json["mobile_varification"] == null ? null : json["mobile_varification"],
    companyName: json["company_name"] == null ? null : json["company_name"],
    subscriptionType: json["subscription_type"] == null ? null : json["subscription_type"],
    expiryDate: json["expiry_date"] == null ? null : DateTime.parse(json["expiry_date"]),
    clientId: json["client_id"] == null ? null : json["client_id"],
    status: json["status"] == null ? null : json["status"],
    cloudcontainername: json["cloudcontainername"] == null ? null : json["cloudcontainername"],
    email: json["email"] == null ? null : json["email"],
    address: json["address"] == null ? null : json["address"],
    city: json["city"] == null ? null : json["city"],
    state: json["state"] == null ? null : json["state"],
    country: json["country"] == null ? null : json["country"],
    subscriptionStartDate: json["subscription_start_date"] == null ? null : DateTime.parse(json["subscription_start_date"]),
    subscriptionEndDate: json["subscription_end_date"] == null ? null : DateTime.parse(json["subscription_end_date"]),
    package_id: json["package_id"] == null ? null : json["package_id"],


  );

  Map<String, dynamic> toJson() => {
    "user_id": userId == null ? null : userId,
    "full_name": fullName == null ? null : fullName,
    "is_desktopadmin": isDesktopadmin == null ? null : isDesktopadmin,
    "desktop_varification": desktopVarification == null ? null : desktopVarification,
    "tally_scheduling": tallyScheduling == null ? null : tallyScheduling,
    "mobile_number": mobileNumber == null ? null : mobileNumber,
    "mobile_varification": mobileVarification == null ? null : mobileVarification,
    "company_name": companyName == null ? null : companyName,
    "subscription_type": subscriptionType == null ? null : subscriptionType,
    "expiry_date": expiryDate == null ? null : expiryDate.toIso8601String(),
    "client_id": clientId == null ? null : clientId,
    "status": status == null ? null : status,
    "cloudcontainername": cloudcontainername == null ? null : cloudcontainername,
    "email": email == null ? null : email,
    "address": address == null ? null : address,
    "city": city == null ? null : city,
    "state": state == null ? null : state,
    "country": country == null ? null : country,
    "subscription_start_date": subscriptionStartDate == null ? null : subscriptionStartDate.toIso8601String(),
    "subscription_end_date": subscriptionEndDate == null ? null : subscriptionEndDate.toIso8601String(),
    "package_id": package_id == null ? null : package_id,

  };
}
class UserModelPayment {
  UserModelPayment({
   this.orderID,
   this.subscriptionStartDate,
    this.subscriptionEndDate,
    this.packageID,
  });


  String orderID;
  DateTime subscriptionStartDate;
  DateTime subscriptionEndDate;
  String packageID;
  factory UserModelPayment.fromJson(Map<String, dynamic> json) => UserModelPayment(

    orderID: json["order_id"] == null ? null : json["order_id"],
    subscriptionStartDate: json["subscription_start_date"] == null ? null : DateTime.parse(json["subscription_start_date"]),
    subscriptionEndDate: json["subscription_end_date"] == null ? null : DateTime.parse(json["subscription_end_date"]),
    packageID: json["package_id"] == null ? null : json["package_id"],


  );

  Map<String, dynamic> toJson() => {

    "order_id": orderID == null ? null : orderID,
    "subscription_start_date": subscriptionStartDate == null ? null : subscriptionStartDate.toIso8601String(),
    "subscription_end_date": subscriptionEndDate == null ? null : subscriptionEndDate.toIso8601String(),
    "package_id": packageID == null ? null : packageID,

  };
}
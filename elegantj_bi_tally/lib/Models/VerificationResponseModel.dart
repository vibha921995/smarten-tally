class VerificationResponseModel {
  String success;
  String response;
  String message;
  String token;
  String userId;

  VerificationResponseModel(
      {this.success, this.response, this.message, this.token, this.userId});

  VerificationResponseModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    response = json['response'];
    message = json['message'];
    token = json['token'];
    userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['response'] = this.response;
    data['message'] = this.message;
    data['token'] = this.token;
    data['user_id'] = this.userId;
    return data;
  }
}

// To parse this JSON data, do
//
//     final companyListResponse = companyListResponseFromJson(jsonString);

import 'dart:convert';

CompanyListResponse companyListResponseFromJson(String str) => CompanyListResponse.fromJson(json.decode(str));

String companyListResponseToJson(CompanyListResponse data) => json.encode(data.toJson());

class CompanyListResponse {
  CompanyListResponse({
    this.success,
    this.response,
    this.message,
  });

  String success;
  Response response;
  String message;

  factory CompanyListResponse.fromJson(Map<String, dynamic> json) => CompanyListResponse(
    success: json["success"] == null ? null : json["success"],
    response: json["response"] == "" ? null : Response.fromJson(json["response"]),
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "response": response == null ? null : response.toJson(),
    "message": message == null ? null : message,
  };
}

class Response {
  Response({
    this.companyIds,
  });

  List<String> companyIds;

  factory Response.fromJson(Map<String, dynamic> json) => Response(
    companyIds: json["company_ids"] == null ? null : List<String>.from(json["company_ids"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "company_ids": companyIds == null ? null : List<dynamic>.from(companyIds.map((x) => x)),
  };
}

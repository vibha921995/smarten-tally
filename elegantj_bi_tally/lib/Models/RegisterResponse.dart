class RegisterResponse {
  String success;
  Response response;
  String message;

  RegisterResponse({this.success, this.response, this.message});

  RegisterResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    response = json['response'] != null
        ? new Response.fromJson(json['response'])
        : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.response != null) {
      data['response'] = this.response.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class Response {
  String userId;
  String fullName;
  String companyName;
  String email;
  String mobileNo;
  String password;
  Null tmpPassword;
  String loginWith;
  String fbId;
  String gpId;
  String twId;
  String liId;
  Null address;
  Null city;
  Null state;
  Null country;
  String currentStatus;
  String partnerId;
  String verificationCode;
  String verificationCodeDate;
  String clientid;
  String isAdminUser;
  String desktopconnectionstatus;
  String registrationDate;
  String subscriptionType;
  String subscriptionStartDate;
  String subscriptionEndDate;
  String createdAt;
  String updatedAt;
  Null createdBy;
  Null updatedBy;
  String isDeleted;
  String isDesktopadmin;
  String deviceuniqueid;
  String regPromocode;

  Response(
      {this.userId,
        this.fullName,
        this.companyName,
        this.email,
        this.mobileNo,
        this.password,
        this.tmpPassword,
        this.loginWith,
        this.fbId,
        this.gpId,
        this.twId,
        this.liId,
        this.address,
        this.city,
        this.state,
        this.country,
        this.currentStatus,
        this.partnerId,
        this.verificationCode,
        this.verificationCodeDate,
        this.clientid,
        this.isAdminUser,
        this.desktopconnectionstatus,
        this.registrationDate,
        this.subscriptionType,
        this.subscriptionStartDate,
        this.subscriptionEndDate,
        this.createdAt,
        this.updatedAt,
        this.createdBy,
        this.updatedBy,
        this.isDeleted,
        this.isDesktopadmin,
        this.deviceuniqueid,
        this.regPromocode});

  Response.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    fullName = json['full_name'];
    companyName = json['company_name'];
    email = json['email'];
    mobileNo = json['mobile_no'];
    password = json['password'];
    tmpPassword = json['tmp_password'];
    loginWith = json['login_with'];
    fbId = json['fb_id'];
    gpId = json['gp_id'];
    twId = json['tw_id'];
    liId = json['li_id'];
    address = json['address'];
    city = json['city'];
    state = json['state'];
    country = json['country'];
    currentStatus = json['current_status'];
    partnerId = json['partner_id'];
    verificationCode = json['verification_code'];
    verificationCodeDate = json['verification_code_date'];
    clientid = json['clientid'];
    isAdminUser = json['is_admin_user'];
    desktopconnectionstatus = json['desktopconnectionstatus'];
    registrationDate = json['registration_date'];
    subscriptionType = json['subscription_type'];
    subscriptionStartDate = json['subscription_start_date'];
    subscriptionEndDate = json['subscription_end_date'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    isDeleted = json['is_deleted'];
    isDesktopadmin = json['is_desktopadmin'];
    deviceuniqueid = json['deviceuniqueid'];
    regPromocode = json['reg_promocode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['full_name'] = this.fullName;
    data['company_name'] = this.companyName;
    data['email'] = this.email;
    data['mobile_no'] = this.mobileNo;
    data['password'] = this.password;
    data['tmp_password'] = this.tmpPassword;
    data['login_with'] = this.loginWith;
    data['fb_id'] = this.fbId;
    data['gp_id'] = this.gpId;
    data['tw_id'] = this.twId;
    data['li_id'] = this.liId;
    data['address'] = this.address;
    data['city'] = this.city;
    data['state'] = this.state;
    data['country'] = this.country;
    data['current_status'] = this.currentStatus;
    data['partner_id'] = this.partnerId;
    data['verification_code'] = this.verificationCode;
    data['verification_code_date'] = this.verificationCodeDate;
    data['clientid'] = this.clientid;
    data['is_admin_user'] = this.isAdminUser;
    data['desktopconnectionstatus'] = this.desktopconnectionstatus;
    data['registration_date'] = this.registrationDate;
    data['subscription_type'] = this.subscriptionType;
    data['subscription_start_date'] = this.subscriptionStartDate;
    data['subscription_end_date'] = this.subscriptionEndDate;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['created_by'] = this.createdBy;
    data['updated_by'] = this.updatedBy;
    data['is_deleted'] = this.isDeleted;
    data['is_desktopadmin'] = this.isDesktopadmin;
    data['deviceuniqueid'] = this.deviceuniqueid;
    data['reg_promocode'] = this.regPromocode;
    return data;
  }
}
